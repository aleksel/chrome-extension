"use strict";

let json = [];
let images = $('.image_holder img');
for (let i = 0, count = images.length; i < count; ++i) {
    if (images[i].src.indexOf('sample.jpg') === -1) {
        json.push({
            url:   images[i].src.replace('t_media_lib_thumb/', ''),
            thumb: images[i].src,
        });
    }
}
console.log('-------------- count: ' + json.length);
console.log(JSON.stringify(json));