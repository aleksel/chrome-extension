"use strict";

export default class NumberUtils {

    /**
     * Returns a random number between min (inclusive) and max (exclusive)
     */
    static getRandomArbitrary = (min, max) => {
        return Math.random() * (max - min) + min;
    };

    /**
     * Returns a random integer between min (inclusive) and max (inclusive)
     * Using Math.round() will give you a non-uniform distribution!
     */
    static getRandomInt = (min, max) => {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
}