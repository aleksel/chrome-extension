"use strict";

let WindowsToasterOut;
let notifierOut;

if (ELECTRON) {
    const notify = require('node-notifier');
    const notifier = require('node-notifier');
    const {WindowsToaster} = notify;
    WindowsToasterOut = WindowsToaster;
    notifierOut = notifier;
}

export default class Notify {

    static notifier = ELECTRON ? new WindowsToasterOut({
            withFallback: false, // Fallback to Growl or Balloons?
            customPath:   './node_modules/node-notifier/vendor/toaster/toast.exe',
        }) : null;

    static notifyWin = (title, message) => {
        if (Notify.notifier) {
            Notify.notifier.notify({
                title,
                message,
                sound: true, // Only Notification Center or Windows Toasters
                //wait: true // Wait with callback, until user action is taken against notification
            }, function (err, response) {
                if (err || response) console.log('WINDOWS NOTIFY ERROR', err, response);
            });
        }
    };

    static notifyNative = (title, message, onClick = null, onTimeout = null) => {
        if (notifierOut) {
            notifierOut.notify({
                title:   title,
                message: message,
                //icon: path.join(__dirname, 'coulson.jpg'), // Absolute path (doesn't work on balloons)
                sound:   true, // Only Notification Center or Windows Toasters
                wait:    true // Wait with callback, until user action is taken against notification
            }, function (err, response) {
                if (err || response) console.log('WINDOWS NOTIFY ERROR', err, response);
            });

            // function (notifierObject, options) { }
            notifierOut.on('click', onClick);

            notifierOut.on('timeout', onTimeout);
            if (typeof onClick === 'function') {
                notifierOut.onclick = onClick;
            }
        }
    };

    static notifyHtml5 = (title, message, icon = null, onClick = null, onShow = null, onClose = null) => {
        let notification;
        const options = {
            body: message,
            dir:  'auto', // or ltr, rtl
            lang: 'EN', //lang used within the notification.
            tag:  'notificationPopup', //An element ID to get/set the content
            icon: icon || '' //The URL of an image to be used as an icon
        };
        if (window.Notification && Notification.permission === "granted") {
            notification = new Notification(title, options);
        } else if (window.Notification && Notification.permission !== 'denied') {
            Notification.requestPermission(function (permission) {
                if (permission === "granted") {
                    notification = new Notification(title, options);
                }
            });
        }
        if (notification) {
            if (typeof onClick === 'function') {
                notification.onclick = onClick;
            }
            if (typeof onShow === 'function') {
                notification.ondisplay = onShow;
            }
            if (typeof onClose === 'function') {
                notification.onclose = onClose;
            }
        }
        return false;
    };
}