"use strict";

export const NOTIFIED_ISSUES = 'NOTIFIED_ISSUES';

export default class Storage {

    /**
     * @param data {object} key: value
     */
    static save = (data, onlyLocalStorage = false) => {
        console.log('---SAVE---', JSON.stringify(data));
        try {
            if (chrome !== undefined && !onlyLocalStorage && chrome.storage) {
                chrome.storage.sync.set(data, () => {
                });
                return;
            }
        } catch (e) {
        }
        for (let item in data) {
            if (data.hasOwnProperty(item)) {
                if (typeof data[item] === 'object') {
                    localStorage.setItem(item, JSON.stringify(data[item]));
                } else {
                    localStorage.setItem(item, data[item]);
                }
            }
        }
    };

    /**
     * chrome.storage.sync.getBytesInUse('APP_SETTINGS_FIELD_NAME', (data) => console.log(data))
     * @returns {Promise}
     */
    static get = (key, onlyLocalStorage = false, async = true) => {
        try {
            if (chrome !== undefined && !onlyLocalStorage && chrome.storage) {
                return new Promise((resolve, reject) => {
                    chrome.storage.sync.get(key, obj => {
                        if (obj[key] !== undefined) {
                            resolve(Storage.checkAndParseJson(obj[key]));
                        } else {
                            reject('Not found');
                        }
                    });
                });
            }
        } catch (e) {
        }
        if (async) {
            return new Promise((resolve, reject) => {
                const value = localStorage.getItem(key);
                if (value !== null) {
                    resolve(Storage.checkAndParseJson(value));
                } else {
                    reject('Not found');
                }
            });
        } else {
            return Storage.checkAndParseJson(localStorage.getItem(key));
        }
    };

    static checkAndParseJson = (data) => {
        try {
            console.log('---LOAD---', data);
            return JSON.parse(data);
        } catch (ignore) {
        }
        return data;
    };
}