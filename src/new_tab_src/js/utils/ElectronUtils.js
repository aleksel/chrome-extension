"use strict";

const electron = ELECTRON ? require('electron') : null;
const shell = ELECTRON ? electron.shell : null;

export default class ElectronUtils {

    static onOpenIssue = link => event => {
        event.preventDefault();
        if (ELECTRON) {
            shell.openExternal(link);
        } else {
            window.open(link);
        }
    };
}