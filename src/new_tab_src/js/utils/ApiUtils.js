"use strict";

import "whatwg-fetch";

export default class ApiUtils {
    static status = response => {
        if (response.status >= 200 && response.status < 399) {
            return Promise.resolve(response);
        } else {
            return Promise.reject({errorMessage: response.statusText, errorCode: response.status});
        }
    };

    static json = response => response.json();

    static get = (url, headers = {}) => {
        console.log('GET', url);
        return fetch(url, {
            method:  'GET',
            headers: {
                'Content-Type': 'application/json',
                ...headers
            }
        }).then(ApiUtils.status)
            .then(ApiUtils.json);
    };

    static put = (url, data) => {
        console.log('PUT', url, data);
        return fetch(url, {
            method:  'PUT',
            body:    JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(ApiUtils.status);
    };

    static post = (url, data) => {
        console.log('POST', url, data);
        return fetch(url, {
            method:  'POST',
            body:    JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(ApiUtils.status)
            .then(response => console.log(response));
    };
}