"use strict";

import * as React from "react";

export const IMAGE_PROPS = React.PropTypes.arrayOf(
    React.PropTypes.shape({
        url:   React.PropTypes.string.isRequired,
        thumb: React.PropTypes.string.isRequired,
    }).isRequired
);