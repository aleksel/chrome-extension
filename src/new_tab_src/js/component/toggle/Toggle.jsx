"use strict";

import * as React from "react";

export default class Toggle extends React.Component {

    static defaultProps = {
        style:           {},
        styleLabel:      {},
        styleInput:      {},
        label:           '',
        labelClassName:  '',
        toggleClassName: '',
        className:       '',
        checked:         false,
        onChange:        () => {
        },
    };

    static propTypes = {
        style:           React.PropTypes.object,
        styleLabel:      React.PropTypes.object,
        styleInput:      React.PropTypes.object,
        label:           React.PropTypes.oneOfType([React.PropTypes.node, React.PropTypes.string, React.PropTypes.func]),
        labelClassName:  React.PropTypes.string,
        toggleClassName: React.PropTypes.string,
        className:       React.PropTypes.string,
        checked:         React.PropTypes.bool,
        onChange:        React.PropTypes.func,
    };

    render() {
        const {
            style, labelClassName, className, toggleClassName, styleLabel, styleInput, label, checked, onChange
        } = this.props;

        return <div style={style} className={className}>
            <div className="togglebutton">
                <label style={styleLabel} className={labelClassName + (checked ? ' active' : '')}>
                    {typeof label === 'function' ? label() : label}
                    <input onChange={onChange}
                           style={styleInput}
                           type="checkbox"
                           checked={checked}/>
                    <span className={"toggle " + toggleClassName}></span>
                </label>
            </div>
        </div>;
    }
}