"use strict";

import * as React from "react";
import {Tooltip, OverlayTrigger} from "react-bootstrap";

export default class TimelineItem extends React.Component {

    static propTypes = {
        inverted: React.PropTypes.bool.isRequired,
        item:     React.PropTypes.shape({
            title:      React.PropTypes.string,
            time:       React.PropTypes.string,
            badgeColor: React.PropTypes.string,
            badgeType:  React.PropTypes.string,
            person:     React.PropTypes.string,
            body:       React.PropTypes.oneOfType([React.PropTypes.node, React.PropTypes.string]),
            icon:       React.PropTypes.string,
        }).isRequired,
    };

    getTooltipDate = date => {
        return <Tooltip id="tooltip">
            <strong>{date.formatDate('yyyy-MM-dd')}</strong> {date.formatDate('HH:mm:ss')}
        </Tooltip>;
    };

    render() {
        const {getTooltipDate} = this;
        const {item, inverted} = this.props;
        const date = new Date(Date.parse(item.time));
        return <li className={inverted ? 'timeline-inverted' : ''}>
            <div className={"timeline-badge " + (item.badgeType ? item.badgeType : '')}
                 style={{backgroundColor: item.badgeColor || '' }}>
                <i className={item.icon || 'fa fa-circle-thin'}></i>
            </div>
            <div className="timeline-panel">
                <div className="timeline-heading">
                    {item.time ?
                        <p className="timeline-time">
                            <small className="text-muted">
                                <OverlayTrigger placement="top"
                                                overlay={getTooltipDate(date)}
                                                rootClose={true}>
                                    <span>
                                        {date.duration()}
                                    </span>
                                </OverlayTrigger>
                            </small>
                        </p>
                        : null}
                    {item.title ? <h4 className="timeline-title">{item.title}</h4> : null}
                </div>
                {item.body ? <div className="timeline-body">
                        {item.body ? item.body : null}
                    </div> : null}
                {item.person ? <p className="timeline-person row">
                        <small>@{item.person}</small>
                    </p> : null}
            </div>
        </li>;
    }
}