"use strict";

import * as React from "react";
import TimelineItem from "./TimelineItem";
import "./timeline.css";

export default class Timeline extends React.Component {

    constructor() {
        super();
        this.state = {
            hljs: null,
        };
        require.ensure([], function (require) {
            require('highlight.js/styles/dracula.css');
        }.bind(this), 'highlight.js/styles/dracula.css');
        require.ensure([], function (require) {
            this.setState({hljs: require('highlight.js')});
        }.bind(this), 'highlight.js');
    }

    static propTypes = {
        title: React.PropTypes.string,
        items: React.PropTypes.arrayOf(React.PropTypes.shape({
            title:      React.PropTypes.string,
            time:       React.PropTypes.string,
            badgeColor: React.PropTypes.string,
            badgeType:  React.PropTypes.string,
            person:     React.PropTypes.string,
            body:       React.PropTypes.oneOfType([React.PropTypes.node, React.PropTypes.string]),
            icon:       React.PropTypes.string,
        }).isRequired).isRequired,
    };

    componentDidMount() {
        $('pre code').each((i, block) => {
            if (this.state.hljs) {
                this.state.hljs.highlightBlock(block);
            }
        });
    }

    render() {
        const {items, title} = this.props;
        return <div>
            {title ? <div>
                <h1>{title}</h1>
            </div> : null}
            <ul className="timeline">
                {items.map((item, index) => <TimelineItem key={index}
                                                          inverted={index % 2 === 0}
                                                          item={item}/>)}
            </ul>
        </div>;
    }
}