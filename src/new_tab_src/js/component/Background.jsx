"use strict";

import * as React from "react";
import NumberUtils from "../utils/NumberUtils";
import {IMAGE_PROPS} from "../constant/PropsConst";
import {CHANGE_WALLPAPER_EVERY_DAY} from "./settings/menu/Backgrounds";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as SettingsActions from "./../actions/settings";
import {MENU_BACKGROUNDS} from "./settings/Settings";

export const BACKGROUND_URL = 'BU';
export const BACKGROUND_URL_DAY = 'BUD';
export const IMAGE_URL_PREFIX = 'http://res.cloudinary.com/dxqfkxbur/image/upload/';
export const IMAGE_LIST_URLS = 'http://res.cloudinary.com/dxqfkxbur/raw/upload/';

class Background extends React.Component {

    state = {
        image: '',
    };

    static defaultProps = {};

    static propTypes = {
        settings: React.PropTypes.shape({
            [BACKGROUND_URL]:     React.PropTypes.string,
            [BACKGROUND_URL_DAY]: React.PropTypes.number,
        }).isRequired,
        images:   IMAGE_PROPS,
    };

    componentWillReceiveProps(props) {
        const {settings, images, changeSettings} = props;
        const url = settings[BACKGROUND_URL];
        const day = settings[BACKGROUND_URL_DAY];
        const dayNumber = new Date().getDate();
        if (images.length && (!url || (settings[CHANGE_WALLPAPER_EVERY_DAY] && day !== dayNumber))) {
            settings[BACKGROUND_URL] = images[NumberUtils.getRandomInt(0, images.length - 1)].url;
            settings[BACKGROUND_URL_DAY] = dayNumber;
            changeSettings(settings, MENU_BACKGROUNDS);
        }
        this.setState({image: settings[BACKGROUND_URL].replace(IMAGE_URL_PREFIX, '')});
    }

    render() {
        const {image} = this.state;
        return <div id="background"
                    style={{
                        background: image ? 'url("' + image + '") no-repeat center' : 'none'
                    }}>
        </div>;
    }
}

function mapStateToProps(state) {
    return {
        settings: state.settings[MENU_BACKGROUNDS],
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(SettingsActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Background);