"use strict";

import * as React from "react";
import Toggle from "../../toggle/Toggle";
import RedmineSettings from "../../../widget/redmine/RedmineSettings";
import TimerSettings from "../../../widget/timer/TimerSettings";
import WeatherSettings from "../../../widget/weather/WeatherSettings";

export const REDMINE_WIDGET = 'RM';
export const TIMER_WIDGET = 'TW';
export const WEATHER_WIDGET = 'WW';
export default class Widgets extends React.Component {

    state = {
        widgetSetting: null,
    };

    WIDGET_SETTINGS = () => {
        return {
            [REDMINE_WIDGET]: {
                clazz:    RedmineSettings,
                key:      REDMINE_WIDGET,
                showFunc: this.onToggleRedmineWidget,
                header:   <span>
                    <div className="settings-window-options-redmine-icon"></div>
                    Redmine
                </span>
            },
            [WEATHER_WIDGET]: {
                clazz:    WeatherSettings,
                key:      WEATHER_WIDGET,
                showFunc: () => {
                    const {settings} = this.props;
                    settings[WEATHER_WIDGET].show = !settings[WEATHER_WIDGET].show;
                    this.props.onChange(settings[WEATHER_WIDGET], WEATHER_WIDGET);
                },
                header:   () => {
                    const {settings} = this.props;
                    return <span><div className="settings-window-options-weather-icon"></div>
                    Weather {settings[WEATHER_WIDGET].show && !Object.keys(settings[WEATHER_WIDGET].loc).length
                            ? <small className="has-error" style={{marginLeft: 10}}> City not set</small> : ''}
                    </span>;
                }
            },
            [TIMER_WIDGET]:   {
                clazz:    TimerSettings,
                key:      TIMER_WIDGET,
                showFunc: () => {
                    const {settings} = this.props;
                    settings[TIMER_WIDGET].show = !settings[TIMER_WIDGET].show;
                    this.props.onChange(settings[TIMER_WIDGET], TIMER_WIDGET);
                },
                header:   <span><div className="settings-window-options-timer-icon"></div>Timer</span>
            }
        };
    };

    static defaultProps = {
        header: '',
    };

    componentWillMount() {
        $(document).on('SETTINGS:RESET', () => this.setState({widgetSetting: null}));
    }

    componentWillUnmount() {
        $(document).off('SETTINGS:RESET');
    }

    static propTypes = {
        header:   React.PropTypes.string.isRequired,
        onChange: React.PropTypes.func.isRequired,
        settings: React.PropTypes.shape({}).isRequired,
    };

    onToggleRedmineWidget = event => {
        const {settings} = this.props;
        if (typeof settings[REDMINE_WIDGET] !== 'object') {
            settings[REDMINE_WIDGET] = {};
        }
        settings[REDMINE_WIDGET].show = !settings[REDMINE_WIDGET].show;
        this.props.onChange(settings[REDMINE_WIDGET], REDMINE_WIDGET);
    };

    renderWidgetSettings = config => event => {
        this.setState({
            widgetSetting: {
                clazz:  config.clazz,
                key:    config.key,
                header: config.header
            }
        });
    };

    renderWidgets = () => {
        const widgets = [];
        const {settings} = this.props;
        const {renderWidgetSettings} = this;

        let {WIDGET_SETTINGS} = this;
        WIDGET_SETTINGS = WIDGET_SETTINGS();

        for (let widget in WIDGET_SETTINGS) {
            if (WIDGET_SETTINGS.hasOwnProperty(widget)) {
                widgets.push(<div key={widget}
                                  className="form-group has-info col-lg-12 col-md-12 col-sm-12 col-xs-12 row"
                                  style={{padding: 0}}>
                    <div
                        onClick={settings[widget].show ? renderWidgetSettings(WIDGET_SETTINGS[widget], this.props) : null}
                        className={"col-lg-1 col-md-1 col-sd-1 col-xs-1 settings-options-widget-slider default-text-color"
                        + (!settings[widget].show ? ' disabled' : '')}>
                        <i className="fa fa-sliders"></i>
                    </div>
                    <Toggle label={WIDGET_SETTINGS[widget].header}
                            className="col-lg-11 col-md-11 col-sm-11 col-xs-11"
                            labelClassName="options-toggle-label"
                            toggleClassName="options-toggle"
                            checked={settings[widget].show}
                            onChange={WIDGET_SETTINGS[widget].showFunc}/>
                </div>);
            }
        }
        return widgets;
    };

    closeWidgetSettings = event => this.setState({widgetSetting: null});

    render() {
        const {header, settings} = this.props;
        const {widgetSetting} = this.state;
        const {renderWidgets, closeWidgetSettings} = this;
        return <div style={{margin: 0}}>
            <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 settings-window-options-header">
                    <div className={widgetSetting ? 'widget-settings-active' : ''}
                         onClick={closeWidgetSettings}
                         style={{float: 'left'}}>
                        <span>{header}</span> {widgetSetting ?
                        <i className="fa fa-angle-right"></i> : null}
                    </div>
                    {widgetSetting ? <div className="settings-widget-header-wrapper">
                            {typeof widgetSetting.header === 'function' ? widgetSetting.header() : widgetSetting.header}
                        </div> : null}
                </div>
            </div>
            {widgetSetting ? React.createElement(widgetSetting.clazz, {
                    settings: settings[widgetSetting.key],
                    onChange: settingsWidget => this.props.onChange(settingsWidget, widgetSetting.key)
                }) : renderWidgets()}
        </div>;
    }
}