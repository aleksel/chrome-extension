"use strict";

import * as React from "react";
import Toggle from "../../toggle/Toggle";

export default class Windows extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            SketchPicker: null,
        };
        require.ensure([], function (require) {
            this.setState({SketchPicker: require('react-color').SketchPicker});
        }.bind(this), 'react-color');
    }

    static defaultProps = {
        header: '',
    };

    static propTypes = {
        header:   React.PropTypes.string.isRequired,
        onChange: React.PropTypes.func.isRequired,
        settings: React.PropTypes.shape({
            st: React.PropTypes.number.isRequired,
            sp: React.PropTypes.number.isRequired,
        }).isRequired,
    };

    onChange = key => event => {
        const {settings} = this.props;
        if (key === 'bc') {
            settings[key] = event.rgb;
        } else {
            settings[key] = settings[key] ? 0 : 1;
        }
        this.props.onChange(settings);
    };

    render() {
        const {header, settings} = this.props;
        const {SketchPicker} = this.state;
        const {onChange} = this;
        return <div style={{margin: 0}}>
            <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 settings-window-options-header">{header}</div>
            </div>
            <div className="row">
                <div className="form-group has-info col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <Toggle label="Show Time window"
                            labelClassName="options-toggle-label"
                            toggleClassName="options-toggle"
                            checked={settings.st === 1}
                            onChange={onChange('st')}/>
                </div>
            </div>
            <div className="row">
                <div style={{marginTop: '-3px'}}
                     className="form-group has-info col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <Toggle label="Show Password window"
                            labelClassName="options-toggle-label"
                            toggleClassName="options-toggle"
                            checked={settings.sp === 1}
                            onChange={onChange('sp')}/>
                </div>
            </div>
            <div className="row">
                <br/>
                <div style={{marginLeft: 15}}>
                    {SketchPicker ? <SketchPicker color={settings.bc}
                                  onChangeComplete={onChange('bc')}/> : null};
                </div>
            </div>
        </div>;
    }
}