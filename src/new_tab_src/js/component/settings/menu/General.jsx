"use strict";

import * as React from "react";
import Toggle from "../../toggle/Toggle";

export const PIN_WIDGETS = 'PIN_W';

export default class General extends React.Component {

    state = {
        name: '',
    };

    timer = null;

    static defaultProps = {
        header: '',
    };

    static propTypes = {
        header:   React.PropTypes.string.isRequired,
        onChange: React.PropTypes.func.isRequired,
        settings: React.PropTypes.shape({
            name: React.PropTypes.string.isRequired,
        }).isRequired,
    };

    onChangeName = event => {
        const {settings} = this.props;
        settings.name = event.target.value;
        this.props.onChange(settings);
    };

    onTogglePinWidget = event => {
        const {settings} = this.props;
        settings[PIN_WIDGETS] = !settings[PIN_WIDGETS];
        this.props.onChange(settings);
    };

    onToggleAutoLaunch = event => {
        const {settings} = this.props;
        settings.al = settings.al === 1 ? 0 : 1;
        if (ELECTRON) {
            const {ipcRenderer} = require('electron');
            ipcRenderer.send('auto-launch', settings.al === 1);
        }
        this.props.onChange(settings);
    };

    render() {
        const {header, settings} = this.props;
        const {onChangeName, onTogglePinWidget, onToggleAutoLaunch} = this;
        return <div className="row" style={{margin: 0}}>
            <div className="col-lg-12 settings-window-options-header">{header}</div>
            <div className="form-group has-info col-lg-12">
                <label style={{marginLeft: '15px'}} className="control-label default-text-color">
                    <input value={settings.name}
                           onChange={onChangeName}
                           type="text" className="form-control default-text-color" placeholder=""/>
                    <sub className="default-text-color">Name</sub>
                </label>
            </div>
            <div style={{marginTop: '-8px'}} className="form-group has-info col-lg-12">
                <Toggle label="Pin widgets on desktop"
                        labelClassName="options-toggle-label"
                        toggleClassName="options-toggle"
                        checked={settings[PIN_WIDGETS]}
                        onChange={onTogglePinWidget}/>
            </div>
            {ELECTRON ? <div style={{marginTop: '-3px'}} className="form-group has-info col-lg-12">
                    <Toggle label="Start app with OS"
                            labelClassName="options-toggle-label"
                            toggleClassName="options-toggle"
                            checked={settings.al === 1}
                            onChange={onToggleAutoLaunch}/>
                </div> : null}
        </div>;
    }
}