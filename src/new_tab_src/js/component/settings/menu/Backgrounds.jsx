"use strict";

import * as React from "react";
import Toggle from "../../toggle/Toggle";
import {BACKGROUND_URL, IMAGE_URL_PREFIX} from "../../Background";
import {IMAGE_PROPS} from "../../../constant/PropsConst";

export const CHANGE_WALLPAPER_EVERY_DAY = 'CWED';

export default class Backgrounds extends React.Component {

    state = {};

    static defaultProps = {
        header: '',
    };

    static propTypes = {
        header:   React.PropTypes.string.isRequired,
        onChange: React.PropTypes.func.isRequired,
        settings: React.PropTypes.shape({}).isRequired,
        images:   IMAGE_PROPS,
    };

    onChangeWallpaper = (url, props) => event => {
        const {settings} = props;
        settings[BACKGROUND_URL] = url.replace(IMAGE_URL_PREFIX, '');
        props.onChange(settings);
    };

    onChangeTriggerChangeWallpaper = event => {
        const {settings} = this.props;
        settings[CHANGE_WALLPAPER_EVERY_DAY] = !settings[CHANGE_WALLPAPER_EVERY_DAY];
        this.props.onChange(settings);
    };

    render() {
        const {header, settings, images} = this.props;
        const {onChangeWallpaper, onChangeTriggerChangeWallpaper} = this;
        return <div className="row settings-window-options-backgrounds" style={{margin: 0}}>
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 settings-window-options-header">{header}</div>
            <br/>
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12" style={{marginTop: '15px'}}>
                <Toggle label="Change wallpaper every day"
                        labelClassName="options-toggle-label"
                        toggleClassName="options-toggle"
                        checked={settings[CHANGE_WALLPAPER_EVERY_DAY]}
                        onChange={onChangeTriggerChangeWallpaper}/>
            </div>
            <div className="form-group has-info col-lg-12 col-md-12 col-sm-12 col-xs-12" style={{padding: '0 7px'}}>
                {images.map(image => {
                    return <div key={image.thumb}
                                className="background-wallpaper"
                                onClick={onChangeWallpaper(image.url, this.props)}
                                style={{background: 'url("' + image.thumb + '") no-repeat center'}}>
                        <div className="background-check">
                            {image.url.indexOf(settings[BACKGROUND_URL]) !== -1 ? <i className="fa fa-check"></i> : null}
                        </div>
                    </div>;
                })}
            </div>
        </div>;
    }
}