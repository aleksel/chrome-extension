"use strict";

import * as React from "react";
import "./settings.css";
import General from "./menu/General";
import Widgets from "./menu/Widgets";
import Windows from "./menu/Windows";
import Backgrounds from "./menu/Backgrounds";
import {IMAGE_PROPS} from "../../constant/PropsConst";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as SettingsActions from "./../../actions/settings";

export const MENU_GENERAL = '1';
export const MENU_WIDGETS = '2';
export const MENU_WINDOWS = '3';
export const MENU_BACKGROUNDS = '4';
export const MENU = [
    MENU_GENERAL,
    MENU_BACKGROUNDS,
    MENU_WIDGETS,
    MENU_WINDOWS,
];

class Settings extends React.Component {

    timer = null;
    state = {
        show:             null,
        currentMenuPoint: MENU_GENERAL,
    };

    MENU_CONFIG = props => {
        return {
            [MENU_GENERAL]:     {
                className: General,
                props:     {
                    header:   'General',
                    onChange: settingsGeneral => {
                        const {settings, changeAllSettings} = this.props;
                        settings[MENU_GENERAL] = settingsGeneral;
                        changeAllSettings({...settings});
                    }
                }
            },
            [MENU_BACKGROUNDS]: {
                className: Backgrounds,
                props:     {
                    header:   'Backgrounds',
                    images:   props.images,
                    onChange: settingsBackground => {
                        const {settings, changeAllSettings} = this.props;
                        settings[MENU_BACKGROUNDS] = settingsBackground;
                        changeAllSettings({...settings});
                    },
                }
            },
            [MENU_WIDGETS]:     {
                className: Widgets,
                props:     {
                    header:   'Widgets',
                    onChange: (settingsWidgets, secondType) => {
                        const {changeSecondSettings} = this.props;
                        changeSecondSettings(settingsWidgets, MENU_WIDGETS, secondType);
                    }
                },
            },
            [MENU_WINDOWS]:     {
                className: Windows,
                props:     {
                    header:   'Windows',
                    onChange: settingsWindows => {
                        const {settings, changeAllSettings} = this.props;
                        settings[MENU_WINDOWS] = settingsWindows;
                        changeAllSettings({...settings});
                    }
                }
            },
        };
    };

    static propTypes = {
        settings: React.PropTypes.shape({
            [MENU_GENERAL]: React.PropTypes.shape({
                name: React.PropTypes.string.isRequired,
            }).isRequired,
        }).isRequired,
        images:   IMAGE_PROPS,
    };

    componentWillMount() {
        $(window).click(() => {
            this.setState({show: this.state.show === null ? null : false});
        });
    }

    onShow = event => {
        event.stopPropagation();
        this.setState({show: !this.state.show});
    };

    onClickSettingsWindow = event => event.stopPropagation();

    onSelectMenu = menu => event => {
        if (menu === this.state.currentMenuPoint) {
            // Little hack, need reset settings page on click on the same menu point
            // For example: change widget settings page from settings of current widget to main
            $(document).trigger('SETTINGS:RESET');
        }
        this.setState({currentMenuPoint: menu});
    };

    render() {
        const {onShow, onSelectMenu, onClickSettingsWindow} = this;
        const {show, currentMenuPoint} = this.state;
        const {settings} = this.props;
        const menuConfig = this.MENU_CONFIG(this.props);
        let showClass = '';
        let animateClass = ' animate-none';
        if (show === true) {
            showClass = ' active';
            animateClass = ' animate-show';
        } else if (show === false) {
            showClass = ' reverse';
            animateClass = ' animate-hide';
        }
        return <div onClick={onClickSettingsWindow}>
            <div className={"settings-window" + animateClass}>
                <div className="settings-window-list">
                    <ul className="default-text-color">
                        {MENU.map(menu => <li key={menu}
                                              onClick={onSelectMenu(menu)}
                                              className={currentMenuPoint === menu ? 'active' : 'reverse'}>
                            {menuConfig[menu].props.header}
                        </li>)}
                    </ul>
                </div>
                <div className="settings-window-delimiter"></div>
                <div className="settings-window-options">
                    {React.createElement(menuConfig[currentMenuPoint].className,
                        {...menuConfig[currentMenuPoint].props, ...{settings: settings[currentMenuPoint],}})}
                </div>
            </div>
            <div onClick={onShow}
                 className={"settings-button" + showClass}>
                <i className="fa fa-cog"></i>
            </div>
        </div>;
    }
}

function mapStateToProps(state) {
    return {
        settings: state.settings,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(SettingsActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);