"use strict";

import * as React from "react";
import Draggable from "react-draggable";
import CopyToClipboard from "react-copy-to-clipboard";
import generatePassword from "password-generator";
import App from "../../App";
import "./password.css";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as SettingsActions from "./../../actions/settings";
import {MENU_WINDOWS} from "./../settings/Settings";
// If import script with webpack, it blocking browser when calculating hash
require('script-loader!./../../lib/twin_bcrypt');

export const DEFAULT_Y = 100;
export const COORDINATE_X = 'pX';
export const COORDINATE_Y = 'pY';

class Password extends React.Component {

    state = {
        password:                '',
        generatedPassword:       '',
        passwordUppercase:       true,
        passwordDigits:          true,
        passwordSymbols:         false,
        generatePasswordLength:  8,
        hash:                    '',
        bCryptCheckResult:       null,
        bCryptCheckResultError:  '',
        bCryptCheckProgress:     0,
        copyOk:                  false,
        copyGeneratedPasswordOk: false,
        bCryptProgress:          0,
        bCryptCost:              10,
        bCrypt:                  '',
        bCryptResult:            '',
        showPassword:            false,
    };

    static defaultProps = {
        zIndex: 0
    };

    static propTypes = {
        zIndex:         React.PropTypes.number,
        onActiveWindow: React.PropTypes.func,
        settings:       React.PropTypes.shape({
            [COORDINATE_X]: React.PropTypes.number,
            [COORDINATE_Y]: React.PropTypes.number,
        }),
    };

    getCoordinates = () => {
        const xCoordinate = this.props.settings[COORDINATE_X];
        const yCoordinate = this.props.settings[COORDINATE_Y];
        return {
            x: !xCoordinate || xCoordinate > 1000 ? 0 : xCoordinate,
            y: !yCoordinate || yCoordinate > 1900 ? DEFAULT_Y : yCoordinate,
        };
    };

    onStopDrag = (...args) => {
        const {settings, changeSettings} = this.props;
        settings[COORDINATE_X] = args[1].x;
        settings[COORDINATE_Y] = args[1].y;
        changeSettings(settings, MENU_WINDOWS);
    };

    onAlarmDismiss = () => this.setState({bCryptResult: ''});
    onCloseGeneratedPassword = () => this.setState({generatedPassword: ''});

    onHashing = () => {
        TwinBcrypt.hash(this.state.bCrypt, this.state.bCryptCost, progress => {
            const bCryptProgress = progress * 100;
            this.setState({bCryptProgress: bCryptProgress === 100 ? 0 : bCryptProgress});
        }, hash => {
            this.setState({bCryptResult: hash});
        });
    };

    onCheckPassword = () => {
        try {
            TwinBcrypt.compare(this.state.password, this.state.hash, progress => {
                const bCryptCheckProgress = progress * 100;
                this.setState({bCryptCheckProgress: bCryptCheckProgress === 100 ? 0 : bCryptCheckProgress});
            }, result => this.setState({bCryptCheckResult: result, bCryptCheckResultError: ''}));
        } catch (e) {
            this.setState({bCryptCheckResultError: true, bCryptCheckResult: null});
        }
    };

    onChangePassword = event => this.setState({
        password:               event.target.value,
        bCryptCheckResult:      null,
        bCryptCheckResultError: ''
    });
    onChangeHash = event => this.setState({
        hash:                   event.target.value,
        bCryptCheckResult:      null,
        bCryptCheckResultError: ''
    });
    onChangeUppercase = event => this.setState({passwordUppercase: event.target.checked});
    onChangeDigits = event => this.setState({passwordDigits: event.target.checked});
    onChangeSymbols = event => this.setState({passwordSymbols: event.target.checked});
    onChangeName = event => this.setState({bCrypt: event.target.value});
    onChangePasswordLength = event => this.setState({generatePasswordLength: event.target.value});
    onChangeCost = event => {
        let cost = parseInt(event.target.value);
        cost = cost < 1 ? 1 : cost;
        cost = cost > 15 ? 15 : cost;
        this.setState({bCryptCost: cost});
    };

    onClose = () => this.setState({showPassword: false});

    onOpen = (event) => {
        event.stopPropagation();
        this.props.onActiveWindow();
        this.setState({showPassword: !this.state.showPassword})
    };

    onCopy = () => {
        this.setState({copyOk: true});
        setTimeout(() => this.setState({copyOk: false}), 1000)
    };

    onCopyGeneratedPassword = () => {
        this.setState({copyGeneratedPasswordOk: true});
        setTimeout(() => this.setState({copyGeneratedPasswordOk: false}), 1000)
    };

    onGeneratePassword = () => {
        let pattern = 'a-z';
        if (!this.state.passwordUppercase && !this.state.passwordDigits && !this.state.passwordSymbols) {
            this.setState({generatedPassword: generatePassword(this.state.generatePasswordLength)});
            return;
        }
        if (this.state.passwordUppercase) {
            pattern += 'A-Z';
        }
        if (this.state.passwordDigits) {
            pattern += '\\d';
        }
        if (this.state.passwordSymbols) {
            pattern += '@#\\$%\\^&\\*\\(\\)_\\-\\+=!:;\\|\\?\\[\\]{}';
        }
        this.setState({generatedPassword: generatePassword(this.state.generatePasswordLength, false, ('[' + pattern + ']'))});
    };

    render() {
        const {
            bCrypt, showPassword, bCryptResult, copyOk, bCryptCost, bCryptProgress, password, hash,
            bCryptCheckProgress, bCryptCheckResult, bCryptCheckResultError, copyGeneratedPasswordOk, generatedPassword,
            generatePasswordLength, passwordUppercase, passwordDigits, passwordSymbols
        } = this.state;
        const {zIndex, onActiveWindow, settings} = this.props;
        const {
            onStopDrag, onAlarmDismiss, onHashing, onChangeName, onOpen, onClose, onCopy, onChangeCost,
            onChangeHash, onChangePassword, onCheckPassword, onCloseGeneratedPassword, onCopyGeneratedPassword,
            onGeneratePassword, onChangePasswordLength, onChangeUppercase, onChangeDigits, onChangeSymbols,
            getCoordinates
        } = this;
        const {bc} = settings;
        const bColor = `rgba(${bc.r},${bc.g},${bc.b},${bc.a})`;
        const errorClass = bCryptCheckResultError ? 'has-error' : '';
        let checkClass = '';
        if (bCryptCheckResult) {
            checkClass = 'has-success';
        } else if (bCryptCheckResult === false) {
            checkClass = 'has-error';
        }
        return <div>
            <div style={{position: 'absolute', marginTop: '30px', zIndex: 10}} className="empty-btn">
                <div className="togglebutton">
                    <label style={{padding: '10px 0 10px 10px', margin: 0}}>
                        <input onClick={onOpen}
                               style={{marginLeft: '10px'}}
                               type="checkbox"
                               checked={showPassword || false}/>
                        <span className="toggle"></span>
                        password
                    </label>
                </div>
            </div>
            {showPassword ? <Draggable grid={[25, 25]}
                                       handle="strong"
                                       zIndex={100}
                                       defaultPosition={getCoordinates()}
                                       onStop={onStopDrag}>
                    <div onClick={onActiveWindow ? onActiveWindow : null} style={{position: 'fixed', top: 0, zIndex}}>
                        <div className="modal-wrapper" style={{
                        minWidth: '560px',
                        border:   zIndex === App.DEFAULT_Z_INDEX ? '1px solid #464646' : '1px solid #47c1f3',
                        outline:  zIndex === App.DEFAULT_Z_INDEX ? '1px solid #464646' : '1px solid #116bff',
                    }}>
                            <strong style={{cursor: 'move'}}>
                                <div className="modal-header" style={{backgroundColor: bColor}}>
                                    <h4 style={{float: 'left'}}>BCrypt</h4>
                                    <button onClick={onClose}>
                                        <span className="close-custom hairline" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </strong>
                            <div className="modal-body" style={{backgroundColor: bColor}}>
                                <div className="row">
                                    <div className="form-group has-info label-static col-lg-7">
                                        <label style={{marginLeft: '15px'}} className="control-label">
                                            Generate BCrypt Hash
                                        </label>
                                        <input value={bCrypt}
                                               onChange={onChangeName}
                                               type="text" className="form-control" placeholder=""/>
                                    </div>
                                    <div className="form-group has-info label-static col-lg-2">
                                        <label style={{marginLeft: '15px'}} className="control-label">
                                            Cost
                                        </label>
                                        <input value={bCryptCost}
                                               onChange={onChangeCost}
                                               type="number" className="form-control" placeholder=""/>
                                    </div>
                                    <button className="col-lg-3 btn btn-default" style={{margin: '30px 10px 0 -15px', float: 'right'}}
                                            onClick={onHashing}
                                            disabled={!!bCryptProgress}>
                                        {bCryptProgress ? (parseInt(bCryptProgress) + '%') : 'Generate'}
                                    </button>
                                </div>
                                {bCryptResult ?
                                    <div className="panel panel-default">
                                        <button style={{float: 'right'}}>
                                            <i className="fa fa-times" aria-hidden="true" onClick={onAlarmDismiss}></i>
                                        </button>
                                        <div className="panel-body row">
                                            <div className="col-lg-11">
                                                {bCryptResult}
                                            </div>
                                            <CopyToClipboard text={bCryptResult}
                                                             className="col-lg-1"
                                                             onCopy={onCopy}>
                                                <button style={{marginTop: '-8px'}}>
                                                    {copyOk
                                                        ? <i className="fa fa-check"></i>
                                                        : <i className="fa fa-clipboard"></i>}
                                                </button>
                                            </CopyToClipboard>&nbsp;
                                        </div>
                                    </div> : null}
                                <div className="row">
                                    <div className={"form-group has-info label-static col-lg-3 " + checkClass}>
                                        <label style={{marginLeft: '15px'}} className="control-label">
                                            Password
                                        </label>
                                        <input value={password}
                                               onChange={onChangePassword}
                                               type="text" className="form-control" placeholder=""/>
                                    </div>
                                    <div className={"form-group has-info label-static col-lg-7 " + errorClass}>
                                        <label style={{marginLeft: '15px'}} className="control-label">
                                            Hash
                                        </label>
                                        <input value={hash}
                                               onChange={onChangeHash}
                                               type="text" className="form-control" placeholder=""/>
                                    </div>
                                    <button className="col-lg-2 btn btn-default" style={{margin: '30px 10px 0 -15px', float: 'right'}}
                                            onClick={onCheckPassword}
                                            disabled={!!bCryptCheckProgress || !hash || !password}>
                                        {bCryptCheckProgress ? (parseInt(bCryptCheckProgress) + '%') : 'Check'}
                                    </button>
                                </div>
                                <div className="row">
                                    <div className="form-group has-info label-static col-lg-2">
                                        <label style={{marginLeft: '15px'}} className="control-label">
                                            Password Length
                                        </label>
                                        <input value={generatePasswordLength}
                                               onChange={onChangePasswordLength}
                                               type="number" className="form-control" placeholder=""/>
                                    </div>
                                    <div className="empty-btn col-lg-2">
                                        <div className="togglebutton">
                                            <label style={{padding: '10px 0 10px 10px', margin: 0}}>
                                                <div>Uppercase</div>
                                                <input onChange={onChangeUppercase}
                                                       style={{marginLeft: '10px'}}
                                                       type="checkbox"
                                                       checked={passwordUppercase}/>
                                                <span className="toggle"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div className="empty-btn col-lg-2">
                                        <div className="togglebutton">
                                            <label style={{padding: '10px 0 10px 10px', margin: 0}}>
                                                <div>Digits</div>
                                                <input onChange={onChangeDigits}
                                                       style={{marginLeft: '10px'}}
                                                       type="checkbox"
                                                       checked={passwordDigits}/>
                                                <span className="toggle"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div className="empty-btn col-lg-2">
                                        <div className="togglebutton">
                                            <label style={{padding: '10px 0 10px 10px', margin: 0}}>
                                                <div>Symbols</div>
                                                <input onChange={onChangeSymbols}
                                                       style={{marginLeft: '10px'}}
                                                       type="checkbox"
                                                       checked={passwordSymbols}/>
                                                <span className="toggle"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <button className="col-lg-3 btn btn-default" style={{margin: '30px 10px 0 -15px', float: 'right'}}
                                            onClick={onGeneratePassword}>
                                        Generate
                                    </button>
                                </div>
                                {generatedPassword ?
                                    <div className="panel panel-default">
                                        <button style={{float: 'right'}}>
                                            <i className="fa fa-times" aria-hidden="true"
                                               onClick={onCloseGeneratedPassword}></i>
                                        </button>
                                        <div className="panel-body row">
                                            <div className="col-lg-11">
                                                {generatedPassword}
                                            </div>
                                            <CopyToClipboard text={generatedPassword}
                                                             className="col-lg-1"
                                                             onCopy={onCopyGeneratedPassword}>
                                                <button style={{marginTop: '-8px'}}>
                                                    {copyGeneratedPasswordOk
                                                        ? <i className="fa fa-check"></i>
                                                        : <i className="fa fa-clipboard"></i>}
                                                </button>
                                            </CopyToClipboard>&nbsp;
                                        </div>
                                    </div> : null}
                                <div style={{clear: 'both'}}></div>
                            </div>
                        </div>
                    </div>
                </Draggable> : null}
        </div>;
    }
}

function mapStateToProps(state) {
    return {
        settings: state.settings[MENU_WINDOWS],
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(SettingsActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Password);