"use strict";

import * as React from "react";
import * as ReactToastr from "react-toastr";
import "./toastr.css";
import "animate.css";

const ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);

export default class Toastr extends React.Component {

    constructor() {
        super();
        $(document).on('TOAST:SHOW', (event, data) => {
            switch (data.class) {
                case ('success'):
                    this.refs.container.success(
                        data.data, '',
                        {
                            showAnimation: 'animated flipInX',
                            hideAnimation: 'animated flipOutX',
                            timeOut:       5000,
                        });
                    break;
                case ('warning'):
                    this.refs.container.error(
                        data.data, '',
                        {
                            showAnimation: 'animated flipInX',
                            hideAnimation: 'animated flipOutX',
                            timeOut:       7000,
                            closeButton:   true,
                        });
                    break;
                default:
                    this.refs.container.info(
                        data.data, '',
                        {
                            showAnimation: 'animated flipInX',
                            hideAnimation: 'animated flipOutX',
                            timeOut:       5000,
                            //extendedTimeOut: 10000
                        });
                    break;
            }
        });
    }

    componentWillUnmount() {
        $(document).off('TOAST:SHOW');
    }

    render() {
        return <div>
            <ReactToastr.ToastContainer ref="container"
                                        toastMessageFactory={ToastMessageFactory}
                                        className="toast-top-right"/>
        </div>;
    }
}