"use strict";

import * as React from "react";
import Draggable from "react-draggable";
import "./time.css";
import CopyToClipboard from "react-copy-to-clipboard";
import App from "../../App";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as SettingsActions from "./../../actions/settings";
import {MENU_WINDOWS} from "./../settings/Settings";
import ObjectId from './../../utils/MongoUtils'

export const DEFAULT_Y = 100;
export const COORDINATE_X = 'tX';
export const COORDINATE_Y = 'tY';

class Time extends React.Component {

    state = {
        copyTimeOk:           false,
        copyTimestampOk:      false,
        copyDateOk:           false,
        copyParseDateOk:      false,
        copyDateToMongoIDOk:  false,
        mongoID:              '',
        dateToParse:          '',
        dateToMongoID:        '',
        dateToMongoIDResult:  '',
        dateParseResult:      0,
        timestamp:            '',
        timestampResult:      '',
        datePattern:          'yyyy-MM-dd HH:mm:ss',
        datePatternTimestamp: 'yyyy-MM-dd HH:mm:ss',
        mongoIDResult:        new Date(),
        showTime:             '',
    };

    static defaultProps = {
        zIndex: 0
    };

    static propTypes = {
        zIndex:         React.PropTypes.number,
        onActiveWindow: React.PropTypes.func,
        settings:       React.PropTypes.shape({
            [COORDINATE_X]: React.PropTypes.number,
            [COORDINATE_Y]: React.PropTypes.number,
        }),
    };

    getCoordinates = () => {
        const xCoordinate = this.props.settings[COORDINATE_X];
        const yCoordinate = this.props.settings[COORDINATE_Y];
        return {
            x: !xCoordinate || xCoordinate > 1000 ? 0 : xCoordinate,
            y: !yCoordinate || yCoordinate > 1900 ? DEFAULT_Y : yCoordinate,
        };
    };

    onStopDrag = (...args) => {
        const {settings, changeSettings} = this.props;
        settings[COORDINATE_X] = args[1].x;
        settings[COORDINATE_Y] = args[1].y;
        changeSettings(settings, MENU_WINDOWS);
    };

    onChangeMongoID = event => {
        let value = event.target.value;
        this.setState({
            mongoID:       value,
            mongoIDResult: new Date(parseInt(value.substring(0, 8), 16) * 1000)
        });
    };

    onChangeTimestamp = event => {
        let value = event.target.value;
        if (/^\d+$/.test(value)) {
            this.setState({
                timestamp:       value,
                timestampResult: new Date(parseInt(value)).formatDate(this.state.datePatternTimestamp),
            });
        } else if (value === '') {
            this.setState({
                timestamp:       '',
                timestampResult: '',
            });
        }
    };

    onChangeDatePattern = event => this.setState({datePattern: event.target.value});
    onChangeDatePatternTimestamp = event => this.setState({datePatternTimestamp: event.target.value});
    onChangeDateToParse = event => this.setState({
        dateToParse:     event.target.value,
        dateParseResult: Date.parse(event.target.value)
    });

    onChangeDateParseToMongoID = event => this.setState({
        dateToMongoID:       event.target.value,
        dateToMongoIDResult: new ObjectId(Date.parse(event.target.value)).toString()
    });

    onClose = () => this.setState({showTime: false});

    onOpen = (event) => {
        event.stopPropagation();
        this.props.onActiveWindow();
        this.setState({showTime: !this.state.showTime})
    };

    onCopyTime = () => {
        this.setState({copyTimeOk: true});
        setTimeout(() => this.setState({copyTimeOk: false}), 1000)
    };

    onCopyDate = () => {
        this.setState({copyDateOk: true});
        setTimeout(() => this.setState({copyDateOk: false}), 1000)
    };

    onCopyTimestamp = () => {
        this.setState({copyTimestampOk: true});
        setTimeout(() => this.setState({copyTimestampOk: false}), 1000)
    };

    onCopyParseDate = () => {
        this.setState({copyParseDateOk: true});
        setTimeout(() => this.setState({copyParseDateOk: false}), 1000)
    };

    onCopyParseDateToMongoID = () => {
        this.setState({copyDateToMongoIDOk: true});
        setTimeout(() => this.setState({copyDateToMongoIDOk: false}), 1000)
    };

    renderMongoID = () => {
        const {mongoID, datePattern, copyTimeOk, copyTimestampOk, mongoIDResult} = this.state;
        const {onChangeDatePattern, onChangeMongoID, onCopyTime, onCopyTimestamp} = this;
        const date = mongoIDResult.formatDate(datePattern);
        const unixTimestamp = mongoIDResult.getTime();
        return <div className="border-time">
            <div className="row">
                <div className="form-group has-info label-static col-lg-7">
                    <label style={{marginLeft: '15px'}} className="control-label">
                        MongoID To Date
                    </label>
                    <input value={mongoID}
                           onChange={onChangeMongoID}
                           type="text" className="form-control" placeholder=""/>
                </div>
                <div className="form-group has-info label-static col-lg-5">
                    <label style={{marginLeft: '15px'}} className="control-label">
                        Date pattern
                    </label>
                    <input value={datePattern}
                           onChange={onChangeDatePattern}
                           type="text" className="form-control" placeholder=""/>
                </div>
            </div>
            {mongoID ? <div style={{color: '#9E9E9E', fontSize: '20px'}}>
                <div style={{float: 'left'}}>Date:</div>
                <div style={{float: 'right', color: 'aliceblue'}}>
                    {date} <CopyToClipboard text={date}
                                            onCopy={onCopyTime}>
                    <button style={{marginTop: '-8px', fontSize: '14px'}}>
                        {copyTimeOk
                            ? <i className="fa fa-check"></i>
                            : <i className="fa fa-clipboard"></i>}
                    </button>
                </CopyToClipboard>&nbsp;
                </div>
            </div> : null}
            {mongoID ? <div style={{marginTop: 28, color: '#9E9E9E', fontSize: '20px'}}>
                <div style={{float: 'left'}}>Timestamp:</div>
                <div style={{float: 'right', color: 'aliceblue'}}>
                    {unixTimestamp} <CopyToClipboard text={unixTimestamp + ''}
                                                     onCopy={onCopyTimestamp}>
                    <button style={{marginTop: '-8px', fontSize: '14px'}}>
                        {copyTimestampOk
                            ? <i className="fa fa-check"></i>
                            : <i className="fa fa-clipboard"></i>}
                    </button>
                </CopyToClipboard>&nbsp;
                </div>
                <br/>
            </div> : null}
                <br/>
        </div>;
    };

    renderTimestamp = () => {
        const {timestamp, timestampResult, datePatternTimestamp, copyDateOk} = this.state;
        const {onChangeTimestamp, onChangeDatePatternTimestamp, onCopyDate} = this;
        return <div className="border-time">
            <div className="row">
                <div className="form-group has-info label-static col-lg-7">
                    <label style={{marginLeft: '15px'}} className="control-label">
                        Timestamp To Date
                    </label>
                    <input value={timestamp}
                           onChange={onChangeTimestamp}
                           type="number" className="form-control" placeholder=""/>
                </div>
                <div className="form-group has-info label-static col-lg-5">
                    <label style={{marginLeft: '15px'}} className="control-label">
                        Date pattern
                    </label>
                    <input value={datePatternTimestamp}
                           onChange={onChangeDatePatternTimestamp}
                           type="text" className="form-control" placeholder=""/>
                </div>
            </div>
            {timestampResult ? <div style={{color: '#9E9E9E', fontSize: '20px'}}>
                <div style={{float: 'left'}}>Date:</div>
                <div style={{float: 'right', color: 'aliceblue'}}>
                    {timestampResult} <CopyToClipboard text={timestampResult}
                                                       onCopy={onCopyDate}>
                    <button style={{marginTop: '-8px', fontSize: '14px'}}>
                        {copyDateOk
                            ? <i className="fa fa-check"></i>
                            : <i className="fa fa-clipboard"></i>}
                    </button>
                </CopyToClipboard>&nbsp;
                </div>
                <br/>
            </div> : null}
                <br/>
        </div>;
    };

    renderParseData = () => {
        const {dateToParse, dateParseResult, copyParseDateOk} = this.state;
        const {onChangeDateToParse, onCopyParseDate} = this;
        return <div className="row">
            <div className="form-group has-info label-static col-lg-4">
                <label style={{marginLeft: '15px'}} className="control-label">
                    Date To Timestamp
                </label>
                <input value={dateToParse}
                       onChange={onChangeDateToParse}
                       type="text" className="form-control" placeholder=""/>
            </div>
            <div className="form-group has-info label-static col-lg-8">
                {dateParseResult ? <div style={{color: '#9E9E9E', fontSize: '20px'}}>
                    <div style={{float: 'left'}}>Timestamp:</div>
                    <div style={{float: 'right', color: 'aliceblue'}}>
                        {dateParseResult} <CopyToClipboard text={dateParseResult + ''}
                                                           onCopy={onCopyParseDate}>
                        <button style={{marginTop: '-8px', fontSize: '14px'}}>
                            {copyParseDateOk
                                ? <i className="fa fa-check"></i>
                                : <i className="fa fa-clipboard"></i>}
                        </button>
                    </CopyToClipboard>&nbsp;
                    </div>
                    <br/>
                </div> : null}
            </div>
            <br/>
            <br/>
        </div>;
    };

    renderParseDataToMongoID = () => {
        const {dateToMongoID, dateToMongoIDResult, copyDateToMongoIDOk} = this.state;
        const {onChangeDateParseToMongoID, onCopyParseDateToMongoID} = this;
        return <div className="row border-time">
            <div className="form-group has-info label-static col-lg-4">
                <label style={{marginLeft: '15px'}} className="control-label">
                    Date To MongoID
                </label>
                <input value={dateToMongoID}
                       onChange={onChangeDateParseToMongoID}
                       type="text" className="form-control" placeholder=""/>
            </div>
            <div className="form-group has-info label-static col-lg-8">
                {dateToMongoIDResult ? <div style={{color: '#9E9E9E', fontSize: '20px'}}>
                    <div style={{float: 'left'}}>MongoID:</div>
                    <div style={{float: 'right', color: 'aliceblue'}}>
                        {dateToMongoIDResult} <CopyToClipboard text={dateToMongoIDResult + ''}
                                                               onCopy={onCopyParseDateToMongoID}>
                        <button style={{marginTop: '-8px', fontSize: '14px'}}>
                            {copyDateToMongoIDOk
                                ? <i className="fa fa-check"></i>
                                : <i className="fa fa-clipboard"></i>}
                        </button>
                    </CopyToClipboard>&nbsp;
                    </div>
                    <br/>
                </div> : null}
            </div>
            <br/>
            <br/>
        </div>;
    };

    render() {
        const {showTime} = this.state;
        const {zIndex, onActiveWindow, settings} = this.props;
        const {
            onStopDrag, onClose, onOpen, getCoordinates, renderMongoID, renderTimestamp, renderParseData,
            renderParseDataToMongoID
        } = this;

        const {bc} = settings;
        const bColor = `rgba(${bc.r},${bc.g},${bc.b},${bc.a})`;

        return <div>
            <div style={{position: 'absolute', marginTop: '30px', zIndex: 10}} className="empty-btn">
                <div className="togglebutton">
                    <label style={{padding: '10px 0 10px 10px', margin: 0}}>
                        <input onClick={onOpen}
                               style={{marginLeft: '10px'}}
                               type="checkbox"
                               checked={showTime || false}/>
                        <span className="toggle"></span>
                        time
                    </label>
                </div>
            </div>
            {showTime ? <Draggable grid={[25, 25]}
                                   handle="strong"
                                   zIndex={100}
                                   defaultPosition={getCoordinates()}
                                   onStop={onStopDrag}>
                <div onClick={onActiveWindow ? onActiveWindow : null} style={{position: 'fixed', top: 0, zIndex}}>
                    <div className="modal-wrapper" style={{
                        minWidth: '580px',
                        border:   zIndex === App.DEFAULT_Z_INDEX ? '1px solid #464646' : '1px solid #47c1f3',
                        outline:  zIndex === App.DEFAULT_Z_INDEX ? '1px solid #464646' : '1px solid #116bff',
                    }}>
                        <strong style={{cursor: 'move'}}>
                            <div className="modal-header" style={{backgroundColor: bColor}}>
                                <h4 style={{float: 'left'}}>Time</h4>
                                <button onClick={onClose}>
                                    <span className="close-custom hairline" aria-hidden="true"></span>
                                </button>
                            </div>
                        </strong>
                        <div className="modal-body" style={{backgroundColor: bColor}}>
                            {renderMongoID()}
                            <div style={{clear: 'both'}}></div>
                            {renderParseDataToMongoID()}
                            <br/>
                            {renderTimestamp()}
                            <br/>
                            {renderParseData()}
                            <div style={{clear: 'both'}}></div>
                        </div>
                    </div>
                </div>
            </Draggable> : null}
        </div>;
    }
}

function mapStateToProps(state) {
    return {
        settings: state.settings[MENU_WINDOWS],
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(SettingsActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Time);