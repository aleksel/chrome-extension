"use strict";

const middleware = store => next => action => {
    if (action.type !== 'PROMISE') {
        return next(action);
    }

    const [startAction, successAction, failureAction] = action.actions;
    store.dispatch({type: startAction});

    action.promise().then(data => {
        if (typeof action.successMessage !== 'undefined') {
            $(document).trigger('TOAST:SHOW', {data: action.successMessage, class: 'success'});
        }
        if (typeof action.successCallback === 'function') {
            action.successCallback(data);
        }
        store.dispatch({
            type:                 successAction,
            payload:              data,
            additionalData:       action.additionalData,
            additionalDataSecond: action.additionalDataSecond,
        });
    }).catch(error => {
            if (typeof action.errorCallback === 'function') {
                action.errorCallback(error);
            }
            store.dispatch({type: failureAction, payload: error});
            if (typeof action.errorMessage !== 'undefined') {
                $(document).trigger('TOAST:SHOW', {data: action.errorMessage, class: 'warning'});
            }
        }
    );
};

export default middleware;