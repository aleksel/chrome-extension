// @flow
import {ISSUES_LOADED, ISSUES_LOADING, ISSUES_LOAD_FAILURE, ISSUES_IS_NEW_CHANGE_STATUS} from "../actions/issues";
import Storage, {NOTIFIED_ISSUES} from "../utils/Storage";

export default function issues(state: Object = {}, action: Object) {
    /*switch(action.type) {
     case ISSUES_CHANGE_STATUS_LOADED: {
     console.log(action.additionalData, state, state[action.additionalData.statusTo], state[action.additionalData.statusFrom]);
     }
     }*/
    if (action.type === ISSUES_IS_NEW_CHANGE_STATUS) {
        const {issueID, issueStatusID} = action.payload;
        const {elems} = state[issueStatusID];
        const elemsNew = [];

        elems.forEach(elem => {
            if (elem.id === issueID) {
                elem.isNew = false;
                elemsNew.push({...elem});
            } else {
                elemsNew.push(elem);
            }
        });
        return {
            ...state, ...{
                [issueStatusID]: {
                    elems:     elemsNew,
                    justIsNew: true,
                    count:     state[issueStatusID] && state[issueStatusID].count ? state[issueStatusID].count : 0,
                }
            }
        };
    } else if (action.type.indexOf(ISSUES_LOADING) === 0) {
        const status = action.type.substring(ISSUES_LOADING.length + 1);
        return {
            ...state, ...{
                [status]: {
                    elems:     state[status] && state[status].elems ? state[status].elems : [],
                    loading:   true,
                    isFetched: false,
                    count:     state[status] && state[status].count ? state[status].count : 0,
                    error:     null,
                }
            }
        };
    } else if (action.type.indexOf(ISSUES_LOADED) === 0) {
        if (action.payload && action.payload.issues && Array.isArray(action.payload.issues)) {
            const elems = action.payload.issues;
            const status = action.type.substring(ISSUES_LOADED.length + 1);
            const notifiedIssues = Storage.get(NOTIFIED_ISSUES, true, false) || {};
            let needSave = false;

            elems.forEach(elem => {
                if (action.additionalDataSecond === 0 && typeof action.additionalData === 'function') {
                    const dateAsNumber = Date.parse(elem.updated_on);
                    if (!notifiedIssues[elem.id] || notifiedIssues[elem.id] !== Date.parse(elem.updated_on)) {
                        needSave = true;
                        notifiedIssues[elem.id] = dateAsNumber;
                    }
                }
                if (!notifiedIssues[elem.id] || notifiedIssues[elem.id] !== Date.parse(elem.updated_on)) {
                    // 600 000 milliseconds default during update issue
                    if (Date.now() - Date.parse(elem.updated_on) < action.additionalDataSecond * 60 * 1000) {
                        elem.isNew = true;
                        if (typeof action.additionalData === 'function') {
                            setTimeout(() => action.additionalData(elem), 1000);
                        }
                    }
                }
            });
            if (needSave) {
                Storage.save({[NOTIFIED_ISSUES]: notifiedIssues}, true);
            }

            return {
                ...state, ...{
                    [status]: {
                        loading:   false,
                        isFetched: true,
                        count:     action.payload.total_count,
                        error:     null,
                        elems:     action.payload.issues,
                    }
                }
            };
        }
        return {
            ...state, ...{
                [action.type.substring(ISSUES_LOADED.length + 1)]: {loading: false, isFetched: true, error: null}
            }
        };
    } else if (action.type.indexOf(ISSUES_LOAD_FAILURE) === 0) {
        const status = action.type.substring(ISSUES_LOAD_FAILURE.length + 1);
        return {
            ...state, ...{
                [status]: {
                    elems:     state[status] && state[status].elems ? state[status].elems : [],
                    loading:   false,
                    count:     state[status] && state[status].count ? state[status].count : 0,
                    isFetched: true,
                    error:     action.payload.stack ? {errorMessage: action.payload.stack} : action.payload,
                }
            }
        };
    } else {
        return state;
    }
}