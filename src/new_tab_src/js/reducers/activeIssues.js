// @flow
import {
    ACTIVE_ISSUES_LOADED,
    ACTIVE_ISSUES_CHANGE
} from "./../actions/activeIssues";

export default function activeIssues(state: Object = {isFetched: false, loading: false, elems: []}, action: Object) {
    switch (action.type) {
        case ACTIVE_ISSUES_CHANGE:
            const {issueID, start} = action.payload;
            const {elems} = state;
            if (start) {
                elems.push({id: issueID, date: Date.now()});
            } else {
                let indexExists = null;
                elems.forEach((issue, index) => {
                    if (issue.id === issueID) {
                        indexExists = index;
                    }
                });
                elems.splice(indexExists, 1);
            }
            return {...state, ...{elems: [...elems]}};
        case ACTIVE_ISSUES_LOADED: {
            const elems = Array.isArray(action.payload) ? action.payload.filter(elem =>
            elem.id && elem.date && typeof elem.id === 'number' && typeof elem.date === 'number') : [];
            return {isFetched: true, loading: false, elems: elems};
        }
        default:
            return state;
    }
}
