// @flow
import {combineReducers} from "redux";
//import { routerReducer as routing } from 'react-router-redux';
import issues from './issues';
import users from './users';
import user from './user';
import issue from './issue';
import activities from './activities';
import name from './name';
import settings from './settings';
import statuses from './statuses';
import priorities from './priorities';
import trackers from './trackers';
import projects from './projects';
import geo from './geo';
import geoCity from './geoCity';
import activeIssues from './activeIssues';

const rootReducer = combineReducers({
    geo,
    geoCity,
    issues,
    issue,
    settings,
    activities,
    activeIssues,
    statuses,
    priorities,
    trackers,
    projects,
    name,
    users,
    user,
});

export default rootReducer;
