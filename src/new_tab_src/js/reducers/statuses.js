// @flow
import {STATUSES_LOAD_FAILURE, STATUSES_LOADED, STATUSES_LOADING} from "./../actions/dictionary";

const initState = {
    elems:     {},
    loading:   false,
    isFetched: false,
};

export default function statuses(state: Object = initState, action: Object) {
    switch (action.type) {
        case STATUSES_LOADING:
            return {...initState, ...{loading: true}};
        case STATUSES_LOADED: {
            if (action.payload && action.payload.issue_statuses) {
                const statuses = {};
                action.payload.issue_statuses.forEach(status => statuses[status.id] = status);
                return {...state, ...{elems: statuses}, loading: false, isFetched: true};
            }
            return state;
        }
        case STATUSES_LOAD_FAILURE: {
            let errorMessage = '';
            if (action.payload.stack) {
                errorMessage = action.payload.stack;
            } else if (action.payload.errorMessage) {
                errorMessage = action.payload.errorCode + ' - ' + action.payload.errorMessage;
            }
            $(document).trigger('TOAST:SHOW', {data: errorMessage, class: 'warning'});
            return {...initState};
        }
        default:
            return state;
    }
}
