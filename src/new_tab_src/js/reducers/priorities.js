// @flow
import {PRIORITIES_LOAD_FAILURE, PRIORITIES_LOADED, PRIORITIES_LOADING} from "./../actions/dictionary";

const initState = {
    elems:     {},
    loading:   false,
    isFetched: false,
};

export default function priorities(state: Object = initState, action: Object) {
    switch (action.type) {
        case PRIORITIES_LOADING:
            return {...initState, ...{loading: true}};
        case PRIORITIES_LOADED: {
            if (action.payload && action.payload.issue_priorities) {
                const priorities = {};
                action.payload.issue_priorities.forEach(priority => priorities[priority.id] = priority);
                return {...state, ...{elems: priorities}, loading: false, isFetched: true};
            }
            return state;
        }
        case PRIORITIES_LOAD_FAILURE: {
            let errorMessage = '';
            if (action.payload.stack) {
                errorMessage = action.payload.stack;
            } else if (action.payload.errorMessage) {
                errorMessage = action.payload.errorCode + ' - ' + action.payload.errorMessage;
            }
            $(document).trigger('TOAST:SHOW', {data: errorMessage, class: 'warning'});
            return {...initState};
        }
        default:
            return state;
    }
}
