// @flow
import {SETTINGS_LOADED, NAME_CREATED, SETTINGS_ALL_CHANGED, SETTINGS_CHANGED} from "./../actions/settings";
import {MENU_GENERAL} from "../component/settings/Settings";

const initState = true;
let nameEntered = false;

export default function name(state: boolean = initState, action: Object) {
    switch (action.type) {
        case SETTINGS_ALL_CHANGED:
            if (nameEntered && (!action.payload.settings[MENU_GENERAL] || !action.payload.settings[MENU_GENERAL].name)) {
                nameEntered = false;
                return true;
            }
            return state;
        case SETTINGS_CHANGED:
            if (nameEntered && action.payload.type === MENU_GENERAL
                && (!action.payload.settings || !action.payload.settings.name)) {
                nameEntered = false;
                return true;
            }
            return state;
        case NAME_CREATED:
            nameEntered = true;
            return false;
        case SETTINGS_LOADED:
            nameEntered = !!action.payload[MENU_GENERAL].name;
            return !action.payload[MENU_GENERAL].name;
        default:
            return state;
    }
}