// @flow
import {CITY_NAME_LOAD_FAILURE, CITY_NAME_LOADED, CITY_NAME_LOADING, COORDINATES_LOADED} from "./../actions/weather";

const initState = {
    city:      '',
    loading:   false,
    isFetched: false,
};

export default function geoCity(state: Object = initState, action: Object) {
    switch (action.type) {
        case CITY_NAME_LOADING:
            return {...initState, ...{loading: true}};
        case CITY_NAME_LOADED: {
            if (action.payload && action.payload.results) {
                for (let i = 0, count = action.payload.results.length; i < count; ++i) {
                    let loc = action.payload.results[i];
                    if (loc.types && loc.types.indexOf('locality') !== -1) {
                        return {...state, ...{city: loc.formatted_address, loading: false, isFetched: true}};
                    }
                }
            }
            return state;
        }
        case COORDINATES_LOADED: {
            if (action.payload && action.payload.results) {
                for (let i = 0, count = action.payload.results.length; i < count; ++i) {
                    let loc = action.payload.results[i];
                    if (loc.types && loc.types.indexOf('locality') !== -1 && loc.formatted_address) {
                        return {
                            ...state, ...{
                                city:      loc.formatted_address,
                                loading:   false,
                                isFetched: true
                            }
                        };
                    }
                }
            }
            return state;
        }
        case CITY_NAME_LOAD_FAILURE: {
            return {...initState};
        }
        default:
            return state;
    }
}
