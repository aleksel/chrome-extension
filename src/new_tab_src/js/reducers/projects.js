// @flow
import {PROJECTS_LOAD_FAILURE, PROJECTS_LOADED, PROJECTS_LOADING} from "./../actions/dictionary";

const initState = {
    elems:     {},
    loading:   false,
    isFetched: false,
};

export default function projects(state: Object = initState, action: Object) {
    switch (action.type) {
        case PROJECTS_LOADING:
            return {...initState, ...{loading: true}};
        case PROJECTS_LOADED: {
            if (action.payload && action.payload.projects) {
                const projects = {};
                action.payload.projects.forEach(project => projects[project.id] = project);
                return {...state, ...{elems: projects}, loading: false, isFetched: true};
            }
            return state;
        }
        case PROJECTS_LOAD_FAILURE: {
            let errorMessage = '';
            if (action.payload.stack) {
                errorMessage = action.payload.stack;
            } else if (action.payload.errorMessage) {
                errorMessage = action.payload.errorCode + ' - ' + action.payload.errorMessage;
            }
            $(document).trigger('TOAST:SHOW', {data: errorMessage, class: 'warning'});
            return {...initState};
        }
        default:
            return state;
    }
}
