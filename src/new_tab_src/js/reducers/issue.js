// @flow
import {
    ISSUE_LOADED,
    ISSUE_LOADING,
    ISSUE_LOAD_FAILURE,
    SAVE_SPEND_TIME_LOAD_FAILURE,
    SAVE_SPEND_TIME_LOADED,
    SAVE_ISSUE_LOAD_FAILURE,
    SAVE_ISSUE_LOADED
} from "../actions/issues";

export default function issue(state: Object = {}, action: Object) {
    switch (action.type) {
        case SAVE_ISSUE_LOAD_FAILURE:
        case SAVE_SPEND_TIME_LOAD_FAILURE: {
            let errorMessage = '';
            if (action.payload.stack) {
                errorMessage = action.payload.stack;
            } else if (action.payload.errorMessage) {
                errorMessage = action.payload.errorCode + ' - ' + action.payload.errorMessage;
            }
            $(document).trigger('TOAST:SHOW', {data: errorMessage, class: 'warning'});
            return state;
        }
        case SAVE_SPEND_TIME_LOADED:
            $(document).trigger('TOAST:SHOW', {
                data:  'Success saved time',
                class: 'success'
            });
            return state;
        case SAVE_ISSUE_LOADED:
            $(document).trigger('TOAST:SHOW', {
                data:  'Success saved changes',
                class: 'success'
            });
            return state;
    }
    if (action.type.indexOf(ISSUE_LOADING) === 0) {
        const issueID = action.type.substring(ISSUE_LOADING.length + 1);
        return {
            ...state, ...{
                [issueID]: {
                    elem:      {},
                    loading:   true,
                    isFetched: false,
                }
            }
        };
    } else if (action.type.indexOf(ISSUE_LOADED) === 0) {
        if (action.payload && action.payload.issue) {
            return {
                ...state, ...{
                    [action.type.substring(ISSUE_LOADED.length + 1)]: {
                        elem:      getSpendTimeFormat(action.payload.issue),
                        loading:   false,
                        isFetched: true,
                    }
                }
            };
        }
        return {
            ...state, ...{
                [action.type.substring(ISSUE_LOADED.length + 1)]: {loading: false, isFetched: true}
            }
        };
    } else if (action.type.indexOf(ISSUE_LOAD_FAILURE) === 0) {
        const issueID = action.type.substring(ISSUE_LOAD_FAILURE.length + 1);
        return {
            ...state, ...{
                [issueID]: {
                    elem:      state[issueID] ? state[issueID].elem : {},
                    loading:   false,
                    isFetched: true,
                    //error:     action.payload.stack ? {errorMessage: action.payload.stack} : action.payload,
                }
            }
        };
    } else {
        return state;
    }
}

const getSpendTimeFormat = (issue) => {
    if (issue.spent_hours === 0) {
        issue.spend = {
            minutes: 0,
            hours:   0,
        };
    } else {
        const spendHours = Math.floor(issue.spent_hours);
        const spendMinutes = Math.floor((Math.ceil(issue.spent_hours * 100)
            / 100 - spendHours) * 60);
        issue.spend = {
            minutes: spendMinutes,
            hours:   spendHours,
        };
    }

    return issue;
};
