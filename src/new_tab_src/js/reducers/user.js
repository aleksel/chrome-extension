// @flow
import {USER_LOAD_FAILURE, USER_LOADED, USER_LOADING} from "./../actions/dictionary";

const initState = {
    elem:      {},
    loading:   false,
    isFetched: false,
};

export default function users(state: Object = initState, action: Object) {
    switch (action.type) {
        case USER_LOADING:
            return {...initState, ...{loading: true}};
        case USER_LOADED: {
            if (action.payload && action.payload.user) {
                return {...state, ...{elem: action.payload.user}, loading: false, isFetched: true};
            }
            return state;
        }
        case USER_LOAD_FAILURE: {
            let errorMessage = '';
            if (action.payload.stack) {
                errorMessage = action.payload.stack;
            } else if (action.payload.errorMessage) {
                errorMessage = action.payload.errorCode + ' - ' + action.payload.errorMessage;
            }
            $(document).trigger('TOAST:SHOW', {data: errorMessage, class: 'warning'});
            return {...initState};
        }
        default:
            return state;
    }
}
