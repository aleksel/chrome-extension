// @flow
import {
    CLEAR_LOCATION,
    COORDINATES_LOAD_FAILURE,
    COORDINATES_LOADED,
    COORDINATES_LOADING,
    GEO_LOCATION_LOAD_FAILURE,
    GEO_LOCATION_LOADED,
    GEO_LOCATION_LOADING
} from "./../actions/weather";

const initState = {
    loc:       {},
    loading:   false,
    isFetched: false,
};

export default function geo(state: Object = initState, action: Object) {
    switch (action.type) {
        case CLEAR_LOCATION:
            return {...initState};
        case COORDINATES_LOADING:
        case GEO_LOCATION_LOADING:
            return {...initState, ...{loading: true}};
        case COORDINATES_LOADED: {
            if (action.payload && action.payload.results) {
                for (let i = 0, count = action.payload.results.length; i < count; ++i) {
                    let loc = action.payload.results[i];
                    if (loc.types && loc.types.indexOf('locality') !== -1 && loc.geometry && loc.geometry.location
                        && loc.geometry.location.lat && loc.geometry.location.lng) {
                        return {
                            ...state, ...{
                                loc:       {lat: loc.geometry.location.lat, lng: loc.geometry.location.lng},
                                loading:   false,
                                isFetched: true
                            }
                        };
                    }
                }
            }
            return state;
        }
        case GEO_LOCATION_LOADED: {
            if (action.payload && action.payload.location) {
                return {
                    ...state, ...{
                        loc:       {lat: action.payload.location.lat, lng: action.payload.location.lng},
                        loading:   false,
                        isFetched: true
                    }
                };
            }
            return state;
        }
        case COORDINATES_LOAD_FAILURE:
        case GEO_LOCATION_LOAD_FAILURE: {
            return {...initState};
        }
        default:
            return state;
    }
}
