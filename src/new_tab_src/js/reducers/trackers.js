// @flow
import {TRACKERS_LOAD_FAILURE, TRACKERS_LOADED, TRACKERS_LOADING} from "./../actions/dictionary";

const initState = {
    elems:     {},
    loading:   false,
    isFetched: false,
};

export default function trackers(state: Object = initState, action: Object) {
    switch (action.type) {
        case TRACKERS_LOADING:
            return {...initState, ...{loading: true}};
        case TRACKERS_LOADED: {
            if (action.payload && action.payload.trackers) {
                const trackers = {};
                action.payload.trackers.forEach(tracker => trackers[tracker.id] = tracker);
                return {...state, ...{elems: trackers}, loading: false, isFetched: true};
            }
            return state;
        }
        case TRACKERS_LOAD_FAILURE: {
            let errorMessage = '';
            if (action.payload.stack) {
                errorMessage = action.payload.stack;
            } else if (action.payload.errorMessage) {
                errorMessage = action.payload.errorCode + ' - ' + action.payload.errorMessage;
            }
            $(document).trigger('TOAST:SHOW', {data: errorMessage, class: 'warning'});
            return {...initState};
        }
        default:
            return state;
    }
}
