// @flow
import {USERS_LOADED, USERS_LOADING, USERS_LOAD_FAILURE} from "./../actions/dictionary";

const initState = {
    elems:     {},
    loading:   false,
    isFetched: false,
};

export default function users(state: Object = initState, action: Object) {
    switch (action.type) {
        case USERS_LOADING:
            return {...initState, ...{loading: true}};
        case USERS_LOADED: {
            if (action.payload && action.payload.users) {
                const users = {};
                action.payload.users.forEach(user => users[user.id] = user);
                return {...state, ...{elems: users}, loading: false, isFetched: true};
            }
            return state;
        }
        case USERS_LOAD_FAILURE: {
            let errorMessage = '';
            if (action.payload.stack) {
                errorMessage = action.payload.stack;
            } else if (action.payload.errorMessage) {
                errorMessage = action.payload.errorCode + ' - ' + action.payload.errorMessage;
            }
            $(document).trigger('TOAST:SHOW', {data: errorMessage, class: 'warning'});
            return {...initState};
        }
        default:
            return state;
    }
}
