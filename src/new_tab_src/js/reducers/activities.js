// @flow
import {ACTIVITIES_LOAD_FAILURE, ACTIVITIES_LOADED, ACTIVITIES_LOADING} from "./../actions/dictionary";

const initState = {
    elems:     [],
    loading:   false,
    isFetched: false,
};

export default function activities(state: Object = initState, action: Object) {
    switch (action.type) {
        case ACTIVITIES_LOADING:
            return {...initState, ...{loading: true}};
        case ACTIVITIES_LOADED: {
            if (action.payload && action.payload.time_entry_activities) {
                return {...state, ...{elems: [...action.payload.time_entry_activities]}};
            }
            return state;
        }
        case ACTIVITIES_LOAD_FAILURE: {
            let errorMessage = '';
            if (action.payload.stack) {
                errorMessage = action.payload.stack;
            } else if (action.payload.errorMessage) {
                errorMessage = action.payload.errorCode + ' - ' + action.payload.errorMessage;
            }
            $(document).trigger('TOAST:SHOW', {data: errorMessage, class: 'warning'});
            return {...initState};
        }
        default:
            return state;
    }
}
