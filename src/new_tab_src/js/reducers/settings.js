// @flow
import {BACKGROUND_URL, BACKGROUND_URL_DAY} from "./../component/Background";
import {
    COORDINATE_X as PASSWORD_COORDINATE_X,
    COORDINATE_Y as PASSWORD_COORDINATE_Y,
    DEFAULT_Y as PASSWORD_DEFAULT_Y
} from "./../component/password/Password";
import {
    COORDINATE_X as TIME_COORDINATE_X,
    COORDINATE_Y as TIME_COORDINATE_Y,
    DEFAULT_Y as TIME_DEFAULT_Y
} from "./../component/time/Time";
import {CHANGE_WALLPAPER_EVERY_DAY} from "./../component/settings/menu/Backgrounds";
import {MENU_BACKGROUNDS, MENU_GENERAL, MENU_WIDGETS, MENU_WINDOWS} from "./../component/settings/Settings";
import {REDMINE_WIDGET, TIMER_WIDGET, WEATHER_WIDGET} from "./../component/settings/menu/Widgets";
import {SETTINGS_ALL_CHANGED, SETTINGS_CHANGED, SETTINGS_LOADED, SETTINGS_SECOND_CHANGED} from "./../actions/settings";
import {CITY_NAME_LOADED, CLEAR_LOCATION, COORDINATES_LOADED, GEO_LOCATION_LOADED} from "./../actions/weather";
import {PIN_WIDGETS} from "../component/settings/menu/General";
import {SORT_DESC_DIRECTION} from "./../App";

const initState = {
    [MENU_GENERAL]:     {
        name:          '',
        [PIN_WIDGETS]: true,
        // AutoLaunch
        al:            0,
    },
    [MENU_WIDGETS]:     {
        [WEATHER_WIDGET]: {
            show: false,
            // location
            loc:  {},
            city: '',
            // backgroundColor
            bc:   {a: 0.72, r: 15, g: 15, b: 15},
            // header backgroundColor
            c:    {a: 1, r: 255, g: 255, b: 255},
        },
        [TIMER_WIDGET]:   {
            show: false,
            // timer list (key: time yyyy-MM-dd HH:mm:ss, value: header)
            t:    {},
            // layouts
            ls:   {},
            // inverse color
            ic:   0,
        },
        [REDMINE_WIDGET]: {
            // notify in minutes, 0 - off
            n:     15,
            // notify in minutes about active task, 0 - off
            na:    40,
            show:  false,
            // backgroundColor
            bc:    {a: 0.72, r: 15, g: 15, b: 15},
            // header backgroundColor
            hbc:   {a: 1, r: 51, g: 51, b: 51},
            // statuses
            ss:    {},
            // layouts
            // layouts.s: sort {f: keyField, d: direction [a, d]}
            ls:    {
                // decorate crossed issue subject
                //dt:    false,
                // default black theme
                //ic:    true,
                // active timer
                //ti:    false,
                // new issue
                //ni:    false,
            },
            // fetch count
            fc:    20,
            // time to auto update issues min
            ut:    1,
            url:   '',
            //qId:   '',
            token: ''
        }
    },
    [MENU_WINDOWS]:     {
        // show Time window toggle
        st:                      0,
        // show Password window toggle
        sp:                      0,
        // backgroundColor for windows
        bc:                      {a: 0.92, r: 15, g: 15, b: 15},
        [TIME_COORDINATE_X]:     0,
        [TIME_COORDINATE_Y]:     TIME_DEFAULT_Y,
        [PASSWORD_COORDINATE_X]: 0,
        [PASSWORD_COORDINATE_Y]: PASSWORD_DEFAULT_Y,
    },
    [MENU_BACKGROUNDS]: {
        [CHANGE_WALLPAPER_EVERY_DAY]: true,
        [BACKGROUND_URL]:             '',
        [BACKGROUND_URL_DAY]:         -1,
    }
};

export default function settings(state: Object = initState, action: Object) {
    switch (action.type) {
        case CITY_NAME_LOADED: {
            if (action.payload && action.payload.results) {
                const settings = {...state};
                for (let i = 0, count = action.payload.results.length; i < count; ++i) {
                    let loc = action.payload.results[i];
                    if (loc.types && loc.types.indexOf('locality') !== -1) {
                        settings[MENU_WIDGETS][WEATHER_WIDGET].city = loc.formatted_address;
                        return settings;
                    }
                }
            }
            return state;
        }
        case COORDINATES_LOADED: {
            if (action.payload && action.payload.results) {
                const settings = {...state};
                for (let i = 0, count = action.payload.results.length; i < count; ++i) {
                    let loc = action.payload.results[i];
                    if (loc.types && loc.types.indexOf('locality') !== -1) {
                        settings[MENU_WIDGETS][WEATHER_WIDGET].city = loc.formatted_address ? loc.formatted_address : '';
                        if (loc.geometry && loc.geometry.location && loc.geometry.location.lat && loc.geometry.location.lng) {
                            settings[MENU_WIDGETS][WEATHER_WIDGET].loc = {
                                lat: loc.geometry.location.lat,
                                lng: loc.geometry.location.lng
                            }
                        }
                        return {
                            ...state, ...{
                                city:      loc.formatted_address ? loc.formatted_address : '',
                                loading:   false,
                                isFetched: true
                            }
                        };
                    }
                }
            }
            return state;
        }
        case GEO_LOCATION_LOADED:
            if (action.payload && action.payload.location) {
                const settings = {...state};
                settings[MENU_WIDGETS][WEATHER_WIDGET].loc = {
                    lat: action.payload.location.lat,
                    lng: action.payload.location.lng
                };
                return settings;
            }
            return state;
        case CLEAR_LOCATION: {
            const settings = {...state};
            settings[MENU_WIDGETS][WEATHER_WIDGET].loc = {};
            return settings;
        }
        case SETTINGS_ALL_CHANGED:
            return createTimerLayoutsSettings(
                createRedmineLayoutsSettings(
                    JSON.parse(JSON.stringify(action.payload.settings))
                )
            );
        case SETTINGS_SECOND_CHANGED: {
            const settings = {...state};
            settings[action.payload.type] = {
                ...settings[action.payload.type],
                ...{[action.payload.secondType]: action.payload.settings}
            };
            const value = action.payload.type === MENU_WIDGETS
                ? createTimerLayoutsSettings(createRedmineLayoutsSettings(settings)) : settings;
            return JSON.parse(JSON.stringify(value));
        }
        case SETTINGS_CHANGED: {
            const settings = {...state, ...{[action.payload.type]: action.payload.settings}};
            return action.payload.type === MENU_WIDGETS
                ? createTimerLayoutsSettings(createRedmineLayoutsSettings(settings)) : settings;
        }
        case SETTINGS_LOADED:
            const settings = {...state};
            mergeSettings(settings, action.payload);
            return createTimerLayoutsSettings(createRedmineLayoutsSettings(settings));
        default:
            return state;
    }
}

const mergeSettings = (defaultObject, gotObject) => {
    for (let item in defaultObject) {
        if (defaultObject.hasOwnProperty(item)) {
            if (gotObject[item] !== undefined) {
                if (!Array.isArray(gotObject[item]) && typeof gotObject[item] === 'object') {
                    if (!Object.keys(defaultObject[item]).length) {
                        defaultObject[item] = gotObject[item];
                    } else {
                        mergeSettings(defaultObject[item], gotObject[item]);
                    }
                } else {
                    defaultObject[item] = gotObject[item];
                }
            }
        }
    }
};

const createRedmineLayoutsSettings = settings => {
    const redmineWidget = settings[MENU_WIDGETS][REDMINE_WIDGET];
    let settingsChanged = false;
    if (settings && redmineWidget && redmineWidget.ss && redmineWidget.show) {
        for (let status in redmineWidget.ss) {
            if (redmineWidget.ss.hasOwnProperty(status)
                && redmineWidget.ss[status].show) {
                if (!redmineWidget.ls[status]) {
                    settingsChanged = true;
                    redmineWidget.ls[status] = {
                        i:    REDMINE_WIDGET + status,
                        x:    10 * 2 * (Object.keys(redmineWidget.ls).length + 1) - 10,
                        y:    5,
                        w:    20,
                        h:    20,
                        s:    [{f: '2', d: SORT_DESC_DIRECTION, i: 1}],
                        minW: 15,
                        minH: 15,
                    };
                }
            }
        }
        if (settingsChanged) settings[MENU_WIDGETS][REDMINE_WIDGET] = redmineWidget;
    }
    return settings;
};

const createTimerLayoutsSettings = settings => {
    const timerWidget = settings[MENU_WIDGETS][TIMER_WIDGET];
    let settingsChanged = false;
    if (settings && timerWidget && timerWidget.show) {
        for (let date in timerWidget.t) {
            if (timerWidget.t.hasOwnProperty(date)) {
                if (!timerWidget.ls[date]) {
                    settingsChanged = true;
                    timerWidget.ls[date] = {
                        i:    TIMER_WIDGET + date,
                        x:    5,
                        y:    2 * (Object.keys(timerWidget.ls).length + 1),
                        w:    28,
                        h:    10,
                        minW: 28,
                        minH: 10,
                    };
                }
            }
        }
        if (settingsChanged) settings[MENU_WIDGETS][TIMER_WIDGET] = timerWidget;
    }
    return settings;
};
