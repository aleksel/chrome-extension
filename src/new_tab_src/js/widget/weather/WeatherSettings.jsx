"use strict";

import * as React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as WeatherActions from "../../actions/weather";
import "./weather.css";

let SketchPicker;

class WeatherSettings extends React.Component {

    constructor(props) {
        super(props);
        require.ensure([], function (require) {
            SketchPicker = require('react-color').SketchPicker;
        }.bind(this), 'react-color');
        const {settings} = props;
        this.state = {city: settings.city ? settings.city : ''}
    }

    static propTypes = {
        header:   React.PropTypes.string,
        onChange: React.PropTypes.func.isRequired,
        settings: React.PropTypes.shape({}).isRequired,
    };

    onChange = field => event => {
        const {settings} = this.props;
        if (field === 'bc' || field === 'c') {
            settings[field] = event.rgb;
        } else {
            settings[field] = event.target.value;
        }
        this.props.onChange(settings);
    };

    getLocation = event => {
        const {locLoading, cityLoading, getGeolocation, getCityNameByCoordinates} = this.props;
        if (!locLoading && !cityLoading) {
            getGeolocation(data => getCityNameByCoordinates(data.location.lat, data.location.lng));
        }
    };

    getLocationByCityName = event => {
        const {city} = this.state;
        const {locLoading, cityLoading, getCoordinatesByCityName} = this.props;
        if (!locLoading && !cityLoading) {
            getCoordinatesByCityName(city);
        }
    };

    onChangeCity = event => {
        const {clearLoc, settings} = this.props;
        if (Object.keys(settings.loc).length) {
            clearLoc();
        }
        this.setState({city: event.target.value})
    };

    componentWillReceiveProps(props) {
        const {settings} = props;
        if (settings.city && this.state.city !== settings.city) {
            this.setState({city: settings.city});
        }
    }

    render() {
        const {settings, locLoading, cityLoading} = this.props;
        const {onChange, getLocation, onChangeCity, getLocationByCityName} = this;
        const {city} = this.state;

        return <div>
            <br/>
            <br/>
            <div className="row">
                <div className="default-text-color has-info col-lg-12 col-md-12 col-sm-12 col-xs-12"
                     style={{margin: 0}}>
                        <span>Coordinates: {Object.keys(settings.loc).length ?
                            <span className="has-success">
                                {settings.loc.lat}, {settings.loc.lng}
                            </span>
                            : <span className="has-error">
                                -- unknown
                            </span>}
                            </span>
                </div>
            </div>
            <br/>
            <div className="row">
                <div className="form-group has-info col-lg-8 col-md-8 col-sm-8 col-xs-8" style={{margin: 0}}>
                    <label style={{marginLeft: '15px'}} className="control-label default-text-color">
                        <input value={city}
                               onChange={onChangeCity}
                               type="text" className="form-control default-text-color" placeholder=""/>
                        <sub className="default-text-color">City</sub>
                    </label>
                </div>
                <button style={{float: 'right', marginRight: '15px', marginLeft: -10, color: '#bbb'}}
                        className="btn btn-default"
                        disabled={locLoading || cityLoading || !city}
                        onClick={getLocationByCityName}>
                    {locLoading || cityLoading
                        ? <span>Loading... <i className='fa fa-spinner fa-spin'></i></span> : 'Check'}
                </button>
            </div>
            <div className="row">
                <button style={{float: 'right', marginRight: '15px', color: '#bbb'}}
                        className="btn btn-default"
                        disabled={locLoading || cityLoading}
                        onClick={getLocation}>
                    {locLoading || cityLoading
                        ? <span>Loading... <i className='fa fa-spinner fa-spin'></i></span> : 'Locate'}
                </button>
            </div>
            <br/>
            <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <h6 style={{padding: '0 5px', color: '#ccc'}}>Color</h6>
                    <SketchPicker color={settings.c} onChangeComplete={onChange('c')}/>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <h6 style={{padding: '0 5px', color: '#ccc'}}>Background color</h6>
                    <SketchPicker color={settings.bc} onChangeComplete={onChange('bc')}/>
                </div>
            </div>
        </div>;
    }
}

function mapStateToProps(state, props) {
    return {
        loc:           state.geo.loc,
        locIsFetched:  state.geo.isFetched,
        locLoading:    state.geo.loading,
        city:          state.geoCity.city,
        cityIsFetched: state.geoCity.isFetched,
        cityLoading:   state.geoCity.loading,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(WeatherActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(WeatherSettings);