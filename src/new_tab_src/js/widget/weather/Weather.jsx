"use strict";

import * as React from "react";

let FlipClockInstance = false;

export const WIDGET_TIMER_WIDTH = 507;
export const WIDGET_TIMER_HEIGHT = 190;

export default class Timer extends React.Component {

    state = {
        wScale: 1, hScale: 1,
    };

    componentDidMount() {
        window.addEventListener('resize', this.onWindowResize);
        if (!FlipClockInstance) {
            require.ensure([], function (require) {
                require('./flipclock.css');

            }.bind(this), 'script!./flipclock.css');
            require.ensure([], function (require) {
                require('flipclock/compiled/flipclock');
                FlipClockInstance = true;
                Timer.getWidgetElem(this.props.date).FlipClock(this.getStartTime(this.props.date), {
                    clockFace:   'DailyCounter',
                    countdown:   true,
                    showSeconds: false
                });

            }.bind(this), 'script!./../../../../../node_modules/flipclock/compiled/flipclock');
        } else {
            Timer.getWidgetElem(this.props.date).FlipClock(this.getStartTime(this.props.date), {
                clockFace:   'DailyCounter',
                countdown:   true,
                showSeconds: false
            });
        }
    }

    getStartTime = date => {
        const lastTime = (Date.parse(date) - Date.now()) / 1000;
        return lastTime > 0 ? lastTime : 0;
    };

    static getWidgetElem = key => $(`#${Timer.getWidgetID(key)}`);
    static getWidgetID = key => `widget-timer-${key.replace(/[\s:]/g, '')}`;

    componentWillUnmount() {
        window.removeEventListener('resize', this.onWindowResize);
    }

    componentWillReceiveProps(props) {
        const {wScale, hScale} = props;
        this.setState({wScale, hScale});
    }

    onWindowResize = () => {
        const {date} = this.props;
        const node = Timer.getWidgetElem(date).parent()[0];
        this.setState({wScale: node.offsetWidth / WIDGET_TIMER_WIDTH, hScale: node.offsetHeight / WIDGET_TIMER_HEIGHT});
    };

    static defaultProps = {
        date:   '',
        header: '',
        wScale: 1,
        hScale: 1,
    };

    static propTypes = {
        date:         React.PropTypes.string.isRequired,
        header:       React.PropTypes.string.isRequired,
        wScale:       React.PropTypes.number,
        inverseColor: React.PropTypes.number,
        hScale:       React.PropTypes.number,
    };

    render() {
        const {date, header, inverseColor} = this.props;
        const {wScale, hScale} = this.state;
        return <div className={"widget-header header-transparent" + (inverseColor === 0 ? '' : ' inverse-color-timer')}>
            <div style={{fontSize: 23 * wScale}} className="timer-header">{header}</div>
            <div id={Timer.getWidgetID(date)}
                 style={{transformOrigin: '0 0', transform: `scale(${wScale},${hScale})`}}></div>
        </div>;
    }
}