"use strict";

import * as React from "react";
import {Modal} from "react-bootstrap";

const INIT_STATE = {
    errors:     {},
    priorities: [],
    users:      [],
    projects:   [],
    trackers:   [],
    showdown:   null,
    textile:    null,
    issue:      {
        subject:        '',
        assigned_to_id: '',
        priority_id:    '',
        tracker_id:     '',
        status_id:      '',
        project_id:     '',
        description:    '',
    }
};

export default class ModalNewIssue extends React.Component {

    state = {...INIT_STATE};

    constructor(props) {
        super(props);
        require.ensure([], function (require) {
            const showdown = require('showdown');
            this.setState({showdown: new showdown.Converter()});
        }.bind(this), 'showdown');
        require.ensure([], function (require) {
            this.setState({textile: require('textile-js')});
        }.bind(this), 'textile-js');
    }

    componentWillReceiveProps(props) {
        this.init(props);
    }

    componentDidMount() {
        this.init(this.props);
    }

    init = props => {
        const {issue} = this.state;
        const {users, priorities, projects, trackers, user} = props;

        const prioritiesArray = [];
        const usersArray = [];
        const projectsArray = [];
        const trackersArray = [];

        for (let tracker in trackers) {
            if (trackers.hasOwnProperty(tracker)) {
                trackersArray.push(trackers[tracker]);
            }
        }
        trackersArray.sort((u1, u2) => {
            if (u1.name === u2.name) return 0;
            return u1.name > u2.name ? 1 : -1;
        });

        for (let user in users) {
            if (users.hasOwnProperty(user)) {
                usersArray.push(users[user]);
            }
        }
        usersArray.sort((u1, u2) => {
            if (u1.lastname === u2.lastname) return 0;
            return u1.lastname > u2.lastname ? 1 : -1;
        });

        for (let priority in priorities) {
            if (priorities.hasOwnProperty(priority)) {
                prioritiesArray.push(priorities[priority]);
            }
        }
        prioritiesArray.sort((p1, p2) => {
            if (p1.id === p2.id) return 0;
            return p1.id > p2.id ? 1 : -1;
        });

        for (let project in projects) {
            if (projects.hasOwnProperty(project)) {
                projectsArray.push(projects[project]);
            }
        }
        projectsArray.sort((p1, p2) => {
            if (p1.name === p2.name) return 0;
            return p1.name > p2.name ? 1 : -1;
        });

        this.setState({
            priorities: prioritiesArray,
            trackers:   trackersArray,
            users:      usersArray,
            projects:   projectsArray,
            issue:      {
                ...issue, ...{
                    assigned_to_id: user || '',
                    project_id:     projectsArray.length ? projectsArray[0].id : '',
                    priority_id:    prioritiesArray.length ? prioritiesArray[0].id : '',
                    tracker_id:     trackersArray.length ? trackersArray[0].id : '',
                }
            }
        });
    };

    static propTypes = {
        onHideModal: React.PropTypes.func.isRequired,
        onSave:      React.PropTypes.func.isRequired,
        showModal:   React.PropTypes.bool.isRequired,
    };

    onSave = () => {
        const {issue, errors} = this.state;
        const {status, onSave} = this.props;
        if (!issue.subject) {
            this.setState({errors: {...errors, ...{subject: 'required'}}});
            return;
        }

        this.setState({...INIT_STATE}, () => onSave({...issue, ...{status_id: parseInt(status)}}));
    };

    onHideModal = () => this.setState({...INIT_STATE}, () => this.props.onHideModal());

    render() {
        const {showModal, statusName} = this.props;
        const {onSave, onHideModal} = this;
        const {issue, errors, users, priorities, projects, trackers, textile, showdown} = this.state;

        return <Modal show={showModal} backdrop={true} onHide={onHideModal} bsSize="lg">
            <Modal.Header>
                <h3 style={{color: '#fff', margin: '4px 0 4px 2%', fontSize: 15}}>
                    Create new issue. Status: <b>{statusName}
                </b></h3>
            </Modal.Header>
            <Modal.Body>
                <div className="row">
                    <div className={"form-group has-info col-lg-12" + (errors.subject ? ' has-error' : '')}>
                        <label style={{margin: 0, width: '100%'}}
                               className="control-label default-text-color">
                    <textarea value={issue.subject}
                              style={{height: '100px', width: '100hw'}}
                              onChange={event => this.setState({issue: {...issue, ...{subject: event.target.value}}})}
                              type="text" className="form-control default-text-color"
                              placeholder=""/>
                            <sub className="default-text-color">Subject</sub>
                            {errors.subject ? <sub style={{marginLeft: 5}}
                                                   className="default-text-color">{errors.subject}</sub> : null}
                        </label>
                    </div>
                    <div className="form-group has-info col-lg-3">
                        <label style={{margin: 0, width: '100%'}} className="control-label default-text-color">
                            <select value={issue.assigned_to_id}
                                    onChange={event => this.setState({
                                        issue: {
                                            ...issue,
                                            ...{
                                                assigned_to_id: event.target.value !== 'me'
                                                                    ? parseInt(event.target.value) : event.target.value
                                            }
                                        }
                                    })}
                                    type="text" className="form-control default-text-color"
                                    placeholder="">
                                {users.map(user => <option key={user.id}
                                                           value={user.id}>{user.lastname} {user.firstname}</option>)}
                            </select>
                            <sub className="default-text-color">Assign</sub>
                        </label>
                    </div>
                    <div className="form-group has-info col-lg-3">
                        <label style={{margin: 0, width: '100%'}} className="control-label default-text-color">
                            <select value={issue.priority_id}
                                    onChange={event =>
                                        this.setState({issue: {...issue, ...{priority_id: parseInt(event.target.value)}}})}
                                    type="text" className="form-control default-text-color"
                                    placeholder="">
                                {priorities.map(priority => <option key={priority.id}
                                                                    value={priority.id}>{priority.name}</option>)}
                            </select>
                            <sub className="default-text-color">Priority</sub>
                        </label>
                    </div>
                    <div className="form-group has-info col-lg-3">
                        <label style={{margin: 0, width: '100%'}} className="control-label default-text-color">
                            <select value={issue.project_id}
                                    onChange={event =>
                                        this.setState({issue: {...issue, ...{project_id: parseInt(event.target.value)}}})}
                                    type="text" className="form-control default-text-color"
                                    placeholder="">
                                {projects.map(project => <option key={project.id}
                                                                 value={project.id}>{project.name}</option>)}
                            </select>
                            <sub className="default-text-color">Project</sub>
                        </label>
                    </div>
                    <div className="form-group has-info col-lg-3">
                        <label style={{margin: 0, width: '100%'}} className="control-label default-text-color">
                            <select value={issue.tracker_id}
                                    onChange={event =>
                                        this.setState({issue: {...issue, ...{tracker_id: parseInt(event.target.value)}}})}
                                    type="text" className="form-control default-text-color"
                                    placeholder="">
                                {trackers.map(tracker => <option key={tracker.id}
                                                                 value={tracker.id}>{tracker.name}</option>)}
                            </select>
                            <sub className="default-text-color">Tracker</sub>
                        </label>
                    </div>
                    <div className="form-group has-info col-lg-12">
                        <div className="form-group has-info">
                            <label style={{margin: 0, width: '100%'}} className="control-label default-text-color">
                                <textarea value={issue.description}
                                          style={{height: '300px'}}
                                          onChange={event => this.setState({issue: {...issue, ...{description: event.target.value}}})}
                                          type="text" className="form-control default-text-color"
                                          placeholder=""/>
                                <sub className="default-text-color">Description</sub>
                            </label>
                            {textile && showdown ? <div dangerouslySetInnerHTML={{
                                __html: textile(showdown.makeHtml(issue.description))
                            }}></div> : null}
                        </div>
                    </div>
                </div>
            </Modal.Body>
            <Modal.Footer>
                <button className="btn btn-flat" onClick={onSave}>Save</button>
                <button className="btn btn-flat" onClick={onHideModal}>Cancel</button>
            </Modal.Footer>
        </Modal>;
    }
}