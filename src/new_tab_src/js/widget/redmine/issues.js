export default {
    "issues":          [{
        "id":              4179,
        "project":         {"id": 38, "name": "Профилирование"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 4, "name": "Срочный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 4145},
        "subject":         "Пишем UI для stats профилирования",
        "description":     "Пишем UI для stats профилирования",
        "start_date":      "2016-08-01",
        "due_date":        "2016-12-13",
        "done_ratio":      91,
        "estimated_hours": 163.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-08-01T10:04:36Z",
        "updated_on":      "2016-12-12T11:55:53Z"
    }, {
        "id":              4182,
        "project":         {"id": 38, "name": "Профилирование"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 4, "name": "Срочный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "fixed_version":   {"id": 57, "name": "Релиз 10"},
        "parent":          {"id": 4179},
        "subject":         "Реализация UI раздела Профилирования",
        "description":     "Реализация UI раздела Профилирования\r\nПроверка API, перепись на базу ClickHouse\r\n\r\n*Пункты (разделы) основного меню интерфейса:*\r\n\r\nСтатистика\r\nКатегории\r\nПодкатегории\r\nСайты",
        "start_date":      "2016-09-26",
        "due_date":        "2016-09-29",
        "done_ratio":      90,
        "estimated_hours": 134.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-08-01T10:08:51Z",
        "updated_on":      "2016-11-09T09:45:35Z"
    }, {
        "id":              4082,
        "project":         {"id": 19, "name": "Блогосфера"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 3, "name": "Высокий"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "fixed_version":   {"id": 58, "name": "Релиз 2. Итерация 3"},
        "subject":         "Перепись АПИ блогосферы",
        "description":     "Бэк выкатывать на стенд\r\nhttp://taskmanager.stage.local.px-l.ru/",
        "start_date":      "2016-07-18",
        "done_ratio":      32,
        "estimated_hours": 170.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-07-18T10:16:12Z",
        "updated_on":      "2016-12-01T08:59:49Z"
    }, {
        "id":              5569,
        "project":         {"id": 8, "name": "Новостной виджет CityNews"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 4146},
        "subject":         "Доработка АПИ методов",
        "description":     "1.  Добавить методы для раздельного получения данных по: Статистики виджета по показам и по переходам\r\n2. Добавить сортировку в статистике по виджету\r\n3. Добавить метод для выгрузки конкретной страницы статистики показов и переходов\r\n4. Добавить дефолтную сортировку для доменов",
        "start_date":      "2016-12-12",
        "done_ratio":      0,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-12-12T12:27:34Z",
        "updated_on":      "2016-12-12T13:31:46Z"
    }, {
        "id":              5037,
        "project":         {"id": 11, "name": "StatS"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 5036},
        "subject":         "Выгрузка статистики по итогам показа поп-апа \"Новый адрес ПГУ\" ",
        "description":     "Прекратить показ поп-апа 15.12.2016 в 10:00 a.m.\r\n\r\nПосле чего - подготовить таблицу с данными о показах поп-апа и кликах по ссылке в поп-апе - за весь период его показа (с 2.11 по 15.12).\r\n\r\nФормат таблицы - cvs с полями:\r\n1) mosid \r\n2) дата и время просмотра поп-апа данным mosid \r\n3) факт перехода по ссылке в поп-апе данным mosid  - true или <пусто>\r\n4) дата и время перехода по ссылке в поп-апе данным mosid \r\n5) ssoid (идентификатор, принадлежащий mosid - может быть несколько - указывать все через разделитель)",
        "start_date":      "2016-11-02",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-02T09:49:54Z",
        "updated_on":      "2016-12-06T11:22:05Z"
    }, {
        "id":              5073,
        "project":         {"id": 19, "name": "Блогосфера"},
        "tracker":         {"id": 11, "name": "Ошибка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 33, "name": "Вадим Ломако"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 5002},
        "subject":         "ЛК Агента. Темы. Не отображается категория отчета.",
        "description":     "*Стенд:*\r\nhttp://pxl-bgsphere.stage.local.px-l.ru/\r\n\r\n*Описание дефекта:*\r\nВ списке тем не отображается категория отчета.\r\n\r\n!https://pp.vk.me/c637422/v637422512/1e8d7/mkiHKJALOdo.jpg!",
        "start_date":      "2016-11-07",
        "done_ratio":      0,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    20,
            "name":  "Время на постановку",
            "value": ""
        }],
        "created_on":      "2016-11-07T12:51:37Z",
        "updated_on":      "2016-12-01T08:54:36Z"
    }, {
        "id":              5290,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 13, "name": "Андрей Иванов"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Исправление ошибок в web-морде КД-К",
        "description":     "В интерфейсе КД-К перестала работать функция ручной склейки и выгрузки в excel результатов поиска, нужно пофиксить косяки :)",
        "start_date":      "2016-11-24",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Prod"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-24T14:06:49Z",
        "updated_on":      "2016-12-01T08:54:14Z"
    }, {
        "id":              5373,
        "project":         {"id": 19, "name": "Блогосфера"},
        "tracker":         {"id": 11, "name": "Ошибка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 5120},
        "subject":         "Валидировать урлы блога на уникальность",
        "description":     "",
        "start_date":      "2016-11-30",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    20,
            "name":  "Время на постановку",
            "value": ""
        }],
        "created_on":      "2016-11-30T12:44:09Z",
        "updated_on":      "2016-12-01T08:53:14Z"
    }, {
        "id":              5379,
        "project":         {"id": 19, "name": "Блогосфера"},
        "tracker":         {"id": 11, "name": "Ошибка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 5078},
        "subject":         "Валидация дат отчетов на бэке",
        "description":     "При редактировании периода отчетности сделать валидацию на даты",
        "start_date":      "2016-11-30",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    20,
            "name":  "Время на постановку",
            "value": ""
        }],
        "created_on":      "2016-11-30T13:41:28Z",
        "updated_on":      "2016-12-01T08:53:04Z"
    }, {
        "id":              4284,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "В еластик сохраняется пустое поле search_value, выпилить",
        "description":     "В еластик сохраняется пустое поле search_value, выпилить",
        "start_date":      "2016-08-11",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-08-11T10:57:03Z",
        "updated_on":      "2016-11-28T09:03:43Z"
    }, {
        "id":              5270,
        "project":         {"id": 12, "name": "Cистема и планы"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Актуализировать список проектов и площадок",
        "description":     "",
        "start_date":      "2016-11-23",
        "due_date":        "2016-11-30",
        "done_ratio":      0,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-23T09:12:00Z",
        "updated_on":      "2016-11-23T09:12:00Z"
    }, {
        "id":              5242,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Нужен еще один контейнер (отмечаем как свойство) для лайаутов",
        "description":     "",
        "start_date":      "2016-11-22",
        "done_ratio":      0,
        "estimated_hours": 3.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-22T14:08:42Z",
        "updated_on":      "2016-11-22T14:08:42Z"
    }, {
        "id":              5241,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Добавить \"Сложные\" параметры для компонентов/лейаутов",
        "description":     "1. List<Options> для элементов Spinner, Checkbox, Radio\r\n2. List<Validate>",
        "start_date":      "2016-11-22",
        "done_ratio":      0,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-22T14:05:46Z",
        "updated_on":      "2016-11-22T14:05:46Z"
    }, {
        "id":              5239,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Добавить в конфиг приложения welcomTransition",
        "description":     "",
        "start_date":      "2016-11-22",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-22T14:02:30Z",
        "updated_on":      "2016-11-22T14:02:30Z"
    }, {
        "id":              4931,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Auth На каждой странице",
        "description":     "- На каждой странице необходимо добавить объект\r\nAuth {\r\n     ....\r\n}\r\nс возможностью редактирования полей",
        "start_date":      "2016-10-19",
        "done_ratio":      0,
        "estimated_hours": 3.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-10-19T17:45:12Z",
        "updated_on":      "2016-10-19T17:45:31Z"
    }, {
        "id":              4921,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Подумать над возможностью получения данных из библиотека мобильщиков",
        "description":     "",
        "start_date":      "2016-10-19",
        "done_ratio":      0,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-10-19T17:31:57Z",
        "updated_on":      "2016-10-19T17:31:57Z"
    }, {
        "id":              4917,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Обновление дата соурсов (кнопка) при изменении дата соурса",
        "description":     "",
        "start_date":      "2016-10-19",
        "done_ratio":      0,
        "estimated_hours": 3.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-10-19T17:28:21Z",
        "updated_on":      "2016-10-19T17:28:21Z"
    }, {
        "id":              3573,
        "project":         {"id": 10, "name": "DC-UI v2.0"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 3326},
        "subject":         "Добавить выбор дат отображения статистики на главной",
        "description":     "Добавить выбор дат отображения статистики на главной",
        "start_date":      "2016-06-14",
        "done_ratio":      0,
        "estimated_hours": 3.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":      "2016-06-14T13:38:54Z",
        "updated_on":      "2016-06-14T13:40:02Z"
    }, {
        "id":              3532,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "При повторной заливки техже данных связи дублируются",
        "description":     "При повторной заливки техже данных связи дублируются, есть ли возможность этого избежать",
        "start_date":      "2016-06-09",
        "done_ratio":      0,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":      "2016-06-09T09:28:24Z",
        "updated_on":      "2016-06-09T09:28:24Z"
    }, {
        "id":              3512,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 3326},
        "subject":         "Выкладка #3326",
        "description":     "Выкладка #3326",
        "start_date":      "2016-06-08",
        "done_ratio":      0,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":      "2016-06-08T10:06:12Z",
        "updated_on":      "2016-06-09T09:19:39Z"
    }, {
        "id":              3511,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Автоматизация развертывания КД",
        "description":     "Автоматизация развертывания КД",
        "start_date":      "2016-06-08",
        "done_ratio":      0,
        "estimated_hours": 4.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":      "2016-06-08T10:01:28Z",
        "updated_on":      "2016-06-08T10:45:43Z"
    }, {
        "id":              3138,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 11, "name": "Ошибка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 1, "name": "Низкий"},
        "author":          {"id": 33, "name": "Вадим Ломако"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "fixed_version":   {"id": 32, "name": "Релиз 27"},
        "parent":          {"id": 2868},
        "subject":         "Параметрический поиск. Дата рождения. Кнопка \"Сегодня\" не кликабельна.",
        "description":     "  *Стенд:*\t\r\n   http://10.250.9.225:8081/index\r\n  \r\n  *Описание дефекта:*\r\n   Параметрический поиск. Дата рождения. Если нажать кнопку \"Сегодня\" а потом сменить дату, кнопка перестает быть кликабельной.",
        "start_date":      "2016-05-11",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    20,
            "name":  "Время на постановку",
            "value": ""
        }],
        "created_on":      "2016-05-11T12:11:55Z",
        "updated_on":      "2016-12-01T09:09:22Z"
    }, {
        "id":              3139,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 11, "name": "Ошибка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 1, "name": "Низкий"},
        "author":          {"id": 33, "name": "Вадим Ломако"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "fixed_version":   {"id": 32, "name": "Релиз 27"},
        "parent":          {"id": 2868},
        "subject":         "Параметрический поиск. При редактировании значений в полях \"Имя\", \"Фамилия\", \"Отчество\" курсор перекидывает в конец строки.",
        "description":     "  *Стенд:*\t\r\n   http://10.250.9.225:8081/index\r\n  \r\n  *Описание дефекта:*\r\n   При редактировании значений в полях \"Имя\", \"Фамилия\", \"Отчество\", \"Телефон\" курсор перекидывает в конец строки.",
        "start_date":      "2016-05-11",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    20,
            "name":  "Время на постановку",
            "value": ""
        }],
        "created_on":      "2016-05-11T12:18:14Z",
        "updated_on":      "2016-12-01T09:09:05Z"
    }, {
        "id":              3136,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 11, "name": "Ошибка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 1, "name": "Низкий"},
        "author":          {"id": 33, "name": "Вадим Ломако"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "fixed_version":   {"id": 32, "name": "Релиз 27"},
        "parent":          {"id": 2868},
        "subject":         "Ошибка отображения страницы.",
        "description":     "  *Стенд:*\t\r\n   http://10.250.9.225:8081/index\r\n  \r\n  *Описание дефекта:*\r\n   Если взять субъект у которого есть вкладка Stats, для примера возьмем карточку Родичева Владимира Юрьевича, переходим во вкладку stats, жмем на MosID (см. 11052016_скриншот_5), далее кликаем по сайтам (см. 11052016_скриншот_6) и жмем в браузере стрелку \"Назад\". Интерфейс отображается некорректно (см. 11052016_скриншот_3).",
        "start_date":      "2016-05-11",
        "done_ratio":      0,
        "estimated_hours": 1.5,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    20,
            "name":  "Время на постановку",
            "value": ""
        }],
        "created_on":      "2016-05-11T12:07:24Z",
        "updated_on":      "2016-12-01T09:08:46Z"
    }, {
        "id":              3079,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 11, "name": "Ошибка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 1, "name": "Низкий"},
        "author":          {"id": 33, "name": "Вадим Ломако"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "fixed_version":   {"id": 32, "name": "Релиз 27"},
        "parent":          {"id": 2868},
        "subject":         "Геотрекер. Некорректное отображение местонахождения пользователя.",
        "description":     "  *Стенд:*\t\r\n   http://10.250.9.225:8081/index\r\n  \r\n  *Описание дефекта:*\r\n   Для примера карточка Алексеева Вадима Васильевича, геотрекер, после нажатия на \"2\" (см. 04052016_скриншот_4), высвечивается местонахождение пользователя, с некорректно отображенными кнопками (см. 04052016_скриншот_3). ",
        "start_date":      "2016-05-04",
        "done_ratio":      0,
        "estimated_hours": 1.5,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    20,
            "name":  "Время на постановку",
            "value": ""
        }],
        "created_on":      "2016-05-04T13:13:23Z",
        "updated_on":      "2016-12-01T09:06:36Z"
    }, {
        "id":              3101,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 11, "name": "Ошибка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 1, "name": "Низкий"},
        "author":          {"id": 33, "name": "Вадим Ломако"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "fixed_version":   {"id": 32, "name": "Релиз 27"},
        "parent":          {"id": 2868},
        "subject":         "Некорректное отображение фотографии.",
        "description":     "  *Стенд:*\t\r\n   http://10.250.9.225:8081/index\r\n  \r\n  *Описание дефекта:*\r\n   Если в простом поиске найти субъект с фотографией, в нашем случае Корниенко Юлия Михайловна, зайти на карточку субъекта, после нажать поиск и найти субъект без фотографии, в нашем случае Чаплыгина Татьяна Евгеньевна, то отобразится фотография первого субъекта, однако если обновить страницу через Ctrl+R, отобразится стандартный аватар.",
        "start_date":      "2016-05-05",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    20,
            "name":  "Время на постановку",
            "value": ""
        }],
        "created_on":      "2016-05-05T11:09:58Z",
        "updated_on":      "2016-12-01T09:06:04Z"
    }, {
        "id":              5529,
        "project":         {"id": 38, "name": "Профилирование"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 9, "name": "Требуется уточнение"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 4145},
        "subject":         "Внести данные в продуктовую базу Профилирования",
        "description":     "Задача заключается в следующем:\r\n\r\nИз xml-файла по адресу http://evp.mos.ru/bitrix/tools/sites_categories.xml вытянуть все значения url с соответствующими категориями.\r\nИ внести полученные данные (урл-категория) в базу продуктового интерфейса Профилирования на https://stats.mos.ru/profiling/",
        "start_date":      "2016-12-07",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Prod"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-12-07T16:06:08Z",
        "updated_on":      "2016-12-12T11:40:34Z"
    }, {
        "id":              4146,
        "project":         {"id": 8, "name": "Новостной виджет CityNews"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 12, "name": "Отложена"},
        "priority":        {"id": 3, "name": "Высокий"},
        "author":          {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "fixed_version":   {"id": 57, "name": "Релиз 10"},
        "subject":         "UI Статистики виджета 2. Реализация интерфейса второй версии",
        "description":     "http://pxl-stats-widget.stage.local.px-l.ru/\r\n\r\nadmin\r\nbxhf8r1r\r\n\r\nРазделы:\r\nДомены\r\nМониторинг установки\r\nАрхив доменов\r\nИсполнительность\r\nПосещаемость\r\nПопулярность",
        "start_date":      "2016-07-15",
        "due_date":        "2016-12-14",
        "done_ratio":      22,
        "estimated_hours": 196.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку"}],
        "created_on":      "2016-07-27T08:45:51Z",
        "updated_on":      "2016-12-12T13:31:46Z"
    }, {
        "id":            4463,
        "project":       {"id": 34, "name": "КДv2"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 12, "name": "Отложена"},
        "priority":      {"id": 3, "name": "Высокий"},
        "author":        {"id": 13, "name": "Андрей Иванов"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Удаление ненужных атрибутов",
        "description":   "Необходимо зачистить все значения (или вовсе удалить атрибуты):\r\n1. person_is_friend_cso\r\n2. person_party_affiliation\r\n3. person_is_exempt\r\n4. exemption_name\r\n\r\nСейчас данные атрибуты не отображаются (исключены через админку), но заменены новыми идентичными по смыслу (хранящими другие значения). \r\n",
        "start_date":    "2016-08-31",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Prod"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-08-31T09:51:09Z",
        "updated_on":    "2016-12-01T09:10:43Z"
    }, {
        "id":            4464,
        "project":       {"id": 34, "name": "КДv2"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 12, "name": "Отложена"},
        "priority":      {"id": 3, "name": "Высокий"},
        "author":        {"id": 13, "name": "Андрей Иванов"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Убрать значение \"нет\" для атрибута person_is_dead",
        "description":   "Атрибут person_is_dead хранит 2 значения: \"да\" и \"нет\"\r\nНеобходимо зачистить значения \"нет\" ",
        "start_date":    "2016-08-31",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Prod"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-08-31T09:55:51Z",
        "updated_on":    "2016-12-01T09:10:34Z"
    }, {
        "id":            4989,
        "project":       {"id": 34, "name": "КДv2"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 12, "name": "Отложена"},
        "priority":      {"id": 3, "name": "Высокий"},
        "author":        {"id": 13, "name": "Андрей Иванов"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Переименование атрибута",
        "description":   "В КД-К изменить название атрибута \"person_birthday_mounth\" на person_birthday_month\"",
        "start_date":    "2016-10-28",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-10-28T09:59:21Z",
        "updated_on":    "2016-12-01T09:10:20Z"
    }, {
        "id":              2875,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 12, "name": "Отложена"},
        "priority":        {"id": 3, "name": "Высокий"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Реализация процеса приема событий о изменении сущностей (создании) от дружественных систем",
        "description":     "Реализация процеса приема событий о изменении сущностей (создании) от дружественных систем",
        "start_date":      "2016-04-22",
        "due_date":        "2016-09-19",
        "done_ratio":      58,
        "estimated_hours": 3.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":      "2016-04-19T14:12:20Z",
        "updated_on":      "2016-09-13T12:53:05Z"
    }, {
        "id":            2477,
        "project":       {"id": 34, "name": "КДv2"},
        "tracker":       {"id": 7, "name": "Разработка"},
        "status":        {"id": 12, "name": "Отложена"},
        "priority":      {"id": 3, "name": "Высокий"},
        "author":        {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "fixed_version": {"id": 32, "name": "Релиз 27"},
        "subject":       "Рефакторинг раздела Поиск",
        "description":   "Реализовать функционал поиска согласно требованиям во вложении\r\n\r\nРезультат поискового запроса по субъектам должен выгружаться таблицей, а также в виде отметок на карте",
        "start_date":    "2016-03-31",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": []}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":    "2016-03-31T15:55:05Z",
        "updated_on":    "2016-06-28T12:11:15Z"
    }, {
        "id":            4147,
        "project":       {"id": 11, "name": "StatS"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 12, "name": "Отложена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "parent":        {"id": 2535},
        "subject":       "Rambler. Подготовить описание тестирования механизма обогащения файлов ",
        "description":   "Необходимо описать процесс тестирования механизма по задаче http://rm.pixel.alx/issues/3838\r\n\r\nКаждого из этапов:\r\n1) забора файлов на http://mos.um.rambler.ru/\r\n2) скрипта по http://rm.pixel.alx/issues/4133\r\n3) выкладки на http://stats.mos.ru/rambler \r\n",
        "start_date":    "2016-07-29",
        "due_date":      "2016-07-29",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-07-27T08:47:24Z",
        "updated_on":    "2016-12-01T09:04:34Z"
    }, {
        "id":            4504,
        "project":       {"id": 11, "name": "StatS"},
        "tracker":       {"id": 7, "name": "Разработка"},
        "status":        {"id": 12, "name": "Отложена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "parent":        {"id": 2535},
        "subject":       "Доработка механизма взаимодействия с Рамблером",
        "description":   "Все получаемые от Рамблера файлы используются для вывода данных в UI профилирования, поэтому данные из выгрузок, которые мы забираем от Рамблера, необходимо сохранять.\r\nПоля: \r\n1) mosid, \r\n2) посещаемые сайты Rambler (сохранять значения в той же последовательности, что и в выгрузках - 1 указанный урл - наиболее посещаемый данным mosid, 2 - второй по посещаемости, и т.д.), \r\n3) посещаемые категории сайтов Rambler (сохранять значения в той же последовательности, что и в выгрузках - 1 указанная категория - наиболее посещаемая данным mosid, 2 - вторая по посещаемости, и т.д.)\r\n\r\nПри этом, выводиться в интерфейсе будут данные по мос айди на самую последнюю дату (если статистика была зафиксирована в несколько дат - выберется самая актуальная)",
        "start_date":    "2016-09-05",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-09-05T09:48:30Z",
        "updated_on":    "2016-12-01T09:04:19Z"
    }, {
        "id":            4162,
        "project":       {"id": 11, "name": "StatS"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 12, "name": "Отложена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "fixed_version": {"id": 57, "name": "Релиз 10"},
        "subject":       "Тестирование механизма разбирания очереди Stats",
        "description":   "2 подзадачи:\r\n\r\n1. Подготовительные работы (развертывание, предоставление требований к технике/площадке/и т.д.)\r\n2. Описание процесса тестирования",
        "start_date":    "2016-07-29",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-07-29T11:20:56Z",
        "updated_on":    "2016-11-28T09:04:16Z"
    }, {
        "id":              3010,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 12, "name": "Отложена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "UI КД. Реализовать раздел \"Расширенный поиск\"",
        "description":     "Требования во вложении",
        "start_date":      "2016-04-29",
        "done_ratio":      50,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-04-26T15:54:18Z",
        "updated_on":      "2016-11-28T09:02:13Z"
    }, {
        "id":              4198,
        "project":         {"id": 19, "name": "Блогосфера"},
        "tracker":         {"id": 11, "name": "Ошибка"},
        "status":          {"id": 12, "name": "Отложена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 33, "name": "Вадим Ломако"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "fixed_version":   {"id": 56, "name": "Релиз 2. Итерация 2"},
        "parent":          {"id": 5002},
        "subject":         "ЛК Агента. Ссылки по теме. При отправке ссылок на проверку, статус проверки ссылок меняется, однако ссылки остаются в невалидных.",
        "description":     "*Стенд:*\r\nhttp://pxl-bgsphere.stage.local.px-l.ru/\r\n\r\n*Описание дефекта:*\r\nПереходим в ссылки по теме и нажимаем кнопку \"Отправить\".\r\n\r\n!http://cs636119.vk.me/v636119512/2036a/2xKAQJBMhDA.jpg!\r\n\r\nПосле отправки ссылки остаются в невалидных и не переносятся в графу \"Ссылки на проверке\". \r\n\r\n!http://cs636119.vk.me/v636119512/2037e/Zg-2ZjUniYs.jpg!\r\n\r\nВ списке тем, в графе \"Статус проверки ссылок\" отображается статус \"Идет проверка\".\r\n\r\n!http://cs636119.vk.me/v636119512/20374/RGH01Zk7K54.jpg!\r\n\r\n\r\nСсылки на проверке должны отображаться в данном блоке только при значении статуса проверки ссылок «Идет проверка».",
        "start_date":      "2016-08-02",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    20,
            "name":  "Время на постановку",
            "value": ""
        }],
        "created_on":      "2016-08-02T11:43:15Z",
        "updated_on":      "2016-10-31T09:39:30Z"
    }, {
        "id":              3569,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 12, "name": "Отложена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Подсчет статистики по количеству атрибутов",
        "description":     "Подсчет статистики по количеству атрибутов",
        "start_date":      "2016-06-14",
        "done_ratio":      0,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-06-14T11:48:53Z",
        "updated_on":      "2016-10-19T10:47:35Z"
    }, {
        "id":              3503,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 12, "name": "Отложена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 2958},
        "subject":         "Редактирование составных атрибутов и их весов",
        "description":     "Редактирование составных атрибутов и их весов\r\nбаза configuration, коллекция rd_attrs, поле attributesComposite",
        "start_date":      "2016-06-21",
        "done_ratio":      100,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-06-08T08:37:38Z",
        "updated_on":      "2016-10-19T10:46:43Z"
    }, {
        "id":              2958,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 12, "name": "Отложена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "fixed_version":   {"id": 32, "name": "Релиз 27"},
        "subject":         "Адаптировать интерфейс КД1 к версии 2",
        "description":     "",
        "start_date":      "2016-04-14",
        "done_ratio":      60,
        "estimated_hours": 31.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-04-25T11:37:31Z",
        "updated_on":      "2016-10-19T10:46:18Z"
    }, {
        "id":              4580,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 12, "name": "Отложена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Валидация стилей (под вопросом)",
        "description":     "",
        "start_date":      "2016-09-12",
        "done_ratio":      0,
        "estimated_hours": 3.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-09-12T10:16:29Z",
        "updated_on":      "2016-10-19T10:44:48Z"
    }, {
        "id":            4315,
        "project":       {"id": 51, "name": "Голосования"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 12, "name": "Отложена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 62, "name": "Ян Комиссаров"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "parent":        {"id": 4291},
        "subject":       "Выкладка 4291",
        "description":   "http://gitlab8.alx/ykomissarov/pxl-app-polls-collector",
        "start_date":    "2016-08-16",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-08-16T12:03:12Z",
        "updated_on":    "2016-10-19T08:52:39Z"
    }, {
        "id":            4606,
        "project":       {"id": 11, "name": "StatS"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 12, "name": "Отложена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "parent":        {"id": 4602},
        "subject":       "Выгрузка данных по mosid, ходивших по 5 урлам dogm",
        "description":   "Задача состоит из 2х этапов:\r\n\r\n1 этап: Выгрузить информацию по mosid, переходившим по следующим url:\r\n\r\n1) https://pgu.mos.ru/ru/application/dogm/077060701/#step_1 \r\n2) https://pgu.mos.ru/ru/application/dogm/077060701/#step_2\r\n3) https://pgu.mos.ru/ru/application/dogm/077060701/#step_3\r\n4) https://pgu.mos.ru/ru/application/dogm/077060701/#step_4\r\n5) https://pgu.mos.ru/ru/application/dogm/077060701/#step_5\r\n\r\nза период *с 12.09.16, 00:00 по 18.09.16, 23:59* \r\n\r\nНа выходе - 5 файлов формата csv с полями:\r\n1) mosid\r\n2) ssoid (связанный с данным mosid, если таких несколько - указать все через разделитель)\r\nиз файла удалить дублирующиеся записи (повторяющиеся *пары* мос и ссо айди - только пары, отдельно ссо и мос могут дублироваться в таблице)\r\n\r\n2 этап: Данными из полученных файлов заполнить таблицу csv следующей структуры:\r\n\r\n!http://rm.pixel.alx/attachments/download/2692/2016-09-14_15-36-23.png!\r\n\r\nВозможные значения в столбцах C D E F G: \"Да\". Если это не \"Да\", ячейку оставлять пустой.\r\n\"Да\" - означает, что mosid посещал данный url\r\nИз данной таблицы необходимо исключить записи, по которым нет ssoid.\r\n\r\nДедлайн выполнения задачи - 19.09.2016, 19:00",
        "start_date":    "2016-09-14",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-09-14T12:33:31Z",
        "updated_on":    "2016-09-14T15:03:28Z"
    }, {
        "id":            3266,
        "project":       {"id": 34, "name": "КДv2"},
        "tracker":       {"id": 7, "name": "Разработка"},
        "status":        {"id": 12, "name": "Отложена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "parent":        {"id": 3010},
        "subject":       "Реализация методов АПИ описанных в задаче 3067",
        "description":   "Реализация методов АПИ описанных в задаче 3067",
        "start_date":    "2016-05-18",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":    "2016-05-18T15:28:20Z",
        "updated_on":    "2016-06-21T07:53:09Z"
    }, {
        "id":            1333,
        "project":       {"id": 7, "name": "Концентратор данных"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 12, "name": "Отложена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "parent":        {"id": 731},
        "subject":       "Визуализация расчета идентичности сущностей на странице информации о сущности",
        "description":   "",
        "start_date":    "2015-12-16",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": []}, {
            "id":   2,
            "name": "Сложность"
        }, {"id": 17, "name": "Источник задачи"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":    "2015-12-16T11:39:12Z",
        "updated_on":    "2015-12-16T11:41:42Z"
    }, {
        "id":            1334,
        "project":       {"id": 7, "name": "Концентратор данных"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 12, "name": "Отложена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "parent":        {"id": 731},
        "subject":       "Вывод результатов анализа возможности слияния сущностей",
        "description":   "",
        "start_date":    "2015-12-16",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": []}, {
            "id":   2,
            "name": "Сложность"
        }, {"id": 17, "name": "Источник задачи"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":    "2015-12-16T11:41:16Z",
        "updated_on":    "2015-12-16T11:41:16Z"
    }, {
        "id":            3106,
        "project":       {"id": 34, "name": "КДv2"},
        "tracker":       {"id": 11, "name": "Ошибка"},
        "status":        {"id": 12, "name": "Отложена"},
        "priority":      {"id": 1, "name": "Низкий"},
        "author":        {"id": 33, "name": "Вадим Ломако"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "fixed_version": {"id": 32, "name": "Релиз 27"},
        "parent":        {"id": 2868},
        "subject":       "Необходимо реализовать валидацию поля при создании группы.",
        "description":   "  *Стенд:*\t\r\n   http://10.250.9.225:8081/index\r\n  \r\n  *Описание дефекта:*\r\n   Необходимо реализовать валидацию поля при создании группы (см. 05052016_скриншот_1).",
        "start_date":    "2016-05-05",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    20,
            "name":  "Время на постановку",
            "value": ""
        }],
        "created_on":    "2016-05-05T13:34:50Z",
        "updated_on":    "2016-12-01T09:07:47Z"
    }, {
        "id":              3505,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 12, "name": "Отложена"},
        "priority":        {"id": 1, "name": "Низкий"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 2958},
        "subject":         "Редактирование конфига для параметрического поиска",
        "description":     "Редактирование конфига для параметрического поиска",
        "start_date":      "2016-06-08",
        "done_ratio":      0,
        "estimated_hours": 8.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-06-08T08:41:14Z",
        "updated_on":      "2016-10-19T10:46:56Z"
    }, {
        "id":            2959,
        "project":       {"id": 34, "name": "КДv2"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 12, "name": "Отложена"},
        "priority":      {"id": 1, "name": "Низкий"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "parent":        {"id": 2958},
        "subject":       "Правка составных атрибутов и визуализация сущностей в административном интерфейсе",
        "description":   "Правка составных атрибутов и визуализация сущностей в административном интерфейсе",
        "start_date":    "2016-04-25",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":    "2016-04-25T11:38:17Z",
        "updated_on":    "2016-06-08T08:35:20Z"
    }, {
        "id":              4145,
        "project":         {"id": 38, "name": "Профилирование"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 11, "name": "Возобновлена"},
        "priority":        {"id": 4, "name": "Срочный"},
        "author":          {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "fixed_version":   {"id": 57, "name": "Релиз 10"},
        "subject":         "UI Профилирования 2. Реализация интерфейса второй версии",
        "description":     "http://pxl-mosid-events.stage.local.px-l.ru/\r\n\r\nadmin\r\njDzCRB9N\r\n\r\nРазделы:\r\nСтатистика\r\nКатегории\r\nСайты",
        "start_date":      "2016-07-15",
        "due_date":        "2016-12-16",
        "done_ratio":      59,
        "estimated_hours": 256.5,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-07-27T08:33:11Z",
        "updated_on":      "2016-12-12T11:55:53Z"
    }, {
        "id":              1433,
        "project":         {"id": 7, "name": "Концентратор данных"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 11, "name": "Возобновлена"},
        "priority":        {"id": 4, "name": "Срочный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Реализация концентратора данных v2",
        "description":     "",
        "start_date":      "2016-01-18",
        "done_ratio":      4,
        "estimated_hours": 18.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": []}, {
            "id":   2,
            "name": "Сложность"
        }, {"id": 17, "name": "Источник задачи"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":      "2015-12-25T11:12:05Z",
        "updated_on":      "2016-10-13T09:50:33Z"
    }, {
        "id":              3002,
        "project":         {"id": 34, "name": "КДv2"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 11, "name": "Возобновлена"},
        "priority":        {"id": 3, "name": "Высокий"},
        "author":          {"id": 11, "name": "Татьяна Петренко"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 2875},
        "subject":         "Взаимодействие с MindScan",
        "description":     "Разработать и реализовать выборку данных по метрикам страниц в социальных сетях с использованием Rest API Crawler/\r\n\r\nВ приложениях - описание методов API и спека на взаимодействие.\r\n\r\nОпишите механизм реализации задачи. Цель - отдавать внешней системе урлы имеющихся в КД-С профилей соцсетей, и забирать и хранить полученные по ним метрики (описаны в доке)",
        "start_date":      "2016-04-27",
        "due_date":        "2016-09-19",
        "done_ratio":      33,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":      "2016-04-26T13:55:50Z",
        "updated_on":      "2016-09-13T12:18:14Z"
    }, {
        "id":            4823,
        "project":       {"id": 38, "name": "Профилирование"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 11, "name": "Возобновлена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "fixed_version": {"id": 57, "name": "Релиз 10"},
        "parent":        {"id": 4061},
        "subject":       "Реализация сбора данных для кругов Эйлера",
        "description":   "Актуальное описание:\r\n\r\nОбласти пересечения в карточке сайта:\r\n1. Круги Эйлера по сайтам из этой категории\r\n\tДолжны быть реализованы круги Эйлера по пересечению аудиторий (mos_id) данного сайта с другими сайтами из категории, к которой относится первый сайт. Минимальное число кругов Эйлера – 2, максимальное число кругов Эйлера – 3. \r\n\tВеличина пересечения на кругах Эйлера меняется в зависимости от величины процента пересечения аудитории. При наведении курсора на область пересечения отображается информация по проценту пересечения аудитории и абсолютное значение пересечения аудитории.\r\n\tИнформация считается в режиме онлайн, учитываются аудитории за три предыдущих месяца.    \r\n2. Круги Эйлера по сайтам из других категорий\r\n\tДолжны быть реализованы круги Эйлера по пересечению аудиторий (mos_id) данного сайта с сайтами сайтами из категорий, к которым *не* относится первый сайт. Минимальное число кругов Эйлера – 2, максимальное число кругов Эйлера – 3. \r\n\tВеличина пересечения на кругах Эйлера меняется в зависимости от величины процента пересечения аудитории. При наведении курсора на область пересечения отображается информация по проценту пересечения аудитории и абсолютное значение пересечения аудитории.\r\n\tИнформация считается в режиме онлайн, учитываются аудитории за три предыдущих месяца.  \r\n",
        "start_date":    "2016-10-10",
        "done_ratio":    90,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-10-10T17:19:40Z",
        "updated_on":    "2016-12-06T16:10:10Z"
    }, {
        "id":              2535,
        "project":         {"id": 11, "name": "StatS"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 5, "name": "Немедленный"},
        "author":          {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Запуск синхронизации с Rambler",
        "description":     "",
        "start_date":      "2016-04-05",
        "due_date":        "2016-08-23",
        "done_ratio":      80,
        "estimated_hours": 18.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": []}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-04-05T09:27:03Z",
        "updated_on":      "2016-10-26T11:15:59Z"
    }, {
        "id":              102,
        "project":         {"id": 7, "name": "Концентратор данных"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 4, "name": "Срочный"},
        "author":          {"id": 12, "name": "Глеб Южаков"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 86},
        "subject":         "Реализация концентратора данных",
        "description":     "",
        "start_date":      "2015-08-03",
        "due_date":        "2015-10-02",
        "done_ratio":      67,
        "estimated_hours": 312.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": []}, {
            "id":   2,
            "name": "Сложность"
        }, {"id": 17, "name": "Источник задачи"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":      "2015-07-31T12:26:31Z",
        "updated_on":      "2016-02-25T06:23:14Z"
    }, {
        "id":            1926,
        "project":       {"id": 7, "name": "Концентратор данных"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 4, "name": "Срочный"},
        "author":        {"id": 19, "name": "Олег Гунченко"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Развернуть концентратор в Stage",
        "description":   "http://as01.stage.local.px-l.ru:15672/\r\nлогин/пароль emp/emp\r\n\r\nWildFly\r\nhttp://as01.stage.local.px-l.ru:8093/ - \r\nhttp://as01.stage.local.px-l.ru:9990/ админка\r\nЛогин/пароль admin/123321\r\n\r\nMongo\r\n\r\n10.3.3.221 без логина и пароля",
        "start_date":    "2016-02-24",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": []}, {
            "id":   2,
            "name": "Сложность"
        }, {"id": 17, "name": "Источник задачи"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":    "2016-02-24T13:26:20Z",
        "updated_on":    "2016-02-24T17:47:21Z"
    }, {
        "id":              105,
        "project":         {"id": 7, "name": "Концентратор данных"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 4, "name": "Срочный"},
        "author":          {"id": 12, "name": "Глеб Южаков"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 102},
        "subject":         "Нормализация значений атрибутов",
        "description":     "",
        "start_date":      "2015-08-05",
        "due_date":        "2015-10-02",
        "done_ratio":      99,
        "estimated_hours": 70.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": []}, {
            "id":   2,
            "name": "Сложность"
        }, {"id": 17, "name": "Источник задачи"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":      "2015-07-31T12:31:50Z",
        "updated_on":      "2016-02-03T09:19:57Z"
    }, {
        "id":              1323,
        "project":         {"id": 9, "name": "Pixel общий"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 4, "name": "Срочный"},
        "author":          {"id": 19, "name": "Олег Гунченко"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Описать используемую инфраструктуру",
        "description":     "Для проектов в Stage и Prod окружениях",
        "start_date":      "2015-12-15",
        "done_ratio":      0,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": []}, {
            "id":   2,
            "name": "Сложность"
        }, {"id": 17, "name": "Источник задачи"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":      "2015-12-15T13:24:38Z",
        "updated_on":      "2015-12-16T10:14:59Z"
    }, {
        "id":            5530,
        "project":       {"id": 11, "name": "StatS"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 3, "name": "Высокий"},
        "author":        {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Подготовка отчетов о посещениях урлов из логов Статса",
        "description":   "Ссылка на архив с данными, выгруженными из логов статса по 4м урлам:\r\n\r\nhttps://drive.google.com/open?id=0BzEewLlqlNpEVnY0dXZ6VTdrMnc\r\n\r\nОсновываясь на данных в выгрузках, необходимо заполнить таблицу во вложении - подсчитать цифры:\r\n--1) общее кол-во посещений урла ; \r\n--2) кол-во уникальных mos_id; \r\n--3) кол-во неуникальных sso_id; \r\n--4) кол-во уникальных sso_id;\r\n\r\n+ Нужно на каждый урл подготовить отдельную выгрузку в формате csv кодировка utf-8 со следующими параметрами:\r\n--1) mos_id (в этом столбце перечень всех mos_id, которые посещали соответствующий урл за указанный период);\r\n--2) дата (в этом столбце дата посещения mos_id соответствующего урла);\r\n--3) время (в том столбце время посещения mos_id соответствующего урла);\r\n*если mos_id появлялся несколько раз, то дублировать запись с указанием даты посещения.\r\n\r\nПример выгрузки во вложении",
        "start_date":    "2016-12-07",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-12-07T16:20:10Z",
        "updated_on":    "2016-12-08T17:03:21Z"
    }, {
        "id":              4061,
        "project":         {"id": 38, "name": "Профилирование"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 3, "name": "Высокий"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 4145},
        "subject":         "Реализация методов АПИ необходимых для реализации интерфейса статистики Профилирования",
        "description":     "Реализация методов АПИ необходимых для реализации интерфейса статистики Widget",
        "start_date":      "2016-07-15",
        "due_date":        "2016-12-16",
        "done_ratio":      16,
        "estimated_hours": 90.5,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-07-15T11:04:19Z",
        "updated_on":      "2016-12-07T09:05:27Z"
    }, {
        "id":              5338,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 3, "name": "Высокий"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Реализация управления данными протокола: \"Олеся\", протокол: \"Валя\"",
        "description":     "",
        "start_date":      "2016-11-28",
        "done_ratio":      0,
        "estimated_hours": 11.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-28T17:05:16Z",
        "updated_on":      "2016-11-30T12:20:33Z"
    }, {
        "id":              5346,
        "project":         {"id": 38, "name": "Профилирование"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 3, "name": "Высокий"},
        "author":          {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 5274},
        "subject":         "Обновление Stage",
        "description":     "Проверить исправление дефектов в родительской http://rm.pixel.alx/issues/5274 и обновить стенд stage http://10.250.9.117:8999/",
        "start_date":      "2016-11-29",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-29T13:29:18Z",
        "updated_on":      "2016-11-30T10:25:20Z"
    }, {
        "id":              5325,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 3, "name": "Высокий"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Выкладка #5003, #4912",
        "description":     "В ветке: \"5003\" содержется задача #4912",
        "start_date":      "2016-11-28",
        "done_ratio":      0,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-28T11:42:27Z",
        "updated_on":      "2016-11-28T17:03:56Z"
    }, {
        "id":              4928,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 3, "name": "Высокий"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Добавить возможность добавлять Events",
        "description":     "1. Добавляем новый параметр events для лайаутов и компонетов\r\nПоле trigger_events принимает значения: REFRESH (name - название экрана), CLOSE (name - название экрана), LOGOUT (name - пустой)\r\n\r\nПоле event_type принимает значения UI (обновление отображения), ONLOAD (отрабатывает при загрузке страницы либо по тригерру), SEND (пока не используется)\r\n\r\n2. Добавляем параметр для лайаута, который будет определять возможность использования лайаута в евенте\r\n\r\n3. В транзишн для компонентов добавить возможность указания евент триггера\r\n\r\n<pre>\r\n<code class=\"json\">\r\n{\r\n\t\"events\": {\r\n\t\t\"ui\": {\r\n\t\t\t\"registerUser\": {\r\n\t\t\t\t\"type\": \"dialog\",\r\n\t\t\t\t\"fields\": {\r\n\t\t\t\t\t\"text_src\": \"#{Вы успешно подали заявку на запись в Национальную библиотеку имени Н.Г. Доможакова. Получить читательский билет при наличии паспорта или иного документа, удостоверяющего личность, и воспользоваться услугами учреждения можно в часы работы библиотеки.}\",\r\n\t\t\t\t\t\"subtext\": \"#{Поздравляем!}\"\r\n\t\t\t\t},\r\n\t\t\t\t\"bottomviews\": [{\r\n\t\t\t\t\t\"type\": \"text\",\r\n\t\t\t\t\t\"fields\": {\r\n\t\t\t\t\t\t\"text_src\": \"OK\"\r\n\t\t\t\t\t},\r\n\t\t\t\t\t\"transition\": {\r\n\t\t\t\t\t\t\"name\": \"news_list\",\r\n\t\t\t\t\t\t\"trigger_events\": [{\r\n\t\t\t\t\t\t\t\"name\": \"register\",\r\n\t\t\t\t\t\t\t\"action\": \"close\",\r\n\t\t\t\t\t\t\t\"event_type\": \"ui\"\r\n\t\t\t\t\t\t}, {\r\n\t\t\t\t\t\t\t\"name\": \"sign_in\",\r\n\t\t\t\t\t\t\t\"action\": \"close\",\r\n\t\t\t\t\t\t\t\"event_type\": \"ui\"\r\n\t\t\t\t\t\t}, {\r\n\t\t\t\t\t\t\t\"name\": \"start_page\",\r\n\t\t\t\t\t\t\t\"action\": \"close\",\r\n\t\t\t\t\t\t\t\"event_type\": \"ui\"\r\n\t\t\t\t\t\t}]\r\n\t\t\t\t\t}\r\n\t\t\t\t}]\r\n\t\t\t}\r\n\t\t}\r\n\t}\r\n}\r\n\r\n{\r\n\t\"transition\": {\r\n\t\t\"name\": \"opacGlobalBackend/users_registerUser\",\r\n\t\t\"trigger_events\": [{\r\n\t\t\t\"name\": \"registerUser\",\r\n\t\t\t\"event_type\": \"ui\"\r\n\t\t}],\r\n\t\t\"params\": [{\r\n\t\t\t\"value\": \"$date\",\r\n\t\t\t\"name\": \"date\",\r\n\t\t\t\"type\": \"STRING\"\r\n\t\t}, {\r\n\t\t\t\"value\": \"$job\",\r\n\t\t\t\"name\": \"job\",\r\n\t\t\t\"type\": \"STRING\"\r\n\t\t}, {\r\n\t\t\t\"value\": \"$email\",\r\n\t\t\t\"name\": \"email\",\r\n\t\t\t\"type\": \"STRING\"\r\n\t\t}, {\r\n\t\t\t\"value\": \"$surname\",\r\n\t\t\t\"name\": \"surname\",\r\n\t\t\t\"type\": \"STRING\"\r\n\t\t}, {\r\n\t\t\t\"value\": \"$name\",\r\n\t\t\t\"name\": \"name\",\r\n\t\t\t\"type\": \"STRING\"\r\n\t\t}, {\r\n\t\t\t\"value\": \"$address\",\r\n\t\t\t\"name\": \"address\",\r\n\t\t\t\"type\": \"STRING\"\r\n\t\t}]\r\n\t}\r\n}\r\n</code>\r\n</pre>",
        "start_date":      "2016-10-19",
        "done_ratio":      40,
        "estimated_hours": 8.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-10-19T17:42:52Z",
        "updated_on":      "2016-11-24T17:19:49Z"
    }, {
        "id":              4576,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 3, "name": "Высокий"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Кастомизация параметра Transition для layout FORM",
        "description":     "На странице FORM есть кнопка SUBMIT у нее необходимо настраивать Transition из датасета\r\n1. Нужен параметр для лейаута и компонента, который будет отмечать что для транзишена можно будет выбрать переход не только на экран, но и на эндпоинт (дата соурс)\r\n2. Заполнение полей из данных формы",
        "start_date":      "2016-09-12",
        "done_ratio":      80,
        "estimated_hours": 3.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-09-12T10:11:43Z",
        "updated_on":      "2016-11-23T12:15:55Z"
    }, {
        "id":            4468,
        "project":       {"id": 34, "name": "КДv2"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 3, "name": "Высокий"},
        "author":        {"id": 13, "name": "Андрей Иванов"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Удаление паспортов, содержащих только фио владельца",
        "description":   "В КД-К присутствуют объекты типа doc_identity, содержащие только один атрибут - doc_identity_full_name\r\nТакие сущности необходимо найти и удалить (и из эластика, и из исходных коллекций в Монге)\r\n\r\np.s. на будущее необходимо обсудить изменение механизма создания сущностей, предусматривающего проверку наличия определенных атрибутов для каждого типа сущностей (пример: создавать doc_identity только при наличии doc_identity_full_number или создавать объект email только с email_name)",
        "start_date":    "2016-08-31",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Prod"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-08-31T11:40:34Z",
        "updated_on":    "2016-10-28T17:14:41Z"
    }, {
        "id":            4889,
        "project":       {"id": 11, "name": "StatS"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 3, "name": "Высокий"},
        "author":        {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Выгрузить email и mosid из событий",
        "description":   "Нужно выгрузить 2 поля: \"email \"и \"mosid\"\r\n\r\nиз событий следующих типов:\r\nFeedbackFormEDOSubmition (параметр в событии \"mosId\" и параметр в объекте события \"email\")\r\nEmailOpenEvent (параметр в событии \"mosId\" и параметр в объекте события \"email\")\r\nEmailLinkFollowEvent (параметр в событии \"mosId\" и параметр в объекте события \"email\")\r\n\r\nВ файл csv",
        "start_date":    "2016-10-18",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-10-18T12:49:44Z",
        "updated_on":    "2016-10-18T15:40:19Z"
    }, {
        "id":              2319,
        "project":         {"id": 11, "name": "StatS"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 3, "name": "Высокий"},
        "author":          {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Запуск синхронизации с Amberdata",
        "description":     "Запустить синхронизацию с системой для обмена данными (описание взаимодействия во вложении)\r\n\r\nСинк через пиксель, вот урл:\r\n//dmg.digitaltarget.ru/1/1228/i/i?a=228&e=$UID&i=$RND\r\n\r\nмакрос $UID нужно заменять на наш id профиля пользователя, $RND - на случайное число (для предотвращения кэширования).\r\n \r\n\r\n \r\n",
        "start_date":      "2016-03-28",
        "done_ratio":      100,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": []}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":      "2016-03-22T15:41:47Z",
        "updated_on":      "2016-10-13T09:47:07Z"
    }, {
        "id":              396,
        "project":         {"id": 7, "name": "Концентратор данных"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 3, "name": "Высокий"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 102},
        "subject":         "Доработка алгоритма концентратора данных",
        "description":     "",
        "start_date":      "2015-09-04",
        "done_ratio":      84,
        "estimated_hours": 83.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": []}, {
            "id":   2,
            "name": "Сложность"
        }, {"id": 17, "name": "Источник задачи"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":      "2015-09-04T13:09:09Z",
        "updated_on":      "2016-02-25T06:23:14Z"
    }, {
        "id":            1046,
        "project":       {"id": 9, "name": "Pixel общий"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 3, "name": "Высокий"},
        "author":        {"id": 19, "name": "Олег Гунченко"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Описать используемые ресурсы проектов Java",
        "description":   "Список серверов, баз данных и демон машин для проектов на Java",
        "start_date":    "2015-11-27",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": []}, {
            "id":   2,
            "name": "Сложность"
        }, {"id": 17, "name": "Источник задачи"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":    "2015-11-27T11:23:35Z",
        "updated_on":    "2015-12-01T13:19:23Z"
    }, {
        "id":            573,
        "project":       {"id": 10, "name": "DC-UI v2.0"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 3, "name": "Высокий"},
        "author":        {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "parent":        {"id": 559},
        "subject":       "Аналитический UI. Истинность значения атрибута",
        "description":   "Истинность значения атрибута не может превышать 1\r\n\r\nПодсчет истинности значения атрибута, собранного их двух систем, осуществляется по формуле\r\n\r\n(u + v - 2uv) / (1 - uv), где\r\nu и v - вероятности двух значений атрибута\r\n\r\nНовая формула\r\n\r\n(x + p1p2)/(1 + x), где \r\nx > 0 (мы сошлись на x = 2)\r\np1 - вероятность атрибута у первой сущности\r\np2 - у второй\r\nx - коэффициент, чем он меньше, тем более чувствителен коэффициент к произведению p1p2",
        "start_date":    "2015-09-22",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": []}, {
            "id":   2,
            "name": "Сложность"
        }, {"id": 17, "name": "Источник задачи"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":    "2015-09-22T12:12:24Z",
        "updated_on":    "2015-10-01T08:28:15Z"
    }, {
        "id":            106,
        "project":       {"id": 7, "name": "Концентратор данных"},
        "tracker":       {"id": 7, "name": "Разработка"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 3, "name": "Высокий"},
        "author":        {"id": 12, "name": "Глеб Южаков"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "parent":        {"id": 105},
        "subject":       "Определение истинности значений атрибутов",
        "description":   "",
        "done_ratio":    100,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": []}, {
            "id":   2,
            "name": "Сложность"
        }, {"id": 17, "name": "Источник задачи"}, {"id": 19, "name": "Внешняя задача"}, {
            "id":   20,
            "name": "Время на постановку"
        }],
        "created_on":    "2015-07-31T12:32:56Z",
        "updated_on":    "2015-08-21T09:44:30Z"
    }, {
        "id":              5568,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Актуализировать список методов для платформы",
        "description":     "",
        "start_date":      "2016-12-12",
        "done_ratio":      0,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-12-12T12:12:45Z",
        "updated_on":      "2016-12-12T13:30:57Z"
    }, {
        "id":            5297,
        "project":       {"id": 39, "name": "Выгрузки"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Поиск профилей ФБ по номеру телефона",
        "description":   "Нужно определить факт наличия профиля в ФБ по номеру телефона (4 столбец в файле во вложении) + фото профиля (при наличии).\r\nВсего 224 677 записей.\r\nВ итоге, дополнить имеющуюся таблицу 2мя полями: Есть профиль (да/нет), Ссылка на фото (если есть)",
        "start_date":    "2016-11-25",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-11-25T10:35:05Z",
        "updated_on":    "2016-12-12T12:11:54Z"
    }, {
        "id":              4974,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 62, "name": "Ян Комиссаров"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 4947},
        "subject":         "Выкладка по админке",
        "description":     "http://gitlab8.alx/java/pxl-admin-mobile/tree/4947",
        "start_date":      "2016-10-26",
        "done_ratio":      0,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-10-26T13:57:06Z",
        "updated_on":      "2016-12-12T11:43:27Z"
    }, {
        "id":              5362,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Доработки управления через протокол \"Валя\"",
        "description":     "1. Возможность редактировать связанные сущности (пример: опрос-вопрос-вариант ответа)\r\n> 1.1. Возможность указания в wsdl зависимые дата сеты\r\n> 1.2. Визуальное редактирование зависимых дата сетов\r\n2. Пагинация данных\r\n3. Сортировка данных\r\n4. Фильтрация данных",
        "start_date":      "2016-11-29",
        "done_ratio":      0,
        "estimated_hours": 16.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-29T17:59:57Z",
        "updated_on":      "2016-12-09T17:44:44Z"
    }, {
        "id":              5371,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Доработка Event'ов",
        "description":     "1. Для event action CLOSE и REFRESH - заполняем только имя триггера\r\n2. Для event action LOGOUT - других полей не требуется\r\n3. Для event name даем возможность заполнять из event name и из названий экранов",
        "start_date":      "2016-11-30",
        "done_ratio":      0,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-30T10:51:51Z",
        "updated_on":      "2016-12-08T13:42:04Z"
    }, {
        "id":            5508,
        "project":       {"id": 38, "name": "Профилирование"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Исправление подсчета средней посещаемости в месяц",
        "description":   "",
        "start_date":    "2016-12-06",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-12-06T17:41:34Z",
        "updated_on":    "2016-12-06T17:41:34Z"
    }, {
        "id":            5507,
        "project":       {"id": 11, "name": "StatS"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Обсуждение задач статс",
        "description":   "",
        "start_date":    "2016-12-06",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-12-06T17:18:04Z",
        "updated_on":    "2016-12-06T17:18:04Z"
    }, {
        "id":            5493,
        "project":       {"id": 9, "name": "Pixel общий"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Собрание по вторникам",
        "description":   "",
        "start_date":    "2016-12-06",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-12-06T09:49:40Z",
        "updated_on":    "2016-12-06T09:49:40Z"
    }, {
        "id":            5492,
        "project":       {"id": 9, "name": "Pixel общий"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Общее",
        "description":   "",
        "start_date":    "2016-12-05",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-12-06T09:48:29Z",
        "updated_on":    "2016-12-06T09:48:29Z"
    }, {
        "id":              5401,
        "project":         {"id": 11, "name": "StatS"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Перенос данных по виджетам в Clickhouse",
        "description":     "Нужно перенести данные о сайтах, на которых был установлен виджет за последний год, из монги в Кликхаус\r\n\r\nИ еще написать запрос, который вернет из Кликхауса перечень уникальных хостов, на которых был установлен виджет с 1.12.2015 по 1.12.2016",
        "start_date":      "2016-12-01",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-12-01T12:30:28Z",
        "updated_on":      "2016-12-05T21:45:50Z"
    }, {
        "id":              5386,
        "project":         {"id": 8, "name": "Новостной виджет CityNews"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Проверка и исправление апи виджета",
        "description":     "",
        "start_date":      "2016-11-30",
        "done_ratio":      0,
        "estimated_hours": 8.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-30T15:27:18Z",
        "updated_on":      "2016-12-05T15:05:00Z"
    }, {
        "id":              5372,
        "project":         {"id": 11, "name": "StatS"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Исправить мос ид с потерянными равно",
        "description":     "",
        "start_date":      "2016-11-30",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-30T12:21:35Z",
        "updated_on":      "2016-12-01T10:56:01Z"
    }, {
        "id":              5384,
        "project":         {"id": 19, "name": "Блогосфера"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Разгребание ошибок",
        "description":     "",
        "start_date":      "2016-11-30",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-30T14:58:03Z",
        "updated_on":      "2016-11-30T14:58:03Z"
    }, {
        "id":            5072,
        "project":       {"id": 19, "name": "Блогосфера"},
        "tracker":       {"id": 11, "name": "Ошибка"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 33, "name": "Вадим Ломако"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "parent":        {"id": 5002},
        "subject":       "ЛК Агента. Темы. Статус проверки ссылок отображается некорректно.",
        "description":   "*Стенд:*\r\nhttp://pxl-bgsphere.stage.local.px-l.ru/\r\n\r\n*Описание дефекта:*\r\nВ теме \"Просто тема\", статус проверки ссылок \"Идет проверка\" .\r\n\r\n!https://pp.vk.me/c637422/v637422512/1e8b9/t1YAI5nOw6Y.jpg!\r\n\r\nПри переходе в ссылки по теме, отображается только одна ссылка, в категории \"Невалидные ссылки\". Категория \"Ссылки на проверке\" пустая.\r\n\r\n!https://pp.vk.me/c637422/v637422512/1e8c3/hGYk2a1PPuo.jpg!\r\n\r\n",
        "start_date":    "2016-11-07",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    20,
            "name":  "Время на постановку",
            "value": ""
        }],
        "created_on":    "2016-11-07T12:38:36Z",
        "updated_on":    "2016-11-30T13:57:26Z"
    }, {
        "id":            5094,
        "project":       {"id": 19, "name": "Блогосфера"},
        "tracker":       {"id": 11, "name": "Ошибка"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 33, "name": "Вадим Ломако"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "parent":        {"id": 5002},
        "subject":       "ЛК Департамента. Темы. При удалении темы, интерфейс перекидывает на страницу с ошибкой 500.",
        "description":   "*Стенд:*\r\nhttp://pxl-bgsphere.stage.local.px-l.ru/\r\n\r\n*Описание дефекта:*\r\nВ списке тем, нажимаем и подтверждаем удаление темы.\r\n\r\n!https://pp.vk.me/c637422/v637422512/1ed3f/T0i8zuu7AOM.jpg!\r\n\r\nПосле подтверждения интерфейс перекидывает на страницу с ошибкой 500.\r\n\r\n!https://pp.vk.me/c637422/v637422512/1ed49/WTOqQTmIZXw.jpg!\r\n",
        "start_date":    "2016-11-08",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    20,
            "name":  "Время на постановку",
            "value": ""
        }],
        "created_on":    "2016-11-08T13:32:46Z",
        "updated_on":    "2016-11-30T13:25:05Z"
    }, {
        "id":            5361,
        "project":       {"id": 9, "name": "Pixel общий"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Собрание по вторникам",
        "description":   "",
        "start_date":    "2016-11-29",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-11-29T17:57:45Z",
        "updated_on":    "2016-11-29T17:57:45Z"
    }, {
        "id":            5360,
        "project":       {"id": 9, "name": "Pixel общий"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Общее",
        "description":   "",
        "start_date":    "2016-11-29",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-11-29T17:57:05Z",
        "updated_on":    "2016-11-29T17:57:05Z"
    }, {
        "id":              4675,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Переключение на АПИ Мобильной платформы",
        "description":     "",
        "start_date":      "2016-09-28",
        "done_ratio":      100,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-09-22T12:09:34Z",
        "updated_on":      "2016-11-28T09:05:24Z"
    }, {
        "id":            5310,
        "project":       {"id": 50, "name": "Малайзия"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Обсуждение админки",
        "description":   "",
        "start_date":    "2016-11-25",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-11-25T17:45:40Z",
        "updated_on":    "2016-11-25T17:45:40Z"
    }, {
        "id":            5271,
        "project":       {"id": 11, "name": "StatS"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "parent":        {"id": 4602},
        "subject":       "Починить поиск мос ид идентификоторов в логах",
        "description":   "Ввиду отсутствия места на машине backup-02.alx она была убита и заменена на 10.250.0.246",
        "start_date":    "2016-11-23",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-11-23T09:57:00Z",
        "updated_on":    "2016-11-24T13:55:42Z"
    }, {
        "id":              4916,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Заполнение параметров транзишена из формы",
        "description":     "",
        "start_date":      "2016-10-19",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-10-19T17:27:33Z",
        "updated_on":      "2016-11-24T12:47:21Z"
    }, {
        "id":            5279,
        "project":       {"id": 9, "name": "Pixel общий"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Общее",
        "description":   "",
        "start_date":    "2016-11-23",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-11-23T17:35:31Z",
        "updated_on":    "2016-11-23T17:35:31Z"
    }, {
        "id":              5240,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "На карте экранов выделить главный экран из конфига приложения",
        "description":     "",
        "start_date":      "2016-11-22",
        "done_ratio":      0,
        "estimated_hours": 0.5,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-22T14:03:26Z",
        "updated_on":      "2016-11-23T12:43:54Z"
    }, {
        "id":            4821,
        "project":       {"id": 8, "name": "Новостной виджет CityNews"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 62, "name": "Ян Комиссаров"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "parent":        {"id": 4785},
        "subject":       "Выкладка 4785",
        "description":   "http://gitlab8.alx/java/pxl-api-stats-widget/tree/4785",
        "start_date":    "2016-10-10",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-10-10T16:17:59Z",
        "updated_on":    "2016-11-22T14:58:48Z"
    }, {
        "id":            5243,
        "project":       {"id": 50, "name": "Малайзия"},
        "tracker":       {"id": 5, "name": "Задание"},
        "status":        {"id": 13, "name": "Решена"},
        "priority":      {"id": 2, "name": "Нормальный"},
        "author":        {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":   {"id": 5, "name": "Андрей Савчук"},
        "subject":       "Уточнение по событиям и транзишину в форме, постановка задач",
        "description":   "",
        "start_date":    "2016-11-22",
        "done_ratio":    0,
        "custom_fields": [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":    "2016-11-22T14:36:43Z",
        "updated_on":    "2016-11-22T14:36:43Z"
    }, {
        "id":              5238,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "У лайаута Pages не отображаются стили",
        "description":     "",
        "start_date":      "2016-11-22",
        "done_ratio":      0,
        "estimated_hours": 0.5,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-22T13:56:49Z",
        "updated_on":      "2016-11-22T14:01:53Z"
    }, {
        "id":              5063,
        "project":         {"id": 50, "name": "Малайзия"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Решена"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "subject":         "Поправить возможность ввести новую локаль",
        "description":     "",
        "start_date":      "2016-11-03",
        "done_ratio":      0,
        "estimated_hours": 0.5,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-03T17:43:47Z",
        "updated_on":      "2016-11-22T11:35:55Z"
    }], "total_count": 225, "offset": 0, "limit": 100
}