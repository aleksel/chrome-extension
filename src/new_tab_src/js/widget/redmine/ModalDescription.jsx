"use strict";

import * as React from "react";
import IssueDescription from "./IssueDescription";
import IssueAttachment from "./IssueAttachment";
import SaveTimeForm from "./SaveTimeForm";
import ElectronUtils from "../../utils/ElectronUtils";
import {Modal, Tab, Tabs} from "react-bootstrap";

export const addTokenToImgTag = (rmToken, description) => {
    return description.replace(/<img src="(.*?)"/g, `<img src="$1?key=${rmToken}"`);
};

export default class ModalDescription extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showdown: null,
            textile:  null,
        };
        require.ensure([], function (require) {
            const showdown = require('showdown');
            this.setState({showdown: new showdown.Converter()});
        }.bind(this), 'showdown');
        require.ensure([], function (require) {
            this.setState({textile: require('textile-js')});
        }.bind(this), 'textile-js');
    }

    static defaultProps = {
        spendMinutes:    0,
        spendHours:      0,
        showModal:       false,
        descriptionEdit: false,
    };

    static propTypes = {
        issue:                  React.PropTypes.object.isRequired,
        rmUrl:                  React.PropTypes.string.isRequired,
        getFormatTime:          React.PropTypes.func.isRequired,
        activityComment:        React.PropTypes.string,
        description:            React.PropTypes.string,
        rmToken:                React.PropTypes.string.isRequired,
        issueIsFetched:         React.PropTypes.bool.isRequired,
        issueLoading:           React.PropTypes.bool.isRequired,
        descriptionEdit:        React.PropTypes.bool.isRequired,
        showModal:              React.PropTypes.bool.isRequired,
        modalContent:           React.PropTypes.bool,
        issueReceived:          React.PropTypes.object.isRequired,
        projects:               React.PropTypes.object.isRequired,
        priorities:             React.PropTypes.object.isRequired,
        statuses:               React.PropTypes.object.isRequired,
        trackers:               React.PropTypes.object.isRequired,
        activities:             React.PropTypes.array.isRequired,
        users:                  React.PropTypes.object.isRequired,
        activityID:             React.PropTypes.number,
        spendHours:             React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.number]).isRequired,
        spendMinutes:           React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.number]).isRequired,
        onHideModal:            React.PropTypes.func.isRequired,
        onChangeSaveTimeInfo:   React.PropTypes.func.isRequired,
        onSaveSpendTime:        React.PropTypes.func.isRequired,
        onChangeDescription:    React.PropTypes.func.isRequired,
        onSaveDescription:      React.PropTypes.func.isRequired,
        onEditDescription:      React.PropTypes.func.isRequired,
        onHideModalCancelTimer: React.PropTypes.func.isRequired,
    };

    render() {
        const {
            issue, rmUrl, issueIsFetched, issueLoading, issueReceived, projects, priorities, statuses, trackers, users,
            rmToken, activities, activityID, activityComment, getFormatTime, showModal, modalContent, spendHours,
            spendMinutes, onHideModal, onChangeSaveTimeInfo, onSaveSpendTime, onHideModalCancelTimer,
            descriptionEdit, description, onChangeDescription, onEditDescription, onSaveDescription
        } = this.props;
        const {showdown, textile} = this.state;
        const issueLink = rmUrl + '/' + 'issues/' + issue.id;
        return <Modal show={showModal} backdrop={true} onHide={onHideModal} bsSize="lg">
            <Modal.Header>
                {!modalContent ? <span>Save spend Time: <span className="time-spend-header">
                            {getFormatTime(spendHours, spendMinutes)}
                            </span></span>
                    : <span>Task: <a onClick={ElectronUtils.onOpenIssue(issueLink)} href={issueLink}
                                     target="_blank"
                                     className="issue-id-link-title">#{issue.id}</a>, Subject: {issue.subject}</span>}
            </Modal.Header>
            <Modal.Body>
                {!modalContent ? <SaveTimeForm activities={activities}
                                               activityComment={activityComment}
                                               activityID={activityID}
                                               spendHours={spendHours}
                                               spendMinutes={spendMinutes}
                                               onChangeSaveTimeInfo={onChangeSaveTimeInfo}/> :
                    <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
                        <Tab eventKey={1} title="Description">
                            <br/>
                            <a style={{float: 'right', cursor: 'pointer'}}
                               onClick={onEditDescription}>
                                <i className="fa fa-pencil"></i>
                            </a>
                            <br/>
                            <br/>
                            {descriptionEdit ? <div className="form-group has-info">
                                <label style={{margin: 0, width: '100%'}}
                                       className="control-label default-text-color">
                                    <textarea value={description || issue.description}
                                              style={{height: '300px'}}
                                              onChange={onChangeDescription}
                                              type="text" className="form-control default-text-color"
                                              placeholder=""/>
                                </label>
                                {textile && showdown ? <div dangerouslySetInnerHTML={{
                                    __html: addTokenToImgTag(rmToken, textile(showdown.makeHtml(description || issue.description)))
                                }}></div> : null}
                            </div>
                                : <div>
                                    <div dangerouslySetInnerHTML={{
                                        __html: addTokenToImgTag(rmToken, textile(showdown.makeHtml(issue.description)))
                                    }}></div>
                                    {!issue.description ? '---' : null}
                                </div>}
                        </Tab>
                        <Tab eventKey={2} title={<div>History {issueReceived && issueReceived.journals
                            ? <span className="badge">{issueReceived.journals.length}</span> : null}</div>}>
                            <br/>
                            {issueLoading && !Object.keys(issueReceived).length ?
                                <div className="loading-item"><i className="loading-icon"></i></div> : null}
                            {issueIsFetched ? <IssueDescription journals={issueReceived.journals}
                                                                projects={projects}
                                                                rmToken={rmToken}
                                                                priorities={priorities}
                                                                statuses={statuses}
                                                                trackers={trackers}
                                                                users={users}/> : null}
                        </Tab>
                        <Tab eventKey={3} title={<div>Attachments {issueReceived && issueReceived.attachments
                            ? <span className="badge">{issueReceived.attachments.length}</span> : null}</div>}>
                            <br/>
                            {issueLoading && !Object.keys(issueReceived).length ?
                                <div className="loading-item"><i className="loading-icon"></i></div> : null}
                            {issueIsFetched ? <IssueAttachment attachments={issueReceived.attachments}
                                                               rmUrl={rmUrl}
                                                               rmToken={rmToken}/> : null}
                        </Tab>
                    </Tabs>}
            </Modal.Body>
            <Modal.Footer>
                {!modalContent ? <button disabled={spendMinutes === 0 && spendHours === 0}
                                         className="btn btn-flat" onClick={onSaveSpendTime}>Save Time
                </button> : null}
                {descriptionEdit ? <button className="btn btn-flat" onClick={onSaveDescription}>
                    Save
                </button> : null}
                {!modalContent ? <button className="btn btn-flat" onClick={onHideModal}>Continue work
                </button> : null}
                <button className="btn btn-flat" onClick={modalContent ? onHideModal : onHideModalCancelTimer}>
                    {modalContent && !descriptionEdit ? 'Close' : 'Cancel'}
                </button>
            </Modal.Footer>
        </Modal>;
    }
}