"use strict";

import * as React from "react";
import {DragDropContext} from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";
import Column from "./Column";
import Storage, {NOTIFIED_ISSUES} from "../../utils/Storage";
import "./redmine.css";

export const REDMINE_ICON = NODE_ENV === 'dev' ? '/image/redmine.png' : 'image/redmine.4c3120.png';
export const MARKDOWN_SYNTAX = 'http://daringfireball.net/projects/markdown/syntax';

export const SORT_CONFIG = [
    {name: 'Priority', key: '1', field: 'priority', getValue: item => item.priority.id},
    {name: 'Update time', key: '2', field: 'updated_on', getValue: item => Date.parse(item.updated_on)},
    {name: 'Create time', key: '3', field: 'created_on', getValue: item => Date.parse(item.updated_on)},
];

class Redmine extends React.Component {

    static propTypes = {
        bgColor:          React.PropTypes.shape({}),
        bgHeaderColor:    React.PropTypes.shape({}),
        header:           React.PropTypes.string,
        xPos:             React.PropTypes.number.isRequired,
        notifyTime:       React.PropTypes.number.isRequired,
        notifyActiveTime: React.PropTypes.number.isRequired,
        /*activeIssues:       React.PropTypes.arrayOf(React.PropTypes.shape({
         id:   React.PropTypes.number.isRequired,
         date: React.PropTypes.number.isRequired,
         }).isRequired),*/
        xMax:             React.PropTypes.number.isRequired,
    };

    cleanNotifiedIssues = props => {
        const {notifyTime} = props;
        const notifyTimeMillis = notifyTime * 60 * 1000;
        const notifiedIssues = Storage.get(NOTIFIED_ISSUES, true, false);
        const current = Date.now();
        for (let item in notifiedIssues) {
            if (notifiedIssues.hasOwnProperty(item) && notifiedIssues[item] + notifyTimeMillis < current) {
                delete notifiedIssues[item];
            }
        }
        Storage.save({[NOTIFIED_ISSUES]: notifiedIssues}, true);
    };

    componentDidMount() {
        this.cleanNotifiedIssues(this.props);
    }

    props: {
        issues: Array,
        issuesIsFetched: boolean,
        issuesLoading:   boolean,
        issuesError:     string | null,
    };

    onChange = (event, obj) => {
        const {issues} = this.state;
        const res = [];
        obj.data.item.status.id = parseInt(obj.data.status);
        obj.data.item.updated_on = new Date().toISOString();
        issues.forEach((issue, index) => res.push(issue.id === obj.data.item.id ? obj.data.item : issue));
        this.setState({issues: res});
    };

    render() {
        const {onChange} = this;
        const {status, header, xPos, xMax, bgColor, bgHeaderColor, notifyActiveTime} = this.props;
        const bColor = `rgba(${bgColor.r},${bgColor.g},${bgColor.b},${bgColor.a})`;

        return <div className="redmine-widget" style={{backgroundColor: bColor}}>
            <Column onChange={onChange}
                    bgHeaderColor={bgHeaderColor}
                    notifyActiveTime={notifyActiveTime}
                    header={header}
                    xPos={xPos}
                    xMax={xMax}
                    status={status}/>
        </div>;
    }
}

export default DragDropContext(HTML5Backend)(Redmine);