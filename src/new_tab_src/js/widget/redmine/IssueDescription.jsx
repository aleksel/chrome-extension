"use strict";

import * as React from "react";
import Timeline from "../../component/timeline/Timeline";
import ElectronUtils from "../../utils/ElectronUtils";
import {addTokenToImgTag} from "./ModalDescription";

export default class IssueDescription extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showdown: null,
            textile:  null,
        };
        require.ensure([], function (require) {
            const showdown = require('showdown');
            this.setState({showdown: new showdown.Converter()});
        }.bind(this), 'showdown');
        require.ensure([], function (require) {
            this.setState({textile: require('textile-js')});
        }.bind(this), 'textile-js');
    }

    static propTypes = {
        journals:   React.PropTypes.array.isRequired,
        users:      React.PropTypes.object.isRequired,
        rmToken:    React.PropTypes.string.isRequired,
        statuses:   React.PropTypes.object.isRequired,
        priorities: React.PropTypes.object.isRequired,
        trackers:   React.PropTypes.object.isRequired,
        projects:   React.PropTypes.object.isRequired,
    };

    createHistoryTimeline = journals => {
        const {rmToken} = this.props;
        const {showdown, textile} = this.state;
        return journals.sort((j1, j2) => {
            const j1CreatedOn = Date.parse(j1.created_on);
            const j2CreatedOn = Date.parse(j2.created_on);
            if (j1CreatedOn === j2CreatedOn) return 0;
            return j1CreatedOn > j2CreatedOn ? -1 : 1;
        }).map(journal => {
            let icon = journal.notes ? 'fa fa-comments-o' : 'fa fa-pencil-square-o';
            let badgeType = journal.notes ? 'primary' : 'info';
            const body = <div>
                {journal.notes ? <div>
                    {journal.details && journal.details.length
                        ? <span className="history-type"><i className="fa fa-comments-o"></i></span> : null}
                    {textile && showdown ? <span
                        dangerouslySetInnerHTML={{__html: addTokenToImgTag(rmToken, textile(showdown.makeHtml(journal.notes)))}}></span>
                        : null}
                </div> : null}
                {journal.details && journal.details.length ? journal.details.map((detail, index) => {
                    let currentIcon;
                    let currentBody;
                    if (detail.property === 'attachment') {
                        const attachmentDisc = this.getAttachmentDescription(icon, detail);
                        icon = attachmentDisc.icon;
                        currentIcon = attachmentDisc.currentIcon;
                        badgeType = attachmentDisc.badgeType;
                        currentBody = attachmentDisc.currentBody;
                    } else if (detail.property === 'attr') {
                        currentIcon = 'fa fa-pencil-square-o';
                        currentBody = this.getChangeIssueDescription(detail);
                    }
                    return <div key={index}>
                        <br/>
                        {journal.details.length > 1 || journal.notes
                            ? <span className="history-type"><i className={currentIcon}></i></span> : null}
                        <span>{currentBody}</span>
                    </div>;
                }) : null}
            </div>;
            return {
                time:   journal.created_on,
                person: journal.user.name,
                badgeType,
                body,
                icon,
            };
        });
    };

    getFieldNameAndValue = (field, detail) => {
        const {users, statuses, priorities, trackers, projects} = this.props;
        const newValue = detail.new_value === undefined ? null : detail.new_value;
        const oldValue = detail.old_value === undefined ? null : detail.old_value;
        switch (field) {
            case 'tracker_id':
                const trackerNew = newValue ? trackers[detail.new_value] : null;
                const trackerOld = oldValue ? trackers[detail.old_value] : null;
                return {
                    name:      'Tracker',
                    new_value: trackerNew ? trackerNew.name : newValue,
                    old_value: trackerOld ? trackerOld.name : oldValue,
                };
            case 'project_id':
                const projectNew = newValue ? projects[detail.new_value] : null;
                const projectOld = oldValue ? projects[detail.old_value] : null;
                return {
                    name:      'Project',
                    new_value: projectNew ? projectNew.name : newValue,
                    old_value: projectOld ? projectOld.name : oldValue,
                };
            case 'assigned_to_id':
                const userNew = newValue ? users[detail.new_value] : null;
                const userOld = oldValue ? users[detail.old_value] : null;
                return {
                    name:      'Assigned to',
                    new_value: userNew ? `${userNew.lastname} ${userNew.firstname}` : newValue,
                    old_value: userOld ? `${userOld.lastname} ${userOld.firstname}` : oldValue,
                };
            case 'status_id':
                const statusNew = newValue ? statuses[detail.new_value] : null;
                const statusOld = oldValue ? statuses[detail.old_value] : null;
                return {
                    name:      'Status',
                    new_value: statusNew ? statusNew.name : newValue,
                    old_value: statusOld ? statusOld.name : oldValue,
                };
            case 'priority_id':
                const priorityNew = newValue ? priorities[detail.new_value] : null;
                const priorityOld = oldValue ? priorities[detail.old_value] : null;
                return {
                    name:      'Priority',
                    new_value: priorityNew
                                   ? <span className={`badge redmine-priority-${priorityNew.id}`}>
                            {priorityNew.name}
                            </span> : newValue,
                    old_value: priorityOld ? <span className={`badge redmine-priority-${priorityOld.id}`}>
                            {priorityOld.name}
                            </span> : oldValue,
                };
            default:
                return {
                    name:      field.replace(/_/g, ' ').replace(/(^|\s)[a-z]/g, f => f.toUpperCase()),
                    new_value: newValue,
                    old_value: oldValue,
                };
        }
    };

    getChangeIssueDescription = detail => {
        const {name, new_value, old_value} = this.getFieldNameAndValue(detail.name, detail);
        if (new_value && old_value) {
            if (detail.name === 'description') {
                return <span>The value of field: <span
                    className="history-field-name">#{name} </span>
                was changed</span>;
            }
            return <span>The value of field: <span
                className="history-field-name">#{name} </span>
                was changed from: <span className="history-field-old-value">{old_value} </span>
                to: <span className="history-field-new-value">{new_value}</span></span>;
        } else if (new_value) {
            return <span>The value of field: <span
                className="history-field-name">#{name} </span>
                was changed to: <span className="history-field-new-value">{new_value}</span></span>;
        } else if (old_value) {
            return <span>The value of field: <span
                className="history-field-name">#{name} </span>
                was removed, old value: <span className="history-elem-remove">{old_value}</span></span>;
        }
    };

    getAttachmentDetailLink = detail => {
        const {rmUrl, rmToken} = this.props;
        return `${rmUrl}/attachments/download/${detail.name}/${detail.new_value || detail.old_value}?key=${rmToken}`;
    };

    getAttachmentDescription = (icon, detail) => {
        let currentIcon = 'fa fa-paperclip', badgeType, currentBody;
        if (icon !== 'fa fa-comments-o') {
            icon = currentIcon;
            badgeType = 'warning';
        }
        const attachmentLink = this.getAttachmentDetailLink(detail);
        if (detail.new_value) {
            currentBody = <span>Add attachment: <a onClick={ElectronUtils.onOpenIssue(attachmentLink)}
                                                   href={attachmentLink}
                                                   target="_blank"
                                                   className="issue-id-link-title">{detail.new_value}</a>
                                </span>;
        }
        if (detail.old_value) {
            currentBody = <span>Remove attachment: <a onClick={ElectronUtils.onOpenIssue(attachmentLink)}
                                                      href={attachmentLink}
                                                      target="_blank"
                                                      className="issue-id-link-title history-elem-remove">
                                    {detail.old_value}
                                    </a>
                                </span>;
        }
        return {icon, currentIcon, badgeType, currentBody};
    };

    render() {
        const {journals} = this.props;
        return <Timeline items={this.createHistoryTimeline(journals)}/>;
    }
}