export default {
    "issues":          [{
        "id":              4179,
        "project":         {"id": 38, "name": "Профилирование"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 4, "name": "Срочный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 4145},
        "subject":         "Пишем UI для stats профилирования",
        "description":     "Пишем UI для stats профилирования",
        "start_date":      "2016-08-01",
        "due_date":        "2016-12-13",
        "done_ratio":      91,
        "estimated_hours": 163.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-08-01T10:04:36Z",
        "updated_on":      "2016-12-12T11:55:53Z"
    }, {
        "id":              4182,
        "project":         {"id": 38, "name": "Профилирование"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 12, "name": "Новая"},
        "priority":        {"id": 4, "name": "Срочный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "fixed_version":   {"id": 57, "name": "Релиз 10"},
        "parent":          {"id": 4179},
        "subject":         "Реализация UI раздела Профилирования",
        "description":     "Реализация UI раздела Профилирования\r\nПроверка API, перепись на базу ClickHouse\r\n\r\n*Пункты (разделы) основного меню интерфейса:*\r\n\r\nСтатистика\r\nКатегории\r\nПодкатегории\r\nСайты",
        "start_date":      "2016-09-26",
        "due_date":        "2016-09-29",
        "done_ratio":      90,
        "estimated_hours": 134.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-08-01T10:08:51Z",
        "updated_on":      "2016-11-09T09:45:35Z"
    }, {
        "id":              4082,
        "project":         {"id": 19, "name": "Блогосфера"},
        "tracker":         {"id": 5, "name": "Задание"},
        "status":          {"id": 13, "name": "Новая"},
        "priority":        {"id": 3, "name": "Высокий"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "fixed_version":   {"id": 58, "name": "Релиз 2. Итерация 3"},
        "subject":         "Перепись АПИ блогосферы",
        "description":     "Бэк выкатывать на стенд\r\nhttp://taskmanager.stage.local.px-l.ru/",
        "start_date":      "2016-07-18",
        "done_ratio":      32,
        "estimated_hours": 170.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-07-18T10:16:12Z",
        "updated_on":      "2016-12-01T08:59:49Z"
    }, {
        "id":              5569,
        "project":         {"id": 8, "name": "Новостной виджет CityNews"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 12, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 5, "name": "Андрей Савчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 4146},
        "subject":         "Доработка АПИ методов",
        "description":     "1.  Добавить методы для раздельного получения данных по: Статистики виджета по показам и по переходам\r\n2. Добавить сортировку в статистике по виджету\r\n3. Добавить метод для выгрузки конкретной страницы статистики показов и переходов\r\n4. Добавить дефолтную сортировку для доменов",
        "start_date":      "2016-12-12",
        "done_ratio":      0,
        "estimated_hours": 2.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-12-12T12:27:34Z",
        "updated_on":      "2016-12-12T13:31:46Z"
    }, {
        "id":              5037,
        "project":         {"id": 11, "name": "StatS"},
        "tracker":         {"id": 7, "name": "Разработка"},
        "status":          {"id": 7, "name": "Новая"},
        "priority":        {"id": 2, "name": "Нормальный"},
        "author":          {"id": 7, "name": "Руслана Яремчук"},
        "assigned_to":     {"id": 5, "name": "Андрей Савчук"},
        "parent":          {"id": 5036},
        "subject":         "Выгрузка статистики по итогам показа поп-апа \"Новый адрес ПГУ\" ",
        "description":     "Прекратить показ поп-апа 15.12.2016 в 10:00 a.m.\r\n\r\nПосле чего - подготовить таблицу с данными о показах поп-апа и кликах по ссылке в поп-апе - за весь период его показа (с 2.11 по 15.12).\r\n\r\nФормат таблицы - cvs с полями:\r\n1) mosid \r\n2) дата и время просмотра поп-апа данным mosid \r\n3) факт перехода по ссылке в поп-апе данным mosid  - true или <пусто>\r\n4) дата и время перехода по ссылке в поп-апе данным mosid \r\n5) ssoid (идентификатор, принадлежащий mosid - может быть несколько - указывать все через разделитель)",
        "start_date":      "2016-11-02",
        "done_ratio":      0,
        "estimated_hours": 1.0,
        "custom_fields":   [{"id": 1, "name": "Стенды", "multiple": true, "value": ["Stage"]}, {
            "id":    2,
            "name":  "Сложность",
            "value": ""
        }, {"id": 17, "name": "Источник задачи", "value": "План"}, {
            "id":    19,
            "name":  "Внешняя задача",
            "value": ""
        }, {"id": 20, "name": "Время на постановку", "value": ""}],
        "created_on":      "2016-11-02T09:49:54Z",
        "updated_on":      "2016-12-06T11:22:05Z"
    }], "total_count": 225, "offset": 0, "limit": 100
}