"use strict";

import * as React from "react";
import ElectronUtils from "../../utils/ElectronUtils";
import {OverlayTrigger, Tooltip} from "react-bootstrap";

export default class IssueAttachment extends React.Component {

    static propTypes = {
        attachments: React.PropTypes.array.isRequired,
        rmUrl:       React.PropTypes.string.isRequired,
        rmToken:     React.PropTypes.string.isRequired,
    };

    getAttachmentLink = attachment => {
        const {rmUrl, rmToken} = this.props;
        return `${rmUrl}/attachments/download/${attachment.id}/${attachment.filename}?key=${rmToken}`;
    };

    createAttachment = attachments => {
        return attachments.map(attachment => {
            const link = this.getAttachmentLink(attachment);
            const date = new Date(Date.parse(attachment.created_on));
            return <div key={attachment.id}>
                <div>
                    <i className="fa fa-paperclip"></i> <a onClick={ElectronUtils.onOpenIssue(link)}
                                                           href={link}
                                                           target="_blank"
                                                           className="issue-id-link-title">{attachment.filename}</a>
                    <span> ({(attachment.filesize / 1024 / 1024).toFixed(2)} Mb)</span>
                </div>
                <div>@{attachment.author.name}, <OverlayTrigger placement="top"
                                                                overlay={this.getTooltipDate(date)}
                                                                rootClose={true}>
                                    <span>
                                        {date.duration()}
                                    </span>
                </OverlayTrigger>
                </div>
                <hr/>
            </div>;
        });
    };

    getTooltipDate = date => {
        return <Tooltip id="tooltip">
            <strong>{date.formatDate('yyyy-MM-dd')}</strong> {date.formatDate('HH:mm:ss')}
        </Tooltip>;
    };

    render() {
        const {attachments} = this.props;
        return <div>{this.createAttachment(attachments)}</div>;
    }
}