"use strict";

import * as React from "react";
import {DropTarget} from "react-dnd";
import Issue from "./Issue";
import {SORT_CONFIG} from "./Redmine";
import {SORT_ASC_DIRECTION, SORT_DESC_DIRECTION} from "../../App";
import {OverlayTrigger, Popover} from "react-bootstrap";
import Toggle from "../../component/toggle/Toggle";
import {MENU_WIDGETS} from "../../component/settings/Settings";
import {REDMINE_WIDGET} from "../../component/settings/menu/Widgets";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as IssuesActions from "../../actions/issues";
import * as SettingsActions from "../../actions/settings";
import * as ActiveIssuesActions from "../../actions/activeIssues";
import * as DictionariesActions from "../../actions/dictionary";
import ModalNewIssue from "./ModalNewIssue";

class Column extends React.Component {

    timer = null;

    state = {
        showModalNewIssue: false,
    };

    static propTypes = {
        issues:               React.PropTypes.arrayOf(React.PropTypes.shape({}).isRequired),
        rmUrl:                React.PropTypes.string,
        bgHeaderColor:        React.PropTypes.shape({}),
        rmToken:              React.PropTypes.string,
        decorateInverseColor: React.PropTypes.bool,
        decorateCrossLine:    React.PropTypes.bool,
        activeTimer:          React.PropTypes.bool,
        header:               React.PropTypes.string,
        xPos:                 React.PropTypes.number.isRequired,
        notifyActiveTime:     React.PropTypes.number.isRequired,
        xMax:                 React.PropTypes.number.isRequired,
        activeIssues:         React.PropTypes.arrayOf(React.PropTypes.shape({
            id:   React.PropTypes.number.isRequired,
            date: React.PropTypes.number.isRequired,
        }).isRequired),
    };

    static USERS_LOADING = false;
    static PROJECTS_LOADING = false;

    componentDidMount() {
        const {
            issuesIsFetched, issuesLoading, settings, issueActions, status, activeIssuesActions, activeIssuesLoading,
            activeIssuesIsFetched, justIsNew, dictionariesActions, usersIsFetched, usersLoading, rmToken, rmUrl,
            projectsIsFetched, projectsLoading
        } = this.props;
        if (settings.url && !issuesIsFetched && !issuesLoading && !justIsNew) {
            this.loadIssues(true)(this.props);
        }
        if (!activeIssuesIsFetched && !activeIssuesLoading) {
            activeIssuesActions.load();
        }
        if (!Column.USERS_LOADING && !usersIsFetched && !usersLoading) {
            Column.USERS_LOADING = true;
            dictionariesActions.loadUsers(rmUrl, rmToken);
        }
        if (!Column.PROJECTS_LOADING && !projectsIsFetched && !projectsLoading) {
            Column.PROJECTS_LOADING = true;
            dictionariesActions.loadProjects(rmUrl, rmToken);
        }
        this.timer = this.initTimer();
    }

    initTimer = () => {
        if (this.props.settings.ut < 1) return null;
        return setInterval(() => {
            const {issuesIsFetched, issuesLoading, settings} = this.props;
            if (settings.url && issuesIsFetched && !issuesLoading) {
                this.loadIssues(true)(this.props);
            }
        }, this.props.settings.ut * 60000);
    };

    componentWillUnmount() {
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = null;
        }
    }

    componentWillReceiveProps(props) {
        clearInterval(this.timer);
        this.timer = this.initTimer();
    }

    static getSortString = (props, gotStatus = null) => {
        let sortString = '';
        const sortConfig = {};
        SORT_CONFIG.forEach(config => sortConfig[config.key] = config);
        const {settings} = props;
        const status = gotStatus || props.status;
        const sort = settings && settings.ls && settings.ls[status] && settings.ls[status].s ? settings.ls[status].s : [];
        sort.forEach(item => {
            sortString += sortConfig[item.f]['field'] + (item.d === SORT_ASC_DIRECTION ? '' : ':desc') + ',';
        });
        if (sortString) {
            sortString = sortString.substring(0, sortString.length - 1);
        }
        return sortString;
    };

    onChangeSort = (key, props) => event => {
        const {settings, settingsActions, status, issueActions} = props;
        let sort = [...settings.ls[status].s || []];
        const sortExists = sort.find(sortTemp => sortTemp.f === key);
        if (sortExists && sortExists.d === SORT_DESC_DIRECTION) {
            sort = sort.filter(sortTemp => {
                if (sortTemp.i > sortExists.i) {
                    --sortTemp.i;
                }
                return sortTemp.f !== key
            });
        } else if (sortExists) {
            sort.forEach(sortTemp => {
                if (sortTemp.f === key) {
                    sortTemp.d = SORT_DESC_DIRECTION;
                }
            });
        } else {
            let maxIndex = 0;
            sort.forEach(item => {
                if (item.i > maxIndex) {
                    maxIndex = item.i;
                }
            });
            sort.push({f: key, d: SORT_ASC_DIRECTION, i: maxIndex + 1});
        }
        settings.ls[status].s = sort;
        settingsActions.changeSecondSettings(settings, MENU_WIDGETS, REDMINE_WIDGET);
        this.loadIssues()(props);
    };

    loadIssues = (notify = false) => (props = this.props, assign = null, project = null) => {
        const {settings, status, issueActions} = props;
        assign = assign || (settings && settings.ls[status] && settings.ls[status].a ? settings.ls[status].a : 'me');
        project = project || (settings && settings.ls[status] && settings.ls[status].p ? settings.ls[status].p : '');
        issueActions.loadIssues(settings.url, settings.token, assign, project, status, Column.getSortString(this.props),
            settings.fc, notify ? settings.n : 0, (issueID, updatedOn) => issueActions.resetIsNew(status, issueID, updatedOn));
    };

    getSortArrow = (direction, index) => {
        return direction === SORT_DESC_DIRECTION
            ? <i className="fa fa-long-arrow-down"><sub>{index}</sub></i>
            : <i className="fa fa-long-arrow-up"><sub>{index}</sub></i>;
    };

    onChangeSettings = (field, defaultValue) => event => {
        const {settings, status, settingsActions} = this.props;
        settings.ls[status][field] = settings.ls[status][field] !== undefined ? !settings.ls[status][field] : !defaultValue;
        settingsActions.changeSecondSettings(settings, MENU_WIDGETS, REDMINE_WIDGET);
    };

    onChangeAssignFilterSettings = event => {
        const {settings, status, settingsActions} = this.props;
        settings.ls[status].a = event.target.value;
        this.loadIssues()(this.props, event.target.value);
        settingsActions.changeSecondSettings(settings, MENU_WIDGETS, REDMINE_WIDGET);
    };

    onChangeProjectFilterSettings = event => {
        const {settings, status, settingsActions} = this.props;
        settings.ls[status].p = event.target.value;
        this.loadIssues()(this.props, null, event.target.value);
        settingsActions.changeSecondSettings(settings, MENU_WIDGETS, REDMINE_WIDGET);
    };

    onRefreshIssues = () => {
        this.loadIssues(true)(this.props);
        if (this.props.activeTimer) {
            const {settings, issues, issueActions} = this.props;
            issues.forEach(issue => issueActions.loadIssue(settings.url, settings.token, issue.id));
        }
    };

    getWidgetSettingsPopover = () => {
        const {settings, status, users, projects} = this.props;
        const {
            onChangeSort, getSortArrow, onChangeSettings, onChangeAssignFilterSettings, onChangeProjectFilterSettings,
        } = this;
        const sort = {};
        const assign = settings && settings.ls[status] && settings.ls[status].a ? settings.ls[status].a : '';
        const project = settings && settings.ls[status] && settings.ls[status].p ? settings.ls[status].p : '';
        (settings && settings.ls[status] && settings.ls[status].s ? settings.ls[status].s : [])
            .forEach((sortTemp, index) => sort[sortTemp.f] = sortTemp);
        const usersArray = [];
        const projectsArray = [];
        for (let user in users) {
            if (users.hasOwnProperty(user)) {
                usersArray.push(users[user]);
            }
        }
        usersArray.sort((u1, u2) => {
            if (u1.lastname === u2.lastname) return 0;
            return u1.lastname > u2.lastname ? 1 : -1;
        });
        for (let project in projects) {
            if (projects.hasOwnProperty(project)) {
                projectsArray.push(projects[project]);
            }
        }
        projectsArray.sort((p1, p2) => {
            if (p1.name === p2.name) return 0;
            return p1.name > p2.name ? 1 : -1;
        });

        return <Popover id="widget-settings-click">
            <div className="form-group">
                <h4>Filter</h4>
                <div className="widget-settings-option form-group has-info" style={{marginTop: -25}}>
                    <label style={{width: '100%'}} className="control-label default-text-color">
                        <select value={assign}
                                onChange={onChangeAssignFilterSettings}
                                type="text" className="form-control default-text-color" placeholder="">
                            <option value="a">All</option>
                            <option value="me">me</option>
                            {usersArray.map(user => <option key={user.id}
                                                            value={user.id}>{user.lastname} {user.firstname}</option>)}
                        </select>
                        <sub className="default-text-color">Assign</sub>
                    </label>
                </div>
                <div className="widget-settings-option form-group has-info" style={{marginTop: -35}}>
                    <label style={{width: '100%'}} className="control-label default-text-color">
                        <select value={project}
                                onChange={onChangeProjectFilterSettings}
                                type="text" className="form-control default-text-color" placeholder="">
                            <option value="a">All</option>
                            {projectsArray.map(project => <option key={project.id}
                                                                  value={project.id}>{project.name}</option>)}
                        </select>
                        <sub className="default-text-color">Project</sub>
                    </label>
                </div>
                <br/>
                <h4>Sort By</h4>
                <div className="widget-settings-option">
                    {SORT_CONFIG.map((sortTemp, index) => <span key={sortTemp.key}
                                                                onClick={onChangeSort(sortTemp.key, this.props)}>
                        {sortTemp.name} {sort[sortTemp.key]
                        ? getSortArrow(sort[sortTemp.key].d, sort[sortTemp.key].i)
                        : <i></i>}
                    </span>)}
                </div>
                <br/>
                <h4>Decorate</h4>
                <div className="widget-settings-option" style={{paddingBottom: 0}}>
                    <Toggle label="Tasks are done"
                            labelClassName="options-toggle-label"
                            toggleClassName="options-toggle"
                            checked={settings && settings.ls[status] && settings.ls[status].dt !== undefined
                                ? settings.ls[status].dt : false}
                            onChange={onChangeSettings('dt', false)}/>
                    <Toggle label="Inverse color"
                            labelClassName="options-toggle-label"
                            toggleClassName="options-toggle"
                            checked={settings && settings.ls[status] && settings.ls[status].ic !== undefined
                                ? settings.ls[status].ic : true}
                            onChange={onChangeSettings('ic', true)}/>
                    <Toggle label="Active timer"
                            labelClassName="options-toggle-label"
                            toggleClassName="options-toggle"
                            checked={settings && settings.ls[status] && settings.ls[status].ti !== undefined
                                ? settings.ls[status].ti : false}
                            onChange={onChangeSettings('ti', false)}/>
                    <Toggle label="Active new Issue"
                            labelClassName="options-toggle-label"
                            toggleClassName="options-toggle"
                            checked={settings && settings.ls[status] && settings.ls[status].ni !== undefined
                                ? settings.ls[status].ni : false}
                            onChange={onChangeSettings('ni', false)}/>
                </div>
            </div>
        </Popover>;
    };

    onHideModalNewIssue = () => this.setState({showModalNewIssue: false});

    onSaveNewIssue = issue => {
        const {issueActions, rmUrl, rmToken} = this.props;
        issueActions.createIssue(rmUrl, rmToken, issue, () => this.loadIssues(true)(this.props));
        this.setState({showModalNewIssue: false});
    };

    render() {
        const {
            isDragging, connectDropTarget, rmUrl, xPos, xMax, activeTimer, rmToken, header, issuesError, issuesCount,
            decorateCrossLine, decorateInverseColor, activeIssues, activeIssuesActions, issuesLoading, issues, isOver,
            bgHeaderColor, notifyActiveTime, settings, status, priorities, users, projects, trackers, user
        } = this.props;
        const {showModalNewIssue} = this.state;
        const {getWidgetSettingsPopover, onRefreshIssues, loadIssues, onHideModalNewIssue, onSaveNewIssue} = this;
        const bHeaderColor = `rgba(${bgHeaderColor.r},${bgHeaderColor.g},${bgHeaderColor.b},${bgHeaderColor.a})`;

        return <div>
            <ModalNewIssue
                priorities={priorities}
                users={users}
                user={user.id}
                projects={projects}
                status={status}
                trackers={trackers}
                statusName={header}
                onSave={onSaveNewIssue}
                onHideModal={onHideModalNewIssue}
                showModal={showModalNewIssue}/>
            <span className="widget-header" style={{backgroundColor: bHeaderColor}}>
                <div className="settings-window-options-redmine-icon"></div>
                <span>
                    {header}
                </span> <small><span className="badge rm-column-badge">
                    {issues.length + ''} of {issuesCount}
                </span></small></span>
            <OverlayTrigger trigger="click" placement="bottom" overlay={getWidgetSettingsPopover()} rootClose={true}>
                <button className="redmine-widget-status-settings">
                    <i className="fa fa-sliders" aria-hidden="true"></i>
                </button>
            </OverlayTrigger>
            <button className="redmine-widget-status-reload" onClick={onRefreshIssues}>
                <i className="fa fa-refresh" aria-hidden="true"></i>
            </button>
            {settings && settings.ls[status] && settings.ls[status].ni
                ? <button className="redmine-widget-status-add-issue"
                          onClick={() => this.setState({showModalNewIssue: true})}>
                    <i className="fa fa-plus" aria-hidden="true"></i>
                </button> : null}
            {issuesLoading && !issues.length ?
                <div className="loading-item"><i className="loading-icon"></i></div> : null}
            {issuesError ? <div className="error-item">
                ERROR: {issuesError.errorCode ? `(${issuesError.errorCode}) ` : ''}{issuesError.errorMessage}
            </div> : null}
            {connectDropTarget(<div style={{height: '100%', opacity: isOver ? 0.5 : 1}}>
                {issues.map(issue => <Issue decorateCrossLine={decorateCrossLine}
                                            activeIssues={activeIssues}
                                            decorateInverseColor={decorateInverseColor}
                                            onChangeTaskActive={activeIssuesActions.changeActive}
                                            activeTimer={activeTimer}
                                            loadIssues={loadIssues()}
                                            notifyActiveTime={notifyActiveTime}
                                            assign={settings && settings.ls[status] && settings.ls[status].a
                                                ? settings.ls[status].a : 'me'}
                                            key={issue.id}
                                            issue={issue}
                                            rmUrl={rmUrl}
                                            rmToken={rmToken}
                                            xPos={xPos}
                                            xMax={xMax}/>)}

            </div>)}
        </div>;
    }
}

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver:            monitor.isOver(),
        canDrop:           monitor.canDrop(),
        isOverCurrent:     monitor.isOver({shallow: true})
    };
}

const squareTarget = {
    drop(props, monitor, component) {
        const item = monitor.getItem();
        const statusTo = parseInt(props.status);
        if (statusTo !== item.status.id) {
            const assignTo = props.settings && props.settings.ls[statusTo] && props.settings.ls[statusTo].a
                ? props.settings.ls[statusTo].a : 'me';
            const projectTo = props.settings && props.settings.ls[statusTo] && props.settings.ls[statusTo].p
                ? props.settings.ls[statusTo].p : '';
            const assign = props.settings && props.settings.ls[item.status.id] && props.settings.ls[item.status.id].a
                ? props.settings.ls[item.status.id].a : 'me';
            const project = props.settings && props.settings.ls[item.status.id] && props.settings.ls[item.status.id].p
                ? props.settings.ls[item.status.id].p : '';
            props.issueActions.changeStatus(props.settings.url, props.settings.token,
                item.id, statusTo, () => {
                    props.issueActions.loadIssues(props.settings.url, props.settings.token, assignTo, projectTo,
                        statusTo, Column.getSortString(props, statusTo), props.settings.fc, 0,
                        (issueID, updatedOn) => props.issueActions.resetIsNew(statusTo, issueID, updatedOn));
                    props.issueActions.loadIssues(props.settings.url, props.settings.token, assign, project,
                        item.status.id, Column.getSortString(props, item.status.id), props.settings.fc, 0,
                        (issueID, updatedOn) => props.issueActions.resetIsNew(item.status.id, issueID, updatedOn));
                });
        }
    },
    canDrop(props, monitor) {
        return true;
    },
};

function mapStateToProps(state, props) {
    const settingsNew = state.settings[MENU_WIDGETS][REDMINE_WIDGET];
    const {status} = props;
    const issues = state.issues[status] ? state.issues[status] : {
        elems:     [],
        isFetched: false,
        loading:   false,
        error:     null,
        count:     0,
    };
    return {
        issues:                issues.elems,
        justIsNew:             issues.justIsNew,
        issuesIsFetched:       issues.isFetched,
        issuesLoading:         issues.loading,
        issuesError:           issues.error,
        issuesCount:           issues.count,
        settings:              settingsNew,
        rmUrl:                 settingsNew.url,
        rmToken:               settingsNew.token,
        activeIssues:          state.activeIssues.elems,
        activeIssuesIsFetched: state.activeIssues.isFetched,
        activeIssuesLoading:   state.activeIssues.loading,
        users:                 state.users.elems,
        user:                  state.user.elem,
        usersIsFetched:        state.users.isFetched,
        usersLoading:          state.users.loading,
        priorities:            state.priorities.elems,
        trackers:              state.trackers.elems,
        projects:              state.projects.elems,
        projectsIsFetched:     state.projects.isFetched,
        projectsLoading:       state.projects.loading,
        decorateCrossLine:     settingsNew && settingsNew.ls[status] && settingsNew.ls[status].dt !== undefined
                                   ? settingsNew.ls[status].dt : false,
        activeTimer:           settingsNew && settingsNew.ls[status] && settingsNew.ls[status].ti !== undefined
                                   ? settingsNew.ls[status].ti : false,
        decorateInverseColor:  settingsNew && settingsNew.ls[status] && settingsNew.ls[status].ic !== undefined
                                   ? settingsNew.ls[status].ic : true,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        issueActions:        bindActionCreators(IssuesActions, dispatch),
        activeIssuesActions: bindActionCreators(ActiveIssuesActions, dispatch),
        settingsActions:     bindActionCreators(SettingsActions, dispatch),
        dictionariesActions: bindActionCreators(DictionariesActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DropTarget('VIEW', squareTarget, collect)(Column));