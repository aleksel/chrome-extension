"use strict";

import * as React from "react";
import {DragSource} from "react-dnd";
import {Modal, OverlayTrigger, Popover, ProgressBar, Tooltip} from "react-bootstrap";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as IssuesActions from "../../actions/issues";
import * as DictionariesActions from "../../actions/dictionary";
import Notify from "../../utils/Notify";
import ElectronUtils from "../../utils/ElectronUtils";
import ModalDescription, {addTokenToImgTag} from "./ModalDescription";
import {hljs, MARKDOWN_SYNTAX, REDMINE_ICON} from "./Redmine";

class Issue extends React.Component {

    timer = null;

    constructor(props) {
        super(props);
        require.ensure([], function (require) {
            const showdown = require('showdown');
            this.setState({showdown: new showdown.Converter()});
        }.bind(this), 'showdown');
        require.ensure([], function (require) {
            this.setState({textile: require('textile-js')});
        }.bind(this), 'textile-js');
        require.ensure([], function (require) {
            require('highlight.js/styles/dracula.css');
        }.bind(this), 'highlight.js/styles/dracula.css');
        require.ensure([], function (require) {
            this.setState({hljs: require('highlight.js')});
        }.bind(this), 'highlight.js');
    }

    state = {
        showdown:         null,
        textile:          null,
        hljs:             null,
        assign:           '',
        priority:         '',
        project:          '',
        subject:          '',
        comment:          '',
        description:      '',
        descriptionEdit:  false,
        estimatedTime:    '',
        doneRatio:        '',
        activityID:       '',
        activityComment:  '',
        showModal:        false,
        showModalComment: false,
        modalContent:     null,
        spendMinutes:     0,
        spendHours:       0,
    };

    static USER_LOADING = false;
    static USERS_LOADING = false;
    static STATUSES_LOADING = false;
    static PRIORITIES_LOADING = false;
    static TRACKERS_LOADING = false;
    static PROJECTS_LOADING = false;

    componentDidMount() {
        $(document).on('CLICK_NOTIFY_ISSUE', this.onShowDescriptionModal);
        this.initActiveTimer(this.props);
    }

    initActiveTimer = props => {
        const {
            issue, rmToken, rmUrl, activeIssues, activeTimer, issuesActions, issueIsFetched, issueLoading,
            activityIsFetched, activityLoading, dictionariesActions, notifyActiveTime, usersIsFetched,
            usersLoading, statusesIsFetched, statusesLoading, prioritiesIsFetched, prioritiesLoading,
            trackersIsFetched, trackersLoading, projectsIsFetched, projectsLoading, userIsFetched, userLoading
        } = props;
        if (rmUrl && rmToken) {
            if (activeTimer && !issueIsFetched && !issueLoading) {
                issuesActions.loadIssue(rmUrl, rmToken, issue.id);
            }
            if (activeTimer && !activityIsFetched && !activityLoading) {
                dictionariesActions.loadActivities(rmUrl, rmToken);
            }
            if (!Issue.USER_LOADING && !userIsFetched && !userLoading) {
                Issue.USER_LOADING = true;
                dictionariesActions.getUser(rmUrl, rmToken);
            }
            if (!Issue.USERS_LOADING && !usersIsFetched && !usersLoading) {
                Issue.USERS_LOADING = true;
                dictionariesActions.loadUsers(rmUrl, rmToken);
            }
            if (!Issue.STATUSES_LOADING && !statusesIsFetched && !statusesLoading) {
                Issue.STATUSES_LOADING = true;
                dictionariesActions.loadStatuses(rmUrl, rmToken);
            }
            if (!Issue.PRIORITIES_LOADING && !prioritiesIsFetched && !prioritiesLoading) {
                Issue.PRIORITIES_LOADING = true;
                dictionariesActions.loadPriorities(rmUrl, rmToken);
            }
            if (!Issue.TRACKERS_LOADING && !trackersIsFetched && !trackersLoading) {
                Issue.TRACKERS_LOADING = true;
                dictionariesActions.loadTrackers(rmUrl, rmToken);
            }
            if (!Issue.PROJECTS_LOADING && !projectsIsFetched && !projectsLoading) {
                Issue.PROJECTS_LOADING = true;
                dictionariesActions.loadProjects(rmUrl, rmToken);
            }
        }
        const active = activeIssues.find(aIssue => aIssue.id === issue.id);
        if (active && !this.timer) {
            this.timer = setInterval(() => {
                const {spendMinutes, showModal} = this.state;
                const tempTime = new Date().getPastTimeTo(new Date(active.date));
                const minutes = tempTime.minutes;
                const hours = tempTime.hours;

                if (spendMinutes !== minutes && minutes !== 0 && !showModal) {
                    if (notifyActiveTime !== 0 && (hours * 60 + minutes) % notifyActiveTime === 0) {
                        Notify.notifyHtml5(`Issue #${issue.id} in work. Past: ${this.getFormatTime(hours, minutes)}`,
                            'Just a friendly reminder, are you still working on the task?',
                            REDMINE_ICON,
                            this.onChangeTaskActive);
                    }
                    this.setState({spendMinutes: minutes, spendHours: hours,});
                }
            }, 1000);
        }
    };

    static propTypes = {
        issue:                React.PropTypes.shape({}).isRequired,
        rmUrl:                React.PropTypes.string,
        rmToken:              React.PropTypes.string,
        assign:               React.PropTypes.string.isRequired,
        notifyActiveTime:     React.PropTypes.number.isRequired,
        xPos:                 React.PropTypes.number.isRequired,
        xMax:                 React.PropTypes.number.isRequired,
        decorateCrossLine:    React.PropTypes.bool,
        activeTimer:          React.PropTypes.bool,
        decorateInverseColor: React.PropTypes.bool,
        onChangeTaskActive:   React.PropTypes.func.isRequired,
        loadIssues:           React.PropTypes.func.isRequired,
        activeIssues:         React.PropTypes.arrayOf(React.PropTypes.shape({
            id:   React.PropTypes.number.isRequired,
            date: React.PropTypes.number.isRequired,
        }).isRequired),
    };

    componentWillReceiveProps(props) {
        this.initActiveTimer(props);
    }

    componentWillUnmount() {
        $(document).off('CLICK_NOTIFY_ISSUE', this.onShowDescriptionModal);
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = null;
        }
    }

    getTooltipDate = date => {
        return <Tooltip id="tooltip">
            <strong>{date.formatDate('yyyy-MM-dd')}</strong> {date.formatDate('HH:mm:ss')}
        </Tooltip>;
    };

    onChangeTaskActive = () => {
        const {onChangeTaskActive, issue, activeIssues} = this.props;
        const active = activeIssues.find(aIssue => aIssue.id === issue.id);
        const play = !(active && this.timer);
        if (!play) {
            if (ELECTRON) {
                const {ipcRenderer} = require('electron');
                ipcRenderer.send('focus-main-window');
            }
            this.setState({showModal: true, modalContent: null});
        } else {
            onChangeTaskActive(issue.id, play);
        }
    };

    getFormatTime = (hours, minutes) => {
        hours += '';
        minutes += '';
        if (hours.length === 1) {
            hours = '0' + hours;
        }
        if (minutes.length === 1) {
            minutes = '0' + minutes;
        }
        return hours + ':' + minutes;
    };

    renderTimerAction = (className) => {
        const {spendHours, spendMinutes} = this.state;
        let {issueReceived} = this.props;
        let {hours, minutes} = issueReceived.spend || {hours: 0, minutes: 0};
        const spendTime = this.getFormatTime(hours, minutes);
        const spendTimeCurrent = this.getFormatTime(spendHours, spendMinutes);
        return <span>
            <span className="issue-timer-spend-all">{spendTime}</span> <i
            className={"fa fa-" + className}>
        </i> <span className="issue-timer-spend-current">{className === 'stop' ? `+${spendTimeCurrent}` : null}</span>
        </span>
    };

    onShowDescriptionModal = (event, data) => {
        const {issue, issuesActions, rmUrl, rmToken} = this.props;
        if (data && data.data && data.data !== issue.id) return;
        issuesActions.loadIssue(rmUrl, rmToken, issue.id);
        this.setState({
            showModal:    true,
            modalContent: true,
        });
    };

    changeIsNew = (issueStatusID, issueID, updatedOn) => event =>
        this.props.issuesActions.resetIsNew(issueStatusID, issueID, updatedOn);

    onHideModal = () => this.setState({showModal: false, description: '', descriptionEdit: false});

    intFields = ['activityID', 'spendHours', 'spendMinutes'];
    onChangeSaveTimeInfo = key => event =>
        this.setState({[key]: this.intFields.indexOf(key) !== -1 ? parseInt(event.target.value) : event.target.value});

    onSaveSpendTime = () => {
        const {spendHours, spendMinutes, activityID, activityComment} = this.state;
        const {issuesActions, issue, rmToken, rmUrl, activities, onChangeTaskActive} = this.props;
        const hours = spendHours + Math.round(spendMinutes / 60 * 100) / 100;
        issuesActions.saveSpendTime(rmUrl, rmToken, issue.id, hours, activityID
                ? activityID : activities[0].id, activityComment,
            () => issuesActions.loadIssue(rmUrl, rmToken, issue.id));
        onChangeTaskActive(issue.id, false);
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = null;
        }
        this.setState({
            showModal:       false,
            modalContent:    null,
            activityComment: '',
            activityID:      '',
            spendHours:      0,
            spendMinutes:    0
        });
    };

    onHideModalCancelTimer = () => {
        const {onChangeTaskActive, issue, activeIssues} = this.props;
        const active = activeIssues.find(aIssue => aIssue.id === issue.id);
        const play = !(active && this.timer);
        onChangeTaskActive(issue.id, play);
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = null;
        }
        this.setState({
            showModal:       false,
            modalContent:    null,
            activityComment: '',
            activityID:      '',
            spendHours:      0,
            spendMinutes:    0
        });
    };

    onChangeField = (fieldName, saveFieldName) => event => {
        const {loadIssues, issuesActions, rmUrl, rmToken, issue} = this.props;
        if (!this.state[fieldName]) return;
        let value;
        if (fieldName === 'estimatedTime') {
            value = parseFloat(this.state[fieldName]);
        } else if (fieldName === 'comment' || fieldName === 'description' || fieldName === 'subject') {
            value = this.state[fieldName];
        } else {
            value = parseInt(this.state[fieldName]);
        }
        issuesActions.saveIssue(rmUrl, rmToken, issue.id, {issue: {[saveFieldName]: value}}, () => loadIssues());
        this.refs.overlay_est_time.hide();
        this.refs.overlay_done_ratio.hide();
        this.refs.overlay_priority.hide();
        this.refs.overlay_assign.hide();
        this.refs.overlay_project.hide();
        this.refs.overlay_subject.hide();
        if (this.state.showModalComment) {
            this.setState({showModalComment: false});
        }
    };

    getPopoverAssign = (issueID, assignID) => {
        const {onChangeField} = this;
        const {users} = this.props;
        const {assign} = this.state;
        const usersArray = [];
        for (let user in users) {
            if (users.hasOwnProperty(user)) {
                usersArray.push(users[user]);
            }
        }
        usersArray.sort((u1, u2) => {
            if (u1.lastname === u2.lastname) return 0;
            return u1.lastname > u2.lastname ? 1 : -1;
        });
        return <Popover id="widget-settings-click">
            <div className="widget-settings-option form-group has-info" style={{marginTop: -25, marginBottom: -25}}>
                <label style={{margin: 0}} className="control-label default-text-color">
                    <select value={assign || assignID}
                            onChange={event => this.setState({assign: event.target.value})}
                            type="text" className="form-control default-text-color"
                            placeholder="">
                        <option value="me">me</option>
                        {usersArray.map(user => <option key={user.id}
                                                        value={user.id}>{user.lastname} {user.firstname}</option>)}
                    </select>
                </label>
                <button style={{padding: '3px 7px'}} className="btn btn-flat"
                        onClick={onChangeField('assign', 'assigned_to_id')}>
                    <i className="fa fa-check"></i>
                </button>
            </div>
        </Popover>;
    };

    getPopoverEstimateTime = currentEstimateTime => {
        const {onChangeField} = this;
        const {estimatedTime} = this.state;
        return <Popover id="widget-settings-click">
            <div className="widget-settings-option form-group has-info" style={{marginTop: -25, marginBottom: -25}}>
                <label style={{margin: 0}} className="control-label default-text-color">
                    <input value={estimatedTime || currentEstimateTime || 0}
                           style={{width: 60}}
                           onChange={event => {
                               const {value} = event.target;
                               this.setState({
                                   estimatedTime: value.indexOf('.') === value.length - 1
                                                      ? value : parseFloat(value)
                               })
                           }}
                           className="form-control default-text-color"
                           placeholder=""/>
                </label>
                <button style={{padding: '3px 7px'}} className="btn btn-flat"
                        onClick={onChangeField('estimatedTime', 'estimated_hours')}>
                    <i className="fa fa-check"></i>
                </button>
            </div>
        </Popover>;
    };

    getPopoverDoneRation = currentDoneRatio => {
        const {onChangeField} = this;
        const {doneRatio} = this.state;
        return <Popover id="widget-settings-click">
            <div className="widget-settings-option form-group has-info" style={{marginTop: -25, marginBottom: -25}}>
                <label style={{margin: 0}} className="control-label default-text-color">
                    <select value={doneRatio || currentDoneRatio || 0}
                            onChange={event => this.setState({doneRatio: event.target.value})}
                            className="form-control default-text-color"
                            placeholder="">
                        {[0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100].map(per => <option key={per}
                                                                                         value={per}>{per}</option>)}
                    </select>
                </label>
                <button style={{padding: '3px 7px'}} className="btn btn-flat"
                        onClick={onChangeField('doneRatio', 'done_ratio')}>
                    <i className="fa fa-check"></i>
                </button>
            </div>
        </Popover>;
    };

    getPopoverPriority = priorityID => {
        const {onChangeField} = this;
        const {priority} = this.state;
        const {priorities} = this.props;
        const prioritiesArray = [];
        for (let priority in priorities) {
            if (priorities.hasOwnProperty(priority)) {
                prioritiesArray.push(priorities[priority]);
            }
        }
        prioritiesArray.sort((p1, p2) => {
            if (p1.id === p2.id) return 0;
            return p1.id > p2.id ? 1 : -1;
        });
        return <Popover id="widget-settings-click">
            <div className="widget-settings-option form-group has-info" style={{marginTop: -25, marginBottom: -25}}>
                <label style={{margin: 0}} className="control-label default-text-color">
                    <select value={priority || priorityID}
                            onChange={event => this.setState({priority: event.target.value})}
                            type="text" className="form-control default-text-color"
                            placeholder="">
                        {prioritiesArray.map(priority => <option key={priority.id}
                                                                 value={priority.id}>{priority.name}</option>)}
                    </select>
                </label>
                <button style={{padding: '3px 7px'}} className="btn btn-flat"
                        onClick={onChangeField('priority', 'priority_id')}>
                    <i className="fa fa-check"></i>
                </button>
            </div>
        </Popover>;
    };

    getPopoverProject = (issueID, projectID) => {
        const {onChangeField} = this;
        const {project} = this.state;
        const {projects} = this.props;
        const projectsArray = [];
        for (let project in projects) {
            if (projects.hasOwnProperty(project)) {
                projectsArray.push(projects[project]);
            }
        }
        projectsArray.sort((p1, p2) => {
            if (p1.name === p2.name) return 0;
            return p1.name > p2.name ? 1 : -1;
        });
        return <Popover id="widget-settings-click">
            <div className="widget-settings-option form-group has-info" style={{marginTop: -25, marginBottom: -25}}>
                <label style={{margin: 0}} className="control-label default-text-color">
                    <select value={project || projectID}
                            onChange={event => this.setState({project: event.target.value})}
                            type="text" className="form-control default-text-color"
                            placeholder="">
                        {projectsArray.map(project => <option key={project.id}
                                                              value={project.id}>{project.name}</option>)}
                    </select>
                </label>
                <button style={{padding: '3px 7px'}} className="btn btn-flat"
                        onClick={onChangeField('project', 'project_id')}>
                    <i className="fa fa-check"></i>
                </button>
            </div>
        </Popover>;
    };

    onCloseCommentModal = () => this.setState({comment: '', showModalComment: false});

    onChangeComment = event => {
        this.setState({comment: event.target.value});
        /*setTimeout(() => $('pre code').each(function (i, block) {
         hljs.highlightBlock(block);
         }));*/
    };

    getPopoverComment = () => {
        const {rmToken} = this.props;
        const {onChangeField, onCloseCommentModal, onChangeComment} = this;
        const {comment, showModalComment, showdown, textile} = this.state;
        return <Modal show={showModalComment} backdrop={true} bsSize="lg" onHide={onCloseCommentModal}>
            <Modal.Body>
                <div className="widget-settings-option form-group has-info" style={{marginTop: -25, marginBottom: -25}}>
                    <OverlayTrigger placement="top"
                                    overlay={<Tooltip id="tooltip">Markdown syntax</Tooltip>}
                                    rootClose={true}>
                        <a style={{float: 'right', cursor: 'pointer', marginTop: 15}}
                           onClick={ElectronUtils.onOpenIssue(MARKDOWN_SYNTAX)}
                           href={MARKDOWN_SYNTAX}
                           target="_blank"><i className="fa fa-question"></i></a>
                    </OverlayTrigger>
                    <br/>
                    <label style={{margin: 0, width: '100%'}}
                           className="control-label default-text-color">
                    <textarea value={comment}
                              style={{height: '300px'}}
                              onChange={onChangeComment}
                              type="text" className="form-control default-text-color"
                              placeholder=""/>
                    </label>
                    {textile && showdown && comment ? <div dangerouslySetInnerHTML={{
                        __html: addTokenToImgTag(rmToken, textile(showdown.makeHtml(comment)))
                    }}></div> : null}
                </div>
            </Modal.Body>
            <Modal.Footer>
                <button disabled={!comment} className="btn btn-flat" onClick={onChangeField('comment', 'notes')}>
                    Save
                </button>
                <button className="btn btn-flat" onClick={onCloseCommentModal}>
                    Cancel
                </button>
            </Modal.Footer>
        </Modal>;
    };

    getPopoverSubject = currentSubject => {
        const {onChangeField} = this;
        const {subject} = this.state;
        return <Popover id="widget-settings-click">
            <div className="widget-settings-option form-group has-info" style={{marginTop: -25, marginBottom: -25}}>
                <label style={{margin: 0}}
                       className="control-label default-text-color">
                    <textarea value={subject || currentSubject}
                              style={{height: '100px', width: '400px'}}
                              onChange={event => this.setState({subject: event.target.value})}
                              type="text" className="form-control default-text-color"
                              placeholder=""/>
                </label>
                <button style={{padding: '3px 7px'}} className="btn btn-flat"
                        onClick={onChangeField('subject', 'subject')}>
                    <i className="fa fa-check"></i>
                </button>
            </div>
        </Popover>;
    };

    onChangeDescription = event => this.setState({description: event.target.value});
    onEditDescription = () => this.setState({descriptionEdit: true});
    onSaveDescription = () => {
        this.onChangeField('description', 'description')();
        this.setState({descriptionEdit: false, description: ''});
        setTimeout(() => setTimeout(() => $('pre code').each((i, block) => {
            if (this.state.hljs) hljs.highlightBlock(block);
        })), 1000);
    };

    getEstimatedTimeTooltip = () => <Tooltip id="EstimatedTimeTooltip">Estimated Time</Tooltip>;
    getDoneRatioTooltip = () => <Tooltip id="DoneRatioTooltip">Done Ratio</Tooltip>;

    render() {
        const {
            issue, connectDragSource, rmUrl, decorateCrossLine, decorateInverseColor, activeIssues, activeTimer,
            issueIsFetched, issueLoading, issueReceived, projects, priorities, statuses, trackers, users, rmToken,
            activities,
        } = this.props;
        const {
            showModal, modalContent, spendHours, spendMinutes, description, descriptionEdit, activityID, activityComment,
        } = this.state;
        const {
            getTooltipDate, onChangeTaskActive, onHideModal, getFormatTime, onChangeSaveTimeInfo, getPopoverAssign,
            renderTimerAction, onShowDescriptionModal, onSaveSpendTime, changeIsNew, onHideModalCancelTimer,
            getPopoverEstimateTime, getPopoverPriority, getPopoverProject, getPopoverComment, onChangeDescription,
            onEditDescription, onSaveDescription, getPopoverSubject, getPopoverDoneRation, getEstimatedTimeTooltip,
            getDoneRatioTooltip
        } = this;
        const date = new Date(Date.parse(issue.updated_on));
        const active = activeIssues.some(aIssue => aIssue.id === issue.id);
        const issueLink = rmUrl + '/' + 'issues/' + issue.id;

        return <div>
            <div className="info-modal">
                <ModalDescription spendMinutes={spendMinutes}
                                  spendHours={spendHours}
                                  onChangeSaveTimeInfo={onChangeSaveTimeInfo}
                                  activityID={activityID || -1}
                                  activities={activities}
                                  activityComment={activityComment || ''}
                                  getFormatTime={getFormatTime}
                                  onChangeDescription={onChangeDescription}
                                  onSaveDescription={onSaveDescription}
                                  onEditDescription={onEditDescription}
                                  description={description}
                                  descriptionEdit={descriptionEdit}
                                  issue={issue}
                                  issueIsFetched={issueIsFetched}
                                  issueLoading={issueLoading}
                                  issueReceived={issueReceived}
                                  modalContent={modalContent}
                                  onHideModal={onHideModal}
                                  onHideModalCancelTimer={onHideModalCancelTimer}
                                  onSaveSpendTime={onSaveSpendTime}
                                  priorities={priorities}
                                  projects={projects}
                                  rmToken={rmToken}
                                  rmUrl={rmUrl}
                                  showModal={showModal}
                                  statuses={statuses}
                                  trackers={trackers}
                                  users={users}/>
            </div>
            {connectDragSource(<div>
                <div className={"widget-grid-item" + (decorateInverseColor ? ' widget-grid-inverse' : '')
                + (decorateCrossLine ? ' widget-grid-dark' : '')}>
                    {active ? <ProgressBar active striped bsStyle="info" now={100}/> : null}
                    {issue.done_ratio ? <div className="progress-done">
                        <div className="progress-bar progress-bar-success"
                             style={{width: `${issue.done_ratio}%`}}></div>
                    </div> : null}
                    <a onClick={ElectronUtils.onOpenIssue(issueLink)} href={issueLink}
                       target="_blank"
                       className="issue-id-link">#{issue.id}</a>
                    {!decorateCrossLine && activeTimer ? <span className="issue-timer" onClick={onChangeTaskActive}>
                    {active ? renderTimerAction('stop') : renderTimerAction('play')}
                </span> : null}
                    <OverlayTrigger placement="top"
                                    ref="overlay_done_ratio"
                                    overlay={getPopoverDoneRation(issue.done_ratio)}
                                    rootClose={true}
                                    onExited={() => this.setState({doneRatio: ''})}
                                    trigger="click">
                        <OverlayTrigger placement="bottom" overlay={getDoneRatioTooltip()}>
                            <div className="badge issue-estimated-time">
                                {issue.done_ratio}
                            </div>
                        </OverlayTrigger>
                    </OverlayTrigger>
                    <OverlayTrigger placement="top"
                                    ref="overlay_priority"
                                    overlay={getPopoverPriority(issue.priority.id)}
                                    rootClose={true}
                                    onExited={() => this.setState({priority: ''})}
                                    trigger="click">
                        <span className={"badge redmine-priority redmine-priority-" + issue.priority.id}>
                            {issue.priority.name}
                        </span>
                    </OverlayTrigger>
                    <OverlayTrigger placement="top"
                                    ref="overlay_est_time"
                                    overlay={getPopoverEstimateTime(issue.estimated_hours)}
                                    rootClose={true}
                                    onExited={() => this.setState({estimatedTime: ''})}
                                    trigger="click">
                        <OverlayTrigger placement="bottom" overlay={getEstimatedTimeTooltip()}>
                            <div className="badge issue-estimated-time">
                                {issue.estimated_hours || <span style={{color: 'red'}}>0</span>}
                            </div>
                        </OverlayTrigger>
                    </OverlayTrigger>
                    <br/>
                    {getPopoverComment()}
                    <i className="redmine-add-comment fa fa-comments-o"
                       onClick={() => this.setState({showModalComment: true})}></i>
                    <OverlayTrigger placement="top"
                                    ref="overlay_project"
                                    overlay={getPopoverProject(issue.id, issue.project.id)}
                                    rootClose={true}
                                    onExited={() => this.setState({project: ''})}
                                    trigger="click">
                        <div className="redmine-project">{issue.project.name}</div>
                    </OverlayTrigger>
                    {issue.isNew ? <i onClick={changeIsNew(issue.status.id, issue.id, issue.updated_on)}
                                      className="fa fa-bell issue-notify"></i> : null}
                    {issue.assigned_to ? <OverlayTrigger placement="top"
                                                         ref="overlay_assign"
                                                         overlay={getPopoverAssign(issue.id, issue.assigned_to.id)}
                                                         rootClose={true}
                                                         onExited={() => this.setState({assign: ''})}
                                                         trigger="click">
                        <div className="issue-assign">@{issue.assigned_to.name}</div>
                    </OverlayTrigger> : null}
                    <div style={{clear: 'both'}}></div>
                    <div onClick={onShowDescriptionModal} className="redmine-issue-subject">
                        {issue.subject} <OverlayTrigger placement="top"
                                                        ref="overlay_subject"
                                                        overlay={getPopoverSubject(issue.subject)}
                                                        rootClose={true}
                                                        onExited={() => this.setState({subject: ''})}
                                                        onClick={event => event.stopPropagation()}
                                                        trigger="click">
                        <span className="issue-edit-subject">
                            <i className="fa fa-pencil"></i>
                        </span>
                    </OverlayTrigger>
                    </div>
                    <div className="issue-author"> @{issue.author.name}</div>
                    <OverlayTrigger placement="top"
                                    overlay={getTooltipDate(date)}
                                    rootClose={true}
                                    trigger="click">
                        <div className="issue-update-date"> {date.duration()}</div>
                    </OverlayTrigger>
                </div>
            </div>)}
        </div>;
    }
}

const beginDrag = {
    beginDrag(props, monitor, component) {
        const {issue} = props;
        //issue.status.id = ;
        return issue;
    },
    endDrag(props, monitor, component) {
        const {issue} = props;
        //issue.status.id = ;
        //console.log('endDrag', props, monitor, component);
        return issue;
    }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging:        monitor.isDragging()
    }
}

function mapStateToProps(state, props) {
    const {issue} = props;
    const issueReceived = state.issue[issue.id] ? state.issue[issue.id] : {
        elem:      {},
        isFetched: false,
        loading:   false,
    };
    return {
        issueReceived:       issueReceived.elem,
        issueIsFetched:      issueReceived.isFetched,
        issueLoading:        issueReceived.loading,
        activities:          state.activities.elems,
        activityIsFetched:   state.activities.isFetched,
        activityLoading:     state.activities.loading,
        users:               state.users.elems,
        usersIsFetched:      state.users.isFetched,
        usersLoading:        state.users.loading,
        statuses:            state.statuses.elems,
        statusesIsFetched:   state.statuses.isFetched,
        statusesLoading:     state.statuses.loading,
        priorities:          state.priorities.elems,
        prioritiesIsFetched: state.priorities.isFetched,
        prioritiesLoading:   state.priorities.loading,
        trackers:            state.trackers.elems,
        trackersIsFetched:   state.trackers.isFetched,
        trackersLoading:     state.trackers.loading,
        projects:            state.projects.elems,
        projectsIsFetched:   state.projects.isFetched,
        projectsLoading:     state.projects.loading,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        issuesActions:       bindActionCreators(IssuesActions, dispatch),
        dictionariesActions: bindActionCreators(DictionariesActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DragSource("VIEW", beginDrag, collect)(Issue));