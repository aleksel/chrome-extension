"use strict";

import * as React from "react";
import Toggle from "../../component/toggle/Toggle";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as DictionariesActions from "../../actions/dictionary";

class RedmineSettings extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            SketchPicker: null,
        };
        require.ensure([], function (require) {
            this.setState({SketchPicker: require('react-color').SketchPicker});
        }.bind(this), 'react-color');
    }

    state = {
        error: '',
    };

    static propTypes = {
        header:   React.PropTypes.string,
        onChange: React.PropTypes.func.isRequired,
        settings: React.PropTypes.shape({}).isRequired,
    };

    onChange = field => event => {
        const {settings} = this.props;
        if (field === 'url') settings.ss = {};
        if (field === 'ut' || field === 'n' || field === 'na') {
            settings[field] = parseInt(event.target.value);
        } else if (field === 'bc' || field === 'hbc') {
            settings[field] = event.rgb;
        } else {
            settings[field] = event.target.value;
        }
        this.props.onChange(settings);
    };

    onCheckConnection = event => {
        const {
            loadStatuses, settings, usersLoading, statusesLoading, prioritiesLoading, trackersLoading, projectsLoading,
            activityLoading, loadActivities, loadUsers, loadPriorities, loadTrackers, loadProjects, user, userLoading,
            getUser
        } = this.props;
        loadStatuses(settings.url, settings.token, data => {
            data.issue_statuses.forEach(status => settings.ss[status.id] = {
                name: status.name,
                show: false
            });
            this.props.onChange(settings);
            if (!activityLoading) {
                loadActivities(settings.url, settings.token);
            }
            if (!usersLoading) {
                loadUsers(settings.url, settings.token);
            }
            if (!userLoading) {
                getUser(settings.url, settings.token);
            }
            if (!prioritiesLoading) {
                loadPriorities(settings.url, settings.token);
            }
            if (!trackersLoading) {
                loadTrackers(settings.url, settings.token);
            }
            if (!projectsLoading) {
                loadProjects(settings.url, settings.token);
            }

            this.setState({error: ''});
        }, error => {
            settings.ss = {};
            this.setState({error});
            this.props.onChange(settings);
        });
    };

    onChangeStatus = statusID => event => {
        const {settings} = this.props;
        if (typeof settings.ss[statusID] !== 'object') {
            settings.ss[statusID] = {};
        }
        settings.ss[statusID].show = !settings.ss[statusID].show;
        this.props.onChange(settings);
    };

    renderStatuses = (statuses) => {
        const {onChangeStatus} = this;
        const result = [];
        for (let status in statuses) {
            if (statuses.hasOwnProperty(status)) {
                result.push(<Toggle label={statuses[status].name}
                                    key={statuses[status].name}
                                    className=""
                                    labelClassName="options-toggle-label"
                                    toggleClassName="options-toggle"
                                    checked={statuses[status].show || false}
                                    onChange={onChangeStatus(status)}/>);
            }
        }
        return result;
    };

    render() {
        const {settings} = this.props;
        const {error, SketchPicker} = this.state;
        const {onChange, onCheckConnection, renderStatuses} = this;
        let checkClass = '';
        if (error) {
            checkClass = ' has-error';
        } else if (settings.ss && Object.keys(settings.ss).length) {
            checkClass = ' has-success';
        }

        return <div>
            <div className="row">
                <div className={"form-group has-info col-lg-12 col-md-12 col-sm-12 col-xs-12" + checkClass}>
                    <label style={{marginLeft: '15px'}} className="control-label default-text-color">
                        <input value={settings && settings.url || ''}
                               onChange={onChange('url')}
                               type="text" className="form-control default-text-color" placeholder=""/>
                        <sub className="default-text-color">Redmine URL</sub>
                    </label>
                </div>
            </div>
            <div className="row">
                <div className={"form-group has-info col-lg-12 col-md-12 col-sm-12 col-xs-12" + checkClass}
                     style={{marginTop: '-10px'}}>
                    <label style={{marginLeft: '15px'}} className="control-label default-text-color">
                        <input value={settings && settings.token || ''}
                               onChange={onChange('token')}
                               type="text" className="form-control default-text-color" placeholder=""/>
                        <sub className="default-text-color">Token</sub>
                    </label>
                </div>
            </div>
            <div className="row">
                <div className="form-group has-info col-lg-2 col-md-2 col-sm-2 col-xs-2" style={{marginTop: '-10px'}}>
                    <label style={{marginLeft: '15px'}} className="control-label default-text-color">
                        <input value={settings && settings.fc || ''}
                               onChange={onChange('fc')}
                               type="text" className="form-control default-text-color" placeholder=""/>
                        <sub className="default-text-color">Fetch count</sub>
                    </label>
                </div>
                <div className="form-group has-info col-lg-3 col-md-3 col-sm-3 col-xs-3" style={{marginTop: '-10px'}}>
                    <label style={{marginLeft: '15px'}} className="control-label default-text-color">
                        <input value={settings && (settings.ut || settings.ut === 0) ? settings.ut + '' : ''}
                               onChange={onChange('ut')}
                               type="number" className="form-control default-text-color" placeholder=""/>
                        <sub className="default-text-color">Update Issue every X min, 0 - turn off</sub>
                    </label>
                </div>
                <div className="form-group has-info col-lg-3 col-md-3 col-sm-3 col-xs-3" style={{marginTop: '-10px'}}>
                    <label style={{marginLeft: '15px'}} className="control-label default-text-color">
                        <input value={settings && (settings.n || settings.n === 0) ? settings.n + '' : ''}
                               onChange={onChange('n')}
                               type="number" className="form-control default-text-color" placeholder=""/>
                        <sub className="default-text-color">Show notify time X min, 0 - turn off</sub>
                    </label>
                </div>
                <div className="form-group has-info col-lg-3 col-md-3 col-sm-3 col-xs-3" style={{marginTop: '-10px'}}>
                    <label style={{marginLeft: '15px'}} className="control-label default-text-color">
                        <input value={settings && (settings.na || settings.na === 0) ? settings.na + '' : ''}
                               onChange={onChange('na')}
                               type="number" className="form-control default-text-color" placeholder=""/>
                        <sub className="default-text-color">Notify active task time X min, 0 - turn off</sub>
                    </label>
                </div>
            </div>
            <button style={{float: 'right', marginRight: '15px', color: '#bbb'}}
                    className="btn btn-default"
                    onClick={onCheckConnection}
                    disabled={!settings || !settings.url || false}>
                Check
            </button>
            <div style={{margin: 15}}>
                {renderStatuses(settings.ss)}
            </div>
            <br/>
            <br/>
            <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <h6 style={{padding: '0 5px', color: '#ccc'}}>Header background color</h6>
                    {SketchPicker ? <SketchPicker color={settings.hbc}
                                                  onChangeComplete={onChange('hbc')}/> : null}
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <h6 style={{padding: '0 5px', color: '#ccc'}}>Background color</h6>
                    {SketchPicker ? <SketchPicker color={settings.bc}
                                                  onChangeComplete={onChange('bc')}/> : null}
                </div>
            </div>
        </div>;
    }
}

function mapStateToProps(state, props) {
    return {
        statuses:            state.statuses.elems,
        statusesIsFetched:   state.statuses.isFetched,
        statusesLoading:     state.statuses.loading,
        activities:          state.activities.elems,
        activityIsFetched:   state.activities.isFetched,
        activityLoading:     state.activities.loading,
        users:               state.users.elems,
        usersIsFetched:      state.users.isFetched,
        usersLoading:        state.users.loading,
        priorities:          state.priorities.elems,
        prioritiesIsFetched: state.priorities.isFetched,
        prioritiesLoading:   state.priorities.loading,
        trackers:            state.trackers.elems,
        trackersIsFetched:   state.trackers.isFetched,
        trackersLoading:     state.trackers.loading,
        projects:            state.projects.elems,
        projectsIsFetched:   state.projects.isFetched,
        projectsLoading:     state.projects.loading,
        user:                state.user.elem,
        userIsFetched:       state.user.isFetched,
        userLoading:         state.user.loading,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(DictionariesActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(RedmineSettings);