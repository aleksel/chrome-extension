"use strict";

import * as React from "react";

export default class SaveTimeForm extends React.Component {

    static defaultProps = {
        name:         'Name',
        spendHours:   0,
        spendMinutes: 0,
    };

    static propTypes = {
        onChangeSaveTimeInfo: React.PropTypes.func.isRequired,
        activities:           React.PropTypes.array.isRequired,
        activityID:           React.PropTypes.number.isRequired,
        activityComment:      React.PropTypes.string.isRequired,
        spendHours:           React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.number]).isRequired,
        spendMinutes:         React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.number]).isRequired,
    };

    render() {
        const {activities, onChangeSaveTimeInfo, activityID, activityComment, spendHours, spendMinutes} = this.props;
        return <div>
            <div className="row">
                <div className="form-group has-info col-lg-1 col-md-1 col-sm-1 col-xs-1" style={{marginTop: 20}}>
                    <label style={{width: '100%'}} className="control-label default-text-color">
                        <input value={spendHours}
                               onChange={onChangeSaveTimeInfo('spendHours')}
                               type="number" className="form-control default-text-color" placeholder=""/>
                        <sub className="default-text-color">Hours</sub>
                    </label>
                </div>
                <div className="form-group has-info col-lg-1 col-md-1 col-sm-1 col-xs-1" style={{marginTop: 20}}>
                    <label style={{width: '100%'}} className="control-label default-text-color">
                        <input value={spendMinutes}
                               onChange={onChangeSaveTimeInfo('spendMinutes')}
                               type="number" className="form-control default-text-color" placeholder=""/>
                        <sub className="default-text-color">Minutes</sub>
                    </label>
                </div>
                <div className="form-group has-info col-lg-4 col-md-4 col-sm-4 col-xs-4" style={{marginTop: 20}}>
                    <label style={{width: '100%'}} className="control-label default-text-color">
                        <select value={activityID}
                                onChange={onChangeSaveTimeInfo('activityID')}
                                type="text" className="form-control default-text-color" placeholder="">
                            {activities.map(activity => <option key={activity.id}
                                                                value={activity.id}>{activity.name}</option>)}
                        </select>
                        <sub className="default-text-color">Activity</sub>
                    </label>
                </div>
                <div className="form-group has-info col-lg-6 col-md-6 col-sm-6 col-xs-6" style={{marginTop: 0}}>
                    <label style={{width: '100%'}} className="control-label default-text-color">
                        <textarea value={activityComment}
                                  onChange={onChangeSaveTimeInfo('activityComment')}
                                  className="form-control default-text-color" placeholder=""/>
                        <sub className="default-text-color">Comment</sub>
                    </label>
                </div>
            </div>
        </div>;
    }
}