"use strict";

import * as React from "react";
import Toggle from "../../component/toggle/Toggle";
import "./timer.css";

export default class TimerSettings extends React.Component {

    constructor(props) {
        super(props);
        const date = new Date(Date.now() + 86400000);
        this.state = {
            date:    date.formatDate('yyyy-MM-dd'),
            header:  '',
            between: date.getPastTimeTo(new Date(), true),
            error:   '',
        };
    }


    static propTypes = {
        header:   React.PropTypes.string,
        onChange: React.PropTypes.func.isRequired,
        settings: React.PropTypes.shape({}).isRequired,
    };

    onChange = key => event => {
        let {between} = this.state;
        if (key === 'date') {
            between = new Date(Date.parse(event.target.value)).getPastTimeTo(new Date(), true);
        }
        this.setState({[key]: event.target.value, between, error: false});
    };

    onAddTimer = event => {
        const {header, date, between} = this.state;
        const {settings} = this.props;
        if (typeof settings.t !== 'object') {
            settings.t = {};
        }
        if (!between || between.hours === 'NaN' || Date.parse(date) < Date.now()) {
            this.setState({error: '--The Date must be more then now'});
        } else if (settings.t[date] !== undefined) {
            this.setState({error: '--Timer already exists'});
        } else {
            settings.t[date] = header;
            this.props.onChange(settings);
            this.setState({error: false});
        }
    };

    onRemoveTimer = date => event => {
        const {settings} = this.props;
        if (typeof settings.t !== 'object') {
            settings.t = {};
        }
        delete settings.t[date];
        delete settings.ls[date];
        this.props.onChange(settings);
    };

    getTimer = () => {
        const {settings} = this.props;
        const timers = [];
        for (let item in settings.t) {
            if (settings.t.hasOwnProperty(item)) {
                timers.push({header: settings.t[item], date: item});
            }
        }
        return timers;
    };

    onChangeIC = () => {
        const {settings} = this.props;
        settings.ic = settings.ic === 0 ? 1 : 0;
        this.props.onChange(settings);
    };

    render() {
        const {header, date, error, between} = this.state;
        const {settings} = this.props;
        const {onChange, onAddTimer, getTimer, onRemoveTimer, onChangeIC} = this;

        return <div>
            <br/>
            <div className="row" style={{padding: '0 15px'}}>
                <Toggle label="Inverse color"
                        className=""
                        labelClassName="options-toggle-label"
                        toggleClassName="options-toggle"
                        checked={settings.ic !== 0}
                        onChange={onChangeIC}/>
            </div>
            <br/>
            <br/>
            <div className="row">
                <div className="default-text-color has-info col-lg-12 col-md-12 col-sm-12 col-xs-12"
                     style={{margin: 0}}>

                        <span>Until the end: {between && between.hours !== 'NaN' ?
                            <span className={error ? ' has-error' : ''}>
                            {between.days ? (between.days + 'd, ') : ''} {between.hours}:{between.minutes}
                            </span>
                            : <span className="has-error">
                                -- date parse error
                            </span>}
                            </span>
                </div>
            </div>
            <div className="row">
                <div className="form-group has-info col-lg-6 col-md-6 col-sm-6 col-xs-6" style={{margin: 0}}>
                    <label style={{marginLeft: '15px'}} className="control-label default-text-color">
                        <input value={header}
                               onChange={onChange('header')}
                               type="text" className="form-control default-text-color" placeholder=""/>
                        <sub className="default-text-color">Header</sub>
                    </label>
                </div>
                <div
                    className={"form-group has-info col-lg-6 col-md-6 col-sm-6 col-xs-6" + (error ? ' has-error' : '')}
                    style={{margin: 0}}>
                    <label style={{marginLeft: '15px'}} className="control-label default-text-color">
                        <input value={date}
                               onChange={onChange('date')}
                               type="text" className="form-control default-text-color" placeholder=""/>
                        <sub className="default-text-color">Date</sub>
                        {error
                            ? <sub style={{marginLeft: 5}}
                                   className="default-text-color">{error}</sub>
                            : null}
                    </label>
                </div>
            </div>
            <button style={{float: 'right', marginRight: '15px', color: '#bbb'}}
                    className="btn btn-default"
                    disabled={!!error}
                    onClick={onAddTimer}>
                Add
            </button>
            <br/>
            <br/>
            <div className="default-text-color timer-list">
                <h5>Timers:</h5>
                <table className="table">
                    <thead>
                    <tr>
                        <th>Header</th>
                        <th>Date</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {getTimer().map((timer, i) => {
                        return <tr key={timer.date}>
                            <td className="col-lg-5 col-md-5 col-sm-5 col-xs-5 timer-list-date">
                                {timer.header || '-----'}
                            </td>
                            <td className="col-lg-5 col-md-5 col-sm-5 col-xs-5 timer-list-date">
                                {timer.date}
                            </td>
                            <td>
                                <button
                                    className="btn btn-default col-lg-2 col-md-2 col-sm-2 col-xs-2 timer-list-button"
                                    onClick={onRemoveTimer(timer.date)}>
                                    <i className="fa fa-trash-o"></i>
                                </button>
                            </td>
                        </tr>;
                    })}
                    </tbody>
                </table>
            </div>
        </div>;
    }
}