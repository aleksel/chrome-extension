"use strict";

import ApiUtils from "../utils/ApiUtils";
import Notify from "../utils/Notify";
import Storage, {NOTIFIED_ISSUES} from "../utils/Storage";
import {REDMINE_ICON} from "../widget/redmine/Redmine";

export const ISSUES_LOADING = 'ISSUES_LOADING';
export const ISSUES_LOADED = 'ISSUES_LOADED';
export const ISSUES_LOAD_FAILURE = 'ISSUES_LOAD_FAILURE';

export const ISSUE_LOADING = 'ISSUE_LOADING';
export const ISSUE_LOADED = 'ISSUE_LOADED';
export const ISSUE_LOAD_FAILURE = 'ISSUE_LOAD_FAILURE';

export const ISSUE_CREATE_LOADING = 'ISSUE_CREATE_LOADING';
export const ISSUE_CREATE_LOADED = 'ISSUE_CREATE_LOADED';
export const ISSUE_CREATE_LOAD_FAILURE = 'ISSUE_CREATE_LOAD_FAILURE';

export const SAVE_SPEND_TIME_LOADING = 'SAVE_SPEND_TIME_LOADING';
export const SAVE_SPEND_TIME_LOADED = 'SAVE_SPEND_TIME_LOADED';
export const SAVE_SPEND_TIME_LOAD_FAILURE = 'SAVE_SPEND_TIME_LOAD_FAILURE';

export const SAVE_ISSUE_LOADING = 'SAVE_ISSUE_LOADING';
export const SAVE_ISSUE_LOADED = 'SAVE_ISSUE_LOADED';
export const SAVE_ISSUE_LOAD_FAILURE = 'SAVE_ISSUE_LOAD_FAILURE';

export const ISSUES_IS_NEW_CHANGE_STATUS = 'ISSUES_IS_NEW_CHANGE_STATUS';

export const ISSUES_CHANGE_STATUS_LOADING = 'ISSUES_CHANGE_STATUS_LOADING';
export const ISSUES_CHANGE_STATUS_LOADED = 'ISSUES_CHANGE_STATUS_LOADED';
export const ISSUES_CHANGE_STATUS_LOAD_FAILURE = 'ISSUES_CHANGE_STATUS_LOAD_FAILURE';

export function loadIssues(rmUrl: string, token: string, assign: string = '', project: string = '', statusID: string,
                           sort: string = 'updated_on:desc', count: number = 20, notifyDuration = 10, successCallback: Function) {
    if (!sort) sort = 'updated_on:desc';
    const assigned = assign && assign !== 'a' ? `&assigned_to_id=${assign}` : '';
    const projectID = project && project !== 'a' ? `&project_id=${project}` : '';
    return {
        actions:              [`${ISSUES_LOADING}_${statusID}`, `${ISSUES_LOADED}_${statusID}`, `${ISSUES_LOAD_FAILURE}_${statusID}`],
        type:                 'PROMISE',
        additionalDataSecond: notifyDuration,
        additionalData:       elem => {
            Notify.notifyHtml5(`Issue update #${elem.id} at ${new Date(Date.parse(elem.updated_on)).duration()}`,
                elem.subject, REDMINE_ICON, () => {
                    if (ELECTRON) {
                        const {ipcRenderer} = require('electron');
                        ipcRenderer.send('focus-main-window');
                    }
                    $(document).trigger('CLICK_NOTIFY_ISSUE', {data: elem.id});
                    const notifiedIssues = Storage.get(NOTIFIED_ISSUES, true, false);
                    notifiedIssues[elem.id] = Date.parse(elem.updated_on);
                    Storage.save({[NOTIFIED_ISSUES]: notifiedIssues}, true);
                    successCallback(elem.id, elem.updated_on);
                });
        },
        promise:              () => {
            const url = `${rmUrl}/issues.json`;
            //fetch('http://rm.pixel.alx/issues.json?key=7329ab2afbea00b22957b0a8f3a229c764e3cf5c&status_id=7&assigned_to_id=me&limit=100')
            return ApiUtils.get(`${url}?key=${token}&status_id=${statusID}${assigned}${projectID}&sort=${sort}&limit=${count}`);
        }
    };
}

export function resetIsNew(issueStatusID, issueID, updatedOn) {
    let notifiedIssues = Storage.get(NOTIFIED_ISSUES, true, false);
    if (!notifiedIssues) {
        notifiedIssues = {};
    }
    notifiedIssues[issueID] = Date.parse(updatedOn);
    Storage.save({[NOTIFIED_ISSUES]: notifiedIssues}, true);
    return {type: ISSUES_IS_NEW_CHANGE_STATUS, payload: {issueID, issueStatusID}};
}

export function loadIssue(rmUrl: string, token: string, issueID: number) {
    return {
        actions: [`${ISSUE_LOADING}_${issueID}`, `${ISSUE_LOADED}_${issueID}`, `${ISSUE_LOAD_FAILURE}_${issueID}`],
        type:    'PROMISE',
        promise: () => ApiUtils.get(`${rmUrl}/issues/${issueID}.json?key=${token}&include=journals,attachments`)
    };
}

export function createIssue(rmUrl: string, token: string, issue: Object, successCallback: Function) {
    return {
        successCallback,
        actions: [ISSUE_CREATE_LOADING, ISSUE_CREATE_LOADED, ISSUE_CREATE_LOAD_FAILURE],
        type:    'PROMISE',
        promise: () => ApiUtils.post(`${rmUrl}/issues.json?key=${token}`, {issue})
    };
}

export function changeStatus(rmUrl: string, token: string, issueID: string, statusTo: number, successCallback: Function) {
    return {
        successCallback,
        actions: [
            ISSUES_CHANGE_STATUS_LOADING,
            ISSUES_CHANGE_STATUS_LOADED,
            ISSUES_CHANGE_STATUS_LOAD_FAILURE
        ],
        type:    'PROMISE',
        promise: () =>
                     ApiUtils.put(`${rmUrl}/issues/${issueID}.json?key=${token}`, {issue: {status_id: parseInt(statusTo)}})
    };
}

export function saveSpendTime(rmUrl: string, token: string,
                              issueID: number,
                              hours: number,
                              activityID: string,
                              activityComment: string,
                              successCallback: Function) {
    return {
        successCallback,
        actions: [
            SAVE_SPEND_TIME_LOADING,
            SAVE_SPEND_TIME_LOADED,
            SAVE_SPEND_TIME_LOAD_FAILURE,
        ],
        type:    'PROMISE',
        promise: () => ApiUtils.post(`${rmUrl}/time_entries.json?key=${token}`,
            {
                time_entry: {issue_id: issueID, comments: activityComment, activity_id: parseInt(activityID), hours}
            })
    };
}

export function saveIssue(rmUrl: string, token: string, issueID: number, data: object, successCallback: Function) {
    return {
        successCallback,
        actions: [
            SAVE_ISSUE_LOADING,
            SAVE_ISSUE_LOADED,
            SAVE_ISSUE_LOAD_FAILURE,
        ],
        type:    'PROMISE',
        promise: () => ApiUtils.put(`${rmUrl}/issues/${issueID}.json?key=${token}`, data)
    };
}
