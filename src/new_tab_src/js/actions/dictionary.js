"use strict";

import ApiUtils from "../utils/ApiUtils";

export const ACTIVITIES_LOADING = 'ACTIVITIES_LOADING';
export const ACTIVITIES_LOADED = 'ACTIVITIES_LOADED';
export const ACTIVITIES_LOAD_FAILURE = 'ACTIVITIES_LOAD_FAILURE';

export const USERS_LOADING = 'USERS_LOADING';
export const USERS_LOADED = 'USERS_LOADED';
export const USERS_LOAD_FAILURE = 'USERS_LOAD_FAILURE';

export const STATUSES_LOADING = 'STATUSES_LOADING';
export const STATUSES_LOADED = 'STATUSES_LOADED';
export const STATUSES_LOAD_FAILURE = 'STATUSES_LOAD_FAILURE';

export const PRIORITIES_LOADING = 'PRIORITIES_LOADING';
export const PRIORITIES_LOADED = 'PRIORITIES_LOADED';
export const PRIORITIES_LOAD_FAILURE = 'PRIORITIES_LOAD_FAILURE';

export const TRACKERS_LOADING = 'TRACKERS_LOADING';
export const TRACKERS_LOADED = 'TRACKERS_LOADED';
export const TRACKERS_LOAD_FAILURE = 'TRACKERS_LOAD_FAILURE';

export const PROJECTS_LOADING = 'PROJECTS_LOADING';
export const PROJECTS_LOADED = 'PROJECTS_LOADED';
export const PROJECTS_LOAD_FAILURE = 'PROJECTS_LOAD_FAILURE';

export const ISSUES_IS_NEW_CHANGE_STATUS = 'ISSUES_IS_NEW_CHANGE_STATUS';

export const USER_LOADING = 'USER_LOADING';
export const USER_LOADED = 'USER_LOADED';
export const USER_LOAD_FAILURE = 'USER_LOAD_FAILURE';

export function getUser(rmUrl: string, token: string, successCallback: Function) {
    return {
        actions: [USER_LOADING, USER_LOADED, USER_LOAD_FAILURE],
        type:    'PROMISE',
        promise: () => ApiUtils.get(`${rmUrl}/users/current.json?key=${token}`)
    };
}

export function loadActivities(rmUrl: string, token: string) {
    return {
        actions: [
            ACTIVITIES_LOADING,
            ACTIVITIES_LOADED,
            ACTIVITIES_LOAD_FAILURE
        ],
        type:    'PROMISE',
        promise: () => ApiUtils.get(`${rmUrl}/enumerations/time_entry_activities.json?key=${token}`)
    };
}

export function loadUsers(rmUrl: string, token: string) {
    return {
        actions: [
            USERS_LOADING,
            USERS_LOADED,
            USERS_LOAD_FAILURE
        ],
        type:    'PROMISE',
        promise: () => ApiUtils.get(`${rmUrl}/users.json?limit=100&key=${token}&sort=last_login_on:desc`)
    };
}

export function loadStatuses(rmUrl: string, token: string, successCallback: Function, errorCallback: Function) {
    return {
        successCallback,
        errorCallback,
        actions: [
            STATUSES_LOADING,
            STATUSES_LOADED,
            STATUSES_LOAD_FAILURE
        ],
        type:    'PROMISE',
        promise: () => ApiUtils.get(`${rmUrl}/issue_statuses.json?limit=100&key=${token}`)
    };
}

export function loadPriorities(rmUrl: string, token: string) {
    return {
        actions: [
            PRIORITIES_LOADING,
            PRIORITIES_LOADED,
            PRIORITIES_LOAD_FAILURE
        ],
        type:    'PROMISE',
        promise: () => ApiUtils.get(`${rmUrl}/enumerations/issue_priorities.json?limit=100&key=${token}`)
    };
}

export function loadTrackers(rmUrl: string, token: string) {
    return {
        actions: [
            TRACKERS_LOADING,
            TRACKERS_LOADED,
            TRACKERS_LOAD_FAILURE
        ],
        type:    'PROMISE',
        promise: () => ApiUtils.get(`${rmUrl}/trackers.json?limit=100&key=${token}`)
    };
}

export function loadProjects(rmUrl: string, token: string) {
    return {
        actions: [
            PROJECTS_LOADING,
            PROJECTS_LOADED,
            PROJECTS_LOAD_FAILURE
        ],
        type:    'PROMISE',
        promise: () => ApiUtils.get(`${rmUrl}/projects.json?limit=100&key=${token}`)
    };
}