"use strict";

import ApiUtils from "../utils/ApiUtils";

export const GEO_LOCATION_LOADING = 'GEO_LOCATION_LOADING';
export const GEO_LOCATION_LOADED = 'GEO_LOCATION_LOADED';
export const GEO_LOCATION_LOAD_FAILURE = 'GEO_LOCATION_LOAD_FAILURE';

export const CLEAR_LOCATION = 'CLEAR_LOCATION';

export const CITY_NAME_LOADING = 'CITY_NAME_LOADING';
export const CITY_NAME_LOADED = 'CITY_NAME_LOADED';
export const CITY_NAME_LOAD_FAILURE = 'CITY_NAME_LOAD_FAILURE';

export const COORDINATES_LOADING = 'COORDINATES_LOADING';
export const COORDINATES_LOADED = 'COORDINATES_LOADED';
export const COORDINATES_LOAD_FAILURE = 'COORDINATES_LOAD_FAILURE';

export function getGeolocation(successCallback: Function) {
    return {
        successCallback,
        actions: [
            GEO_LOCATION_LOADING,
            GEO_LOCATION_LOADED,
            GEO_LOCATION_LOAD_FAILURE
        ],
        type:    'PROMISE',
        promise: () => ApiUtils.get(`https://maps.googleapis.com/maps/api/browserlocation/json?browser=chromium&sensor=true`)
    };
}

export function clearLoc() {
    return {type: CLEAR_LOCATION,};
}

export function getCityNameByCoordinates(lat: number, lng: number, successCallback: Function) {
    return {
        successCallback,
        actions: [
            CITY_NAME_LOADING,
            CITY_NAME_LOADED,
            CITY_NAME_LOAD_FAILURE
        ],
        type:    'PROMISE',
        promise: () => ApiUtils.get(`http://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&sensor=false`)
    };
}

export function getCoordinatesByCityName(cityName: string, successCallback: Function) {
    return {
        successCallback,
        actions: [
            COORDINATES_LOADING,
            COORDINATES_LOADED,
            COORDINATES_LOAD_FAILURE
        ],
        type:    'PROMISE',
        promise: () => ApiUtils.get(`http://maps.googleapis.com/maps/api/geocode/json?address=${cityName}`)
    };
}