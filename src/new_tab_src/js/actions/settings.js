"use strict";

import App from "../App";
import Storage from "../utils/Storage";

export const SETTINGS_LOADING = 'SETTINGS_LOADING';
export const SETTINGS_CHANGED = 'SETTINGS_CHANGED';
export const SETTINGS_SECOND_CHANGED = 'SETTINGS_SECOND_CHANGED';
export const SETTINGS_ALL_CHANGED = 'SETTINGS_ALL_CHANGED';
export const SETTINGS_LOADED = 'SETTINGS_LOADED';
export const SETTINGS_LOAD_FAILURE = 'SETTINGS_LOAD_FAILURE';

export const NAME_CREATED = 'NAME_CREATED';

export function loadSettings() {
    return {
        actions: [SETTINGS_LOADING, SETTINGS_LOADED, SETTINGS_LOAD_FAILURE],
        type:    'PROMISE',
        promise: () => Storage.get(App.APP_SETTINGS_FIELD_NAME)
    };
}

export function saveSettings(settings) {
    Storage.save({[App.APP_SETTINGS_FIELD_NAME]: settings});
    return {type: ''};
}

export function changeSettings(settings, type) {
    return {type: SETTINGS_CHANGED, payload: {settings, type}};
}

export function changeSecondSettings(settings, type, secondType) {
    return {type: SETTINGS_SECOND_CHANGED, payload: {settings, type, secondType}};
}

export function changeAllSettings(settings) {
    return {type: SETTINGS_ALL_CHANGED, payload: {settings}};
}

export function nameCreated() {
    return {type: NAME_CREATED, payload: {}};
}