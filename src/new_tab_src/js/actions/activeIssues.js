"use strict";

import App from "../App";
import Storage from "../utils/Storage";

export const ACTIVE_ISSUES_LOADED = 'ACTIVE_ISSUES_LOADED';
export const ACTIVE_ISSUES_CHANGE = 'ACTIVE_ISSUES_CHANGE';

export function load() {
    return {
        type:    ACTIVE_ISSUES_LOADED,
        payload: Storage.get(App.APP_REDMINE_ACTIVE_ISSUES, true, false)
    };
}

export function save(activeIssues) {
    Storage.save({[App.APP_REDMINE_ACTIVE_ISSUES]: activeIssues}, true);
    return {type: ''};
}

export function changeActive(issueID, start = true) {
    return {type: ACTIVE_ISSUES_CHANGE, payload: {issueID, start}};
}