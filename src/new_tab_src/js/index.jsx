import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./utils/DateExtendsUtils";
import "./App.css";
import "./../css/load_screen_spinner.css";
import "./../css/index.css";
import "./../css/sketch.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-material-design";
import "bootstrap-material-design/dist/css/bootstrap-material-design.min.css";
import "font-awesome/css/font-awesome.css";
import configureStore from "./store/configureStore";
import {Provider} from "react-redux";

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('main-container')
);
