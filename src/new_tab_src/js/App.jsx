import React, {Component} from "react";
import Background, {IMAGE_LIST_URLS} from "./component/Background";
import Password from "./component/password/Password";
import Time from "./component/time/Time";
import Redmine from "./widget/redmine/Redmine";
import Timer, {WIDGET_TIMER_HEIGHT, WIDGET_TIMER_WIDTH} from "./widget/timer/Timer";
import ApiUtils from "./utils/ApiUtils";
import Settings, {MENU_GENERAL, MENU_WIDGETS, MENU_WINDOWS} from "./component/settings/Settings";
import ReactGridLayout from "react-grid-layout";
import WidthProvider from "./component/WidthProvider";
import Toastr from "./component/toastr/Toastr";
import {REDMINE_WIDGET, TIMER_WIDGET} from "./component/settings/menu/Widgets";
import {PIN_WIDGETS} from "./component/settings/menu/General";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as SettingsActions from "./actions/settings";
import * as ActiveIssuesActions from "./actions/activeIssues";
import "react-grid-layout/css/styles.css";
import "react-resizable/css/styles.css";
import "./../css/electron.css";
import "./widget/widget.css";

export const BACKGROUND_JSON = IMAGE_LIST_URLS + 'json/images.json';
export const SORT_ASC_DIRECTION = 'a';
export const SORT_DESC_DIRECTION = 'd';

let BrowserWindowOut = null;
let ipcRendererOut = null;

if (ELECTRON) {
    const {ipcRenderer} = require('electron');
    ipcRendererOut = ipcRenderer;
    const {remote} = require('electron');
    const {BrowserWindow} = remote;
    BrowserWindowOut = BrowserWindow;
}
const ResponsiveReactGridLayout = WidthProvider(ReactGridLayout);
const MAX_ROW = 50;

class App extends Component {

    constructor(props) {
        super(props);
        this.timer = null;
        this.state = {
            images:         [],
            activeRmIssues: [],
            zIndexPass:     App.DEFAULT_Z_INDEX,
            zIndexTime:     App.DEFAULT_Z_INDEX,
            enterName:      false,
            hours:          new Date().getHours(),
            minutes:        new Date().getMinutes(),
        };
    }

    static dayTimes = [
        {from: 0, to: 6, name: 'night'},
        {from: 6, to: 12, name: 'morning'},
        {from: 12, to: 18, name: 'afternoon'},
        {from: 18, to: 24, name: 'evening'}
    ];

    static NAME_FIELD_NAME = 'CURRENT_NAME';
    static APP_SETTINGS_FIELD_NAME = 'APP_SETTINGS_FIELD_NAME';
    static APP_REDMINE_ACTIVE_ISSUES = 'APP_REDMINE_ACTIVE_ISSUES';

    static DEFAULT_Z_INDEX = 12;
    static DEFAULT_Z_INDEX_WIDGET = 10;
    static DRAGGING_WINDOW_Z_INDEX = 100;


    static defaultProps = {
        className:       "layout_main_widgets",
        items:           1,
        cols:            100,
        rowHeight:       10,
        maxRows:         MAX_ROW,
        autoSize:        false,
        // This turns off compaction so you can place items wherever.
        verticalCompact: false
    };

    onChangeName = event => {
        this.setState({settings: this.changeName(event.target.value)});
    };

    changeName = name => {
        const {settings, settingsActions} = this.props;
        settings[MENU_GENERAL].name = name;
        settingsActions.changeSettings(settings[MENU_GENERAL], MENU_GENERAL);
    };

    handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            e.preventDefault();
            const name = e.target.value;
            this.changeName(name);
            this.props.settingsActions.nameCreated();
        }
    };

    componentDidMount() {
        // Make not active window on click outside window
        $(window).click(() => {
            this.setState({zIndexPass: App.DEFAULT_Z_INDEX, zIndexTime: App.DEFAULT_Z_INDEX});
        });
    }

    componentWillMount() {
        const {settingsActions, activeIssuesActions} = this.props;
        ApiUtils.get(BACKGROUND_JSON, {'pragma': 'no-cache', 'cache-control': 'no-cache'}).then(data => {
            this.setState({images: data}, this.hideSplash)
        });
        settingsActions.loadSettings();
        $.material.init();
        this.timer = setInterval(() => {
                const date = new Date();
                if (this.state.minutes !== date.getMinutes()) {
                    this.setState({
                        hours:   date.getHours(),
                        minutes: date.getMinutes()
                    });
                }
            }, 1000
        );
        window.addEventListener("beforeunload", e => {
            activeIssuesActions.save(this.props.activeIssues);
            settingsActions.saveSettings(this.props.settings);
            clearInterval(this.timer);
        }, false);
    }

    getTime = () => {
        const {hours, minutes} = this.state;
        const currentHours = (hours + '').length === 1 ? ('0' + hours) : hours;
        const currentMinutes = (minutes + '').length === 1 ? ('0' + minutes) : minutes;
        let dayTime = '';
        App.dayTimes.forEach(item => {
            if (item.from <= hours && item.to > hours) {
                dayTime = item.name;
            }
        });
        return {currentHours, currentMinutes, dayTime};
    };

    changeZIndexPass = (event) => {
        if (event) {
            event.stopPropagation();
        }
        this.setState({zIndexPass: App.DRAGGING_WINDOW_Z_INDEX, zIndexTime: App.DEFAULT_Z_INDEX})
    };

    changeZIndexTime = (event) => {
        if (event) {
            event.stopPropagation();
        }
        this.setState({zIndexTime: App.DRAGGING_WINDOW_Z_INDEX, zIndexPass: App.DEFAULT_Z_INDEX})
    };

    hideSplash = () => {
        const element = document.getElementById('loading');
        const className = element.className;
        element.className += " animated fadeOut";
        setTimeout(() => {
            element.className = className;
            element.style = 'display:none;';
        }, 600);
    };

    onResizeStopWidget = (array, widgetBefore, widgetAfter) => {
        let widget;
        if (widgetAfter.i.indexOf(REDMINE_WIDGET) === 0) {
            widget = REDMINE_WIDGET;
        } else if (widgetAfter.i.indexOf(TIMER_WIDGET) === 0) {
            widget = TIMER_WIDGET;
        }
        const {settings, settingsActions} = this.props;
        const key = widgetAfter.i.replace(widget, '');
        settings[MENU_WIDGETS][widget].ls[key].h = widgetAfter.h;
        if (widget === TIMER_WIDGET) {
            const node = Timer.getWidgetElem(key).parent()[0];
            settings[MENU_WIDGETS][widget].ls[key].sw = node.offsetWidth / WIDGET_TIMER_WIDTH;
            settings[MENU_WIDGETS][widget].ls[key].sh = node.offsetHeight / WIDGET_TIMER_HEIGHT;
        }
        settings[MENU_WIDGETS][widget].ls[key].w = widgetAfter.w;
        settingsActions.changeAllSettings(settings);
        $(document).trigger('WIDGET:RESIZE', {data: {array, widgetBefore, widgetAfter}});
    };

    onDragWidget = (array, widgetBefore, widgetAfter) => {
        let widget;
        if (widgetAfter.i.indexOf(REDMINE_WIDGET) === 0) {
            widget = REDMINE_WIDGET;
        } else if (widgetAfter.i.indexOf(TIMER_WIDGET) === 0) {
            widget = TIMER_WIDGET;
        }
        const {settings, settingsActions} = this.props;
        const key = widgetAfter.i.replace(widget, '');
        settings[MENU_WIDGETS][widget].ls[key].x = widgetAfter.x;
        settings[MENU_WIDGETS][widget].ls[key].y = widgetAfter.y;
        settingsActions.changeAllSettings(settings);
    };

    createRedmineLayouts = layouts => {
        const {cols, settings} = this.props;
        const redmineWidget = settings[MENU_WIDGETS][REDMINE_WIDGET];
        if (settings && redmineWidget && redmineWidget.ss && redmineWidget.show) {
            if (Object.keys(redmineWidget.ss).length) {
                for (let status in redmineWidget.ss) {
                    if (redmineWidget.ss.hasOwnProperty(status)
                        && redmineWidget.ss[status].show) {
                        layouts.push({
                            ...redmineWidget.ls[status],
                            type:   REDMINE_WIDGET,
                            widget: <Redmine header={redmineWidget.ss[status].name}
                                             xPos={redmineWidget.ls[status].x}
                                             xMax={cols}
                                             notifyTime={redmineWidget.n}
                                             notifyActiveTime={redmineWidget.na}
                                             bgColor={redmineWidget.bc}
                                             bgHeaderColor={redmineWidget.hbc}
                                             status={status}/>,
                        });
                    }
                }
            }
        }
        return layouts;
    };

    createTimerLayouts = layouts => {
        const {cols, settings} = this.props;
        const timerWidget = settings[MENU_WIDGETS][TIMER_WIDGET];
        if (settings && timerWidget && timerWidget.show) {
            for (let date in timerWidget.t) {
                if (timerWidget.t.hasOwnProperty(date)) {
                    layouts.push({
                        ...timerWidget.ls[date],
                        type:   TIMER_WIDGET,
                        //isResizable: false,
                        widget: <Timer date={date}
                                       inverseColor={settings[MENU_WIDGETS][TIMER_WIDGET].ic}
                                       wScale={settings[MENU_WIDGETS][TIMER_WIDGET].ls[date].sw}
                                       hScale={settings[MENU_WIDGETS][TIMER_WIDGET].ls[date].sh}
                                       header={timerWidget.t[date]}/>,
                    });
                }
            }
        }
        return layouts;
    };

    onCloseWindow = () => {
        if (BrowserWindowOut) {
            BrowserWindowOut.getFocusedWindow().hide();
            ipcRendererOut.send('quit');
        }
    };

    onMaximizeWindow = () => {
        if (BrowserWindowOut) {
            if (BrowserWindowOut.getFocusedWindow().isMaximized()) {
                BrowserWindowOut.getFocusedWindow().unmaximize();
            } else {
                BrowserWindowOut.getFocusedWindow().maximize();
            }
        }
    };

    onMinimizeWindow = () => {
        if (BrowserWindowOut) BrowserWindowOut.getFocusedWindow().hide();
    };

    onChangeLayout = layouts => layouts.forEach(layout => {
        // If widget breakdown through bottom
        const {settings, settingsActions} = this.props;
        if (layout.y > MAX_ROW - 1) {
            layout.y = 1;
            settingsActions.changeAllSettings(settings);
        } else if (layout.y < 1) {
            layout.y = 1;
            settingsActions.changeAllSettings(settings);
        }
    });

    render() {
        const {currentHours, currentMinutes, dayTime} = this.getTime();
        const {zIndexPass, zIndexTime, images} = this.state;
        const {cols, settings, name} = this.props;
        const {
            changeZIndexPass, changeZIndexTime, onResizeStopWidget, onDragWidget, createRedmineLayouts, onCloseWindow,
            onMaximizeWindow, onMinimizeWindow, onChangeLayout, createTimerLayouts
        } = this;
        const layouts = createTimerLayouts(createRedmineLayouts([]));

        return <div className="animated fadeIn">
            {ELECTRON ? <div className="main-header-drag"></div> : null}
            {ELECTRON ? <div className="main-header">
                <div className="modal-header">
                    <button className="first-header-button" onClick={onCloseWindow}>
                        <span className="close-custom hairline"></span>
                    </button>
                    <button className="second-header-button" onClick={onMaximizeWindow}>
                        <span className="second-custom hairline"></span>
                    </button>
                    <button className="second-header-button" onClick={onMinimizeWindow}>
                        <span className="third-custom hairline"></span>
                    </button>
                </div>
            </div> : null}
            <Toastr />
            <Settings images={images}/>
            <div className="background-overlay"
                 style={ELECTRON ? {border: '1px solid #47c1f3', outline: '1px solid #116bff'} : {}}></div>
            <Background images={images}/>
            <div id="widget">
                <div id="center">
                    {!name
                        ? <div>
                            <div id="centerclock">
                                <div className="clock default-clock">
                                    <h1 className="time">{currentHours}:{currentMinutes}</h1>
                                </div>
                            </div>
                            <div id="greeting">
                                <h2>Good <span className="period">{dayTime}</span>
                                    , <span className="name" spellCheck="false">{settings[MENU_GENERAL].name}</span>.
                                </h2>
                            </div>
                        </div>
                        : <div className="prompt">
                            <h2>
                                Hello, what's your name?
                            </h2>
                            <input type="text" value={settings[MENU_GENERAL].name} onKeyPress={this.handleKeyPress}
                                   onChange={this.onChangeName}/>
                        </div>}
                </div>
                {settings[MENU_WINDOWS].st === 1 ? <Time zIndex={zIndexTime} onActiveWindow={changeZIndexTime}/> : null}
                {settings[MENU_WINDOWS].sp === 1 ?
                    <Password zIndex={zIndexPass} onActiveWindow={changeZIndexPass}/> : null}
                <div style={{position: "fixed", zIndex: App.DEFAULT_Z_INDEX_WIDGET, top: 0, width: '100%'}}>
                    <ResponsiveReactGridLayout {...this.props} onLayoutChange={onChangeLayout}
                                               style={{width: '100%'}}
                                               layout={layouts}
                                               isDraggable={!settings[MENU_GENERAL][PIN_WIDGETS]}
                                               isResizable={!settings[MENU_GENERAL][PIN_WIDGETS]}
                                               onDragStop={onDragWidget}
                                               onResizeStop={onResizeStopWidget}
                                               draggableCancel=".widget-grid-item"
                                               draggableHandle=".widget-header">
                        {layouts.map(layout => <div key={layout.i}
                                                    className={`widget-grid ${layout.type.toLowerCase()}-widget`
                                                    + (settings[MENU_GENERAL][PIN_WIDGETS] ? ' not-move' : '')}>
                            {layout.widget}
                        </div>)}
                    </ResponsiveReactGridLayout>
                </div>
            </div>
        </div>;
    }
}

function mapStateToProps(state) {
    return {
        settings:     state.settings,
        activeIssues: state.activeIssues.elems,
        name:         state.name,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        settingsActions:     bindActionCreators(SettingsActions, dispatch),
        activeIssuesActions: bindActionCreators(ActiveIssuesActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);