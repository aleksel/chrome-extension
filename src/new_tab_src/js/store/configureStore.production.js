// @flow
import { createStore, applyMiddleware } from 'redux';
/*import { hashHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';*/
import rootReducer from '../reducers';
import middleware from "./../middleware/middleware";

//const router = routerMiddleware(hashHistory);

const enhancer = applyMiddleware(middleware/*, router*/);

export default function configureStore(initialState: Object | void) {
  return createStore(rootReducer, initialState, enhancer);
}
