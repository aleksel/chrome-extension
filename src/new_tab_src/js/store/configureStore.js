// @flow
if (NODE_ENV === 'dev') {
  module.exports = require('./configureStore.development'); // eslint-disable-line global-require
} else {
  module.exports = require('./configureStore.production'); // eslint-disable-line global-require
}
