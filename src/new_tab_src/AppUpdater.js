import {BrowserWindow as BrowserWindowElectron, app} from "electron";
import * as os from "os";
import {autoUpdater} from "electron-auto-updater";

export default class AppUpdater {
    constructor(window) {
        const platform = os.platform() + '_' + os.arch();
        const version = app.getVersion();
        autoUpdater.signals.updateDownloaded(it => {
            notify("A new update is ready to install", `Version ${it.version} is downloaded and will be automatically installed on Quit`)
        });
        console.log('http://10.250.9.114:5000/update/' + platform + '/' + version + '/RELEASES');
        autoUpdater.setFeedURL({
            'provider': 'generic',
            'url':      'http://10.250.9.114:5000/update/' + platform + '/' + version + '/RELEASES'
        });
        autoUpdater.checkForUpdates()
    }
}

function notify(title: string, message: string) {
    let windows = BrowserWindowElectron.getAllWindows();
    if (windows.length == 0) {
        return
    }

    windows[0].webContents.send("notify", title, message)
}