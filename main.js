const {app, BrowserWindow, Menu, shell, ipcMain, Tray} = require('electron');
//const AppUpdater = require("./src/new_tab_src/AppUpdater").default;
const path = require('path');
const url = require('url');
const BOUNDS_KEY = 'WINDOW_POSITION';
const AUTO_LAUNCH = 'AUTO_LAUNCH';

//====================JSON Storage=======================
const JSONStorage = require('node-localstorage').JSONStorage;
const electron = require('electron');
const userDataPath = (electron.app || electron.remote.app).getPath('userData');
console.log('USER_DATA', userDataPath);
const localStorage = new JSONStorage(userDataPath);

const checkAndParseJson = (data) => {
    try {
        console.log('---LOAD---', data);
        return JSON.parse(data);
    } catch (ignore) {
    }
    return data;
};
//====================JSON Storage=======================

let menu;
let template;
let mainWindow = null;
let tray = undefined;
let iconSetting = 'auto';

if (process.env.NODE_ENV === 'production') {
    const sourceMapSupport = require('source-map-support'); // eslint-disable-line
    sourceMapSupport.install();
}

if (process.env.NODE_ENV === 'dev') {
    require('electron-debug')(); // eslint-disable-line global-require
    const path = require('path'); // eslint-disable-line
    const p = path.join(__dirname, '..', 'app', 'node_modules'); // eslint-disable-line
    require('module').globalPaths.push(p); // eslint-disable-line
}

const installExtensions = () => {
    if (process.env.NODE_ENV === 'dev') {
        const installer = require('electron-devtools-installer'); // eslint-disable-line global-require

        const extensions = [
            'REACT_DEVELOPER_TOOLS',
            'REDUX_DEVTOOLS'
        ];
        const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
        for (const name of extensions) { // eslint-disable-line
            try {
                installer.default(installer[name], forceDownload);
            } catch (e) {
            } // eslint-disable-line
        }
    }
};

//====================Focus Main Window=======================
ipcMain.on('focus-main-window', event => {
    if (!mainWindow.isVisible()) {
        mainWindow.show();
        mainWindow.focus();
    } else {
        mainWindow.focus();
    }
});
//====================Focus Main Window=======================

//====================Auto Launcher=======================
const AutoLaunch = require('auto-launch');
const appLauncher = new AutoLaunch({
    name:     'temps',
    path:     userDataPath,
    isHidden: true
});

appLauncher.isEnabled().then(function (enabled) {
    if (enabled) return;
    return appLauncher.enable();
}).then(function (err) {
    if (err) {
        console.log(err);
    }
});

if (localStorage.getItem(AUTO_LAUNCH) === true) {
    console.log('enable auto-launch');
    appLauncher.enable();
}

ipcMain.on('auto-launch', (event, autoLaunch) => {
    // ToDo: appLauncher.isEnabled() not working for now
    // console.log(appLauncher.isEnabled());
    if (!autoLaunch) {
        appLauncher.disable();
        localStorage.setItem(AUTO_LAUNCH, false);
        console.log('disable auto-launch');
    } else {
        localStorage.setItem(AUTO_LAUNCH, true);
        appLauncher.enable();
        console.log('enable auto-launch');
    }
});
//====================Auto Launcher=======================

//====================TRAY=======================
const createTray = () => {
    tray = new Tray(path.join(__dirname + (process.env.NODE_ENV === 'dev'
            ? '/src/new_tab_src/image/dashboard-512.png'
            : '/src/new_tab/image/dashboard-512.12e237.png')));
    /*{
     label: 'Item1',
     type: 'radio',
     icon:  path.join(__dirname, '/resources/icon.png'),
     checked: true
     },
     {
     label: 'Item2',
     submenu: [
     { label: 'submenu1' },
     { label: 'submenu2' }
     ]
     }*/
    const menu = [
        {
            label:       'Quit',
            accelerator: 'Command+Q',
            selector:    'terminate:',
            click() {
                app.quit();
            }
        }
    ];
    if (process.env.NODE_ENV === 'dev') {
        menu.unshift({
            label:       'Toggle DevTools',
            accelerator: 'Alt+Command+I',
            click:       function () {
                mainWindow.show();
                mainWindow.toggleDevTools();
            }
        });
    }
    const contextMenu = Menu.buildFromTemplate(menu);
    tray.setToolTip('This is my application.');
    tray.setContextMenu(contextMenu);
    tray.on('right-click', toggleWindow);
    tray.on('double-click', toggleWindow);
    tray.on('click', function (event) {
        toggleWindow();
    });

    /*const changeIcon = function (icon) {
     if (iconSetting === 'auto') {
     if (process.platform === 'darwin') {
     mb.tray.setImage(path.join(__dirname, '/../assets/icons', icon + 'Template.png'))
     } else {
     mb.tray.setImage(path.join(__dirname, '/../assets/icons', icon + 'W.png'))
     }
     } else {
     if (iconSetting === 'white') {
     mb.tray.setImage(path.join(__dirname, '/../assets/icons', icon + 'W.png'))
     } else {
     mb.tray.setImage(path.join(__dirname, '/../assets/icons', icon + '.png'))
     }
     }
     }*/
};

const toggleWindow = () => {
    if (mainWindow.isVisible()) {
        mainWindow.hide();
    } else {
        mainWindow.show();
        mainWindow.focus();
    }
};
//====================TRAY=======================


//====================Events=======================
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit();
});
ipcMain.on('quit', () => {
    app.quit();
});

app.on('ready', () => {
    installExtensions();
    createTray();
    let data = checkAndParseJson(localStorage.getItem(BOUNDS_KEY));
    mainWindow = new BrowserWindow({
        webPreferences:  {webSecurity: false},
        show:            false,
        frame:           false,
        backgroundColor: '#fff',
        icon:            path.join(__dirname + (process.env.NODE_ENV === 'dev'
                ? '/src/new_tab_src/image/dashboard-512.png'
                : '/src/new_tab/image/dashboard-512.12e237.png')),
        width:           data && data.bounds && data.bounds.width ? data.bounds.width : 1280,
        x:               data && data.bounds && data.bounds.x ? data.bounds.x : null,
        //fullscreen:   true,
        height:          data && data.bounds && data.bounds.height ? data.bounds.height : 1280,
        y:               data && data.bounds && data.bounds.y ? data.bounds.y : null,
    });
    //new AppUpdater(mainWindow);
    if (data && data.isMaximized) {
        mainWindow.maximize();
    }
    mainWindow.on("close", () => localStorage.setItem(BOUNDS_KEY, JSON.stringify({
        bounds:      mainWindow.getBounds(),
        isMaximized: mainWindow.isMaximized()
    })));

    mainWindow.loadURL(process.env.NODE_ENV === 'dev'
        ? `http://${process.env.IP}:${process.env.PORT}`
        : `file://${__dirname}/src/new_tab/index.html`);
    mainWindow.webContents.on('did-finish-load', () => {
        mainWindow.show();
        mainWindow.focus();
    });

    mainWindow.on('closed', () => {
        mainWindow = null;
    });

    if (process.env.NODE_ENV === 'dev') {
        mainWindow.openDevTools();
        mainWindow.webContents.on('context-menu', (e, props) => {
            const {x, y} = props;

            Menu.buildFromTemplate([{
                label: 'Inspect element',
                click() {
                    mainWindow.inspectElement(x, y);
                }
            }]).popup(mainWindow);
        });
    }

    if (process.platform === 'darwin') {
        template = [{
            label:   'Electron',
            submenu: [{
                label:    'About ElectronReact',
                selector: 'orderFrontStandardAboutPanel:'
            }, {
                type: 'separator'
            }, {
                label:   'Services',
                submenu: []
            }, {
                type: 'separator'
            }, {
                label:       'Hide ElectronReact',
                accelerator: 'Command+H',
                selector:    'hide:'
            }, {
                label:       'Hide Others',
                accelerator: 'Command+Shift+H',
                selector:    'hideOtherApplications:'
            }, {
                label:    'Show All',
                selector: 'unhideAllApplications:'
            }, {
                type: 'separator'
            }, {
                label:       'Quit',
                accelerator: 'Command+Q',
                click() {
                    app.quit();
                }
            }]
        }, {
            label:   'Edit',
            submenu: [{
                label:       'Undo',
                accelerator: 'Command+Z',
                selector:    'undo:'
            }, {
                label:       'Redo',
                accelerator: 'Shift+Command+Z',
                selector:    'redo:'
            }, {
                type: 'separator'
            }, {
                label:       'Cut',
                accelerator: 'Command+X',
                selector:    'cut:'
            }, {
                label:       'Copy',
                accelerator: 'Command+C',
                selector:    'copy:'
            }, {
                label:       'Paste',
                accelerator: 'Command+V',
                selector:    'paste:'
            }, {
                label:       'Select All',
                accelerator: 'Command+A',
                selector:    'selectAll:'
            }]
        }, {
            label:   'View',
            submenu: (process.env.NODE_ENV === 'dev') ? [{
                    label:       'Reload',
                    accelerator: 'Command+R',
                    click() {
                        mainWindow.webContents.reload();
                    }
                }, {
                    label:       'Toggle Full Screen',
                    accelerator: 'Ctrl+Command+F',
                    click() {
                        mainWindow.setFullScreen(!mainWindow.isFullScreen());
                    }
                }, {
                    label:       'Toggle Developer Tools',
                    accelerator: 'Alt+Command+I',
                    click() {
                        mainWindow.toggleDevTools();
                    }
                }] : [{
                    label:       'Toggle Full Screen',
                    accelerator: 'Ctrl+Command+F',
                    click() {
                        mainWindow.setFullScreen(!mainWindow.isFullScreen());
                    }
                }]
        }, {
            label:   'Window',
            submenu: [{
                label:       'Minimize',
                accelerator: 'Command+M',
                selector:    'performMiniaturize:'
            }, {
                label:       'Close',
                accelerator: 'Command+W',
                selector:    'performClose:'
            }, {
                type: 'separator'
            }, {
                label:    'Bring All to Front',
                selector: 'arrangeInFront:'
            }]
        }, {
            label:   'Help',
            submenu: [{
                label: 'Learn More',
                click() {
                    shell.openExternal('http://electron.atom.io');
                }
            }, {
                label: 'Documentation',
                click() {
                    shell.openExternal('https://github.com/atom/electron/tree/master/docs#readme');
                }
            }, {
                label: 'Community Discussions',
                click() {
                    shell.openExternal('https://discuss.atom.io/c/electron');
                }
            }, {
                label: 'Search Issues',
                click() {
                    shell.openExternal('https://github.com/atom/electron/issues');
                }
            }]
        }];

        menu = Menu.buildFromTemplate(template);
        Menu.setApplicationMenu(menu);
    } else {
        template = [{
            label:   '&File',
            submenu: [{
                label:       '&Open',
                accelerator: 'Ctrl+O'
            }, {
                label:       '&Close',
                accelerator: 'Ctrl+W',
                click() {
                    mainWindow.close();
                }
            }]
        }, {
            label:   '&View',
            submenu: (process.env.NODE_ENV === 'dev') ? [{
                    label:       '&Reload',
                    accelerator: 'Ctrl+R',
                    click() {
                        mainWindow.webContents.reload();
                    }
                }, {
                    label:       'Toggle &Full Screen',
                    accelerator: 'F11',
                    click() {
                        mainWindow.setFullScreen(!mainWindow.isFullScreen());
                    }
                }, {
                    label:       'Toggle &Developer Tools',
                    accelerator: 'Alt+Ctrl+I',
                    click() {
                        mainWindow.toggleDevTools();
                    }
                }] : [{
                    label:       'Toggle &Full Screen',
                    accelerator: 'F11',
                    click() {
                        mainWindow.setFullScreen(!mainWindow.isFullScreen());
                    }
                }]
        }, {
            label:   'Help',
            submenu: [{
                label: 'Learn More',
                click() {
                    shell.openExternal('http://electron.atom.io');
                }
            }, {
                label: 'Documentation',
                click() {
                    shell.openExternal('https://github.com/atom/electron/tree/master/docs#readme');
                }
            }, {
                label: 'Community Discussions',
                click() {
                    shell.openExternal('https://discuss.atom.io/c/electron');
                }
            }, {
                label: 'Search Issues',
                click() {
                    shell.openExternal('https://github.com/atom/electron/issues');
                }
            }]
        }];
        menu = Menu.buildFromTemplate(template);
        mainWindow.setMenu(menu);
    }
});
