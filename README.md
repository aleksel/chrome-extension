# How to run

Dev app:
```js
npm run elec-hot
```

Browser:
```js
npm run dev
```

Build for browser (prop):
```js
npm run build
```

Build electron package:
```js
npm run package-all
```

Compile js files:
```js
npm run build-e
```

# Changelog
#### 0.0.24:
> **New:** Add generate mongoId

> **New:** Add show and change redmine issue done ratio

#### 0.0.25:
> **New:** Add creating new issue

> ##### 0.0.25.1
>> **Fix:** Required module after transfer to webpack2
fix save redmine time && received redmine users with sort by last_login_in && make up
> ##### 0.0.25.2
>> **Fix:** Save redmine issue time

>> **Change:** Received redmine users with sort by last_login_in (temporary fix if users count more then 100 (Redmine have api limit 100 to receive any items))