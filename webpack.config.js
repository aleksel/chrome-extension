'use strict';

const NODE_ENV = process.env.NODE_ENV || 'dev';
const ELECTRON = process.env.ELECTRON || false;
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const ENTRY_POINT = './js/index.jsx';
const path = require('path');
const context = path.join(__dirname, 'src/new_tab_src');
const contentDir = path.join(__dirname, '/src/new_tab/');
const contentPath = process.env.ELECTRON && process.env.NODE_ENV !== 'dev' ? '' : '/src/new_tab/';
const contentTemplate = path.join(context, 'html_template/index.html');

const webpack = require('webpack');
const port = process.env.PORT || 8088;
const ip = process.env.IP || '127.0.0.1';

function addHash(template, hash) {
    return NODE_ENV !== 'dev' ? template.replace(/\.[^.]+$/, `.[${hash}]$&`) : template;
}
const config = {
    context:       context, // исходная директория
    module:        {
        rules: [
            {
                test:   /\.css$/,
                loader: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader'})
            },
            {test: /\.js$/, loader: 'babel-loader', exclude: /(node_modules|bower_components)/,},
            {
                test:    /\.jsx$/,
                include: context,
                loader:  NODE_ENV === 'dev' ? 'babel-loader?presets[]=es2015,presets[]=react,presets[]=stage-0,presets[]=react-hmre'
                             : 'babel-loader?presets[]=es2015,presets[]=react,presets[]=stage-0'
            },
            {
                test:    /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)/,
                //exclude: /\/node_modules\//,
                exclude: /\/dashboard-512\//,
                loader:  addHash('file-loader?name=[path][name].[ext]&limit=4096', 'hash:6')
            }
        ]
    },
    devtool:       'cheap-inline-module-source-map',
    resolve:       {
        extensions: ['.js', '.jsx', '.css'],
        modules:    ["node_modules"]
    },
    resolveLoader: {
        modules:    ['node_modules'],
        //moduleTemplates:    ['*-loader', '*'],
        extensions: ['.js', '.jsx']
    },
    entry:         {
        app:    [
            ENTRY_POINT,
        ],
        //twin_bcrypt: ['./js/lib/twin_bcrypt.js'],
        vendor: [
            "bootstrap",
            "bootstrap-material-design",
            "jquery",
            "react",
            "react-dom",
            "react-redux",
            "redux",
            "whatwg-fetch",
        ]
    }, // файл для сборки, если несколько - указываем hash (entry name => filename)
    externals:     {},
    output:        {
        filename:      addHash('[name].js', 'chunkhash'),
        library:       '[name]', // Экспорт точки входа в глобальную переменную
        chunkFilename: addHash('[id].js', 'chunkhash'), // Шаблон названия файлов с динамическими модулями
        path:          contentDir, // выходная директория
        pathinfo:      true,
        publicPath:    contentPath
    },
    plugins:       [
        new CleanWebpackPlugin([contentDir], {
            //root:    __dirname,
            verbose: true,
            dry:     false,
            exclude: []
        }),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.ProvidePlugin({
            'window.jQuery': 'jquery',
            'root.jQuery':   'jquery',
            jQuery:          'jquery',
            $:               'jquery',
            toastr:          'toastr',
            ReactDOM:        'react-dom',
            React:           'react'
        }),
        new webpack.DefinePlugin({
            ELECTRON: JSON.stringify(ELECTRON),
            NODE_ENV: JSON.stringify(NODE_ENV),
            PORT:     JSON.stringify(port),
            IP:       JSON.stringify(ip)
        }),
        new HtmlWebpackPlugin({
            filename: path.join(contentDir, 'index.html'),
            template: contentTemplate,
            chunks:   ['app', 'vendor'],
            //css:      ['style']
        }),
        new ExtractTextPlugin({filename: addHash('[name].css', 'contenthash'), disable: NODE_ENV === 'dev'}),
        new webpack.optimize.CommonsChunkPlugin({name: 'vendor', filename: 'vendor.js'}),
    ],
    node:          {
        __dirname:  false,
        __filename: false
    },
};

if (NODE_ENV === 'dev') {
    config.output.publicPath = `http://${ip}:${port}/`;
    config.devtool = 'eval';
    config.devServer = {
        /*proxy:              [{
         path:   '/api/!**',
         target: 'http://127.0.0.1:8899'
         }, {
         path:   '/j_spring_security_check',
         target: 'http://127.0.0.1:8899'
         }],*/
        host:               '0.0.0.0',
        contentBase:        contentDir,
        hot:                true,
        info:               true,
        port:               port,
        stats:              {
            colors: true
        },
        publicPath:         '/',
        historyApiFallback: true
    };
    config.plugins.push(
        new webpack.HotModuleReplacementPlugin()
    );
    config.entry.app = [
        `webpack-hot-middleware/client?path=http://${ip}:${port}/__webpack_hmr`,
        'babel-polyfill',
        ENTRY_POINT,
    ];
} else {
    config.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            beautify:  false,
            comments:  false,
            minimize:  true,
            sourceMap: true,
            compress:  {
                sequences:    true,
                booleans:     true,
                loops:        true,
                unused:       true,
                warnings:     false,
                drop_console: true,
                unsafe:       true
            }
        })
    );
}

if (ELECTRON) {
    config.target = 'electron-renderer';
}

module.exports = config;
