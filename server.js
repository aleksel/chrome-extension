/* eslint-disable no-console */
/**
 * Setup and run the development server for Hot-Module-Replacement
 * https://webpack.github.io/docs/hot-module-replacement-with-webpack.html
 */
const express = require("express");
const webpack = require("webpack");
const webpackDevMiddleware = require("webpack-dev-middleware");
const webpackHotMiddleware = require("webpack-hot-middleware");
const spawn = require("child_process").spawn;
const config = require("./webpack.config");

const argv = require('minimist')(process.argv.slice(2));

const app = express();
const compiler = webpack(config);
const PORT = process.env.PORT || 8088;
const IP = process.env.IP || '127.0.0.1';

const wdm = webpackDevMiddleware(compiler, {
    contentBase: config.output.publicPath,
    publicPath:  config.output.publicPath,
    //quiet:       true,
    noInfo:      true,
    hot:         true,
    inline:      true,
    lazy:        false,
    headers:     {'Access-Control-Allow-Origin': '*'},
    stats:       {
        colors: true
    }
});

app.use(wdm);

app.use(webpackHotMiddleware(compiler));

const server = app.listen(PORT, IP, serverError => {
    if (serverError) {
        return console.error(serverError);
    }

    if (argv['start-hot']) {
        spawn('npm', ['run', 'start-hot'], {shell: true, env: process.env, stdio: 'inherit'})
            .on('close', code => process.exit(code))
            .on('error', spawnError => console.error(spawnError));
    }

    console.log(`Listening at ${config.output.publicPath}`);
});

process.on('SIGTERM', () => {
    console.log('Stopping dev server');
    wdm.close();
    server.close(() => {
        process.exit(0);
    });
});
